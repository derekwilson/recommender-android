package net.derekwilson.recommender.sharing.emailsharer;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import net.derekwilson.recommender.BaseTests;
import net.derekwilson.recommender.R;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.json.IExporter;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.sharing.EmailSharer;
import net.derekwilson.recommender.sharing.IntentShareResult;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.MediumTest;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class GetIntentTests extends BaseTests {
	// mocks
	private IExporter mockExporter = mock(IExporter.class);

	// object under test
	private EmailSharer sharer;

	@Before
	public void setUp() throws ExternalStorageException {
		// configure behaviour
		when(mockExporter.getBaseUri()).thenReturn("BASE URI");
		when(mockExporter.exportRecommendations(anyListOf(Recommendation.class), anyString())).thenAnswer(new Answer<Uri>() {
			@Override
			public Uri answer(InvocationOnMock invocation) throws Throwable {
				String str = (String) invocation.getArguments()[1];
				return Uri.fromParts("file", str, "fragment");
			}
		});

		// this is an integration test run on a real device - lets use and test the real resources
		Context context = getApplicationContext();
		sharer = new EmailSharer(mockLoggerFactory, mockExporter, context);
	}

	@Test
	public void gets_email_share_intent_number() throws ExternalStorageException {
		// arrange
		Recommendation recommendation = setupRecommendation("test_name", "test_by", "test_category", "test_notes", "test_uri", "test_source");

		// act
		IntentShareResult result = sharer.getIntent(recommendation);

		// assert
		assertThat(result.NumberOfRecommendations, is(1));
	}

	@Test
	public void gets_email_share_intent_number2() throws ExternalStorageException {
		// arrange
		Recommendation recommendation = setupRecommendation("test_name", "test_by", "test_category", "test_notes", "test_uri","test_source");
		Recommendation recommendation2 = setupRecommendation("test_name", "test_by", "test_category", "test_notes", "test_uri","test_source");
		ArrayList<Recommendation> list = new ArrayList<>(2);
		list.add(recommendation);
		list.add(recommendation2);

		// act
		IntentShareResult result = sharer.getIntent(list);

		// assert
		assertThat(result.NumberOfRecommendations, is(2));
	}

	@Test
	public void gets_email_share_intent_type() throws ExternalStorageException {
		// arrange
		Recommendation recommendation = setupRecommendation("test_name", "test_by", "test_category", "test_notes", "test_uri","test_source");

		// act
		IntentShareResult result = sharer.getIntent(recommendation);

		// assert
		assertThat(result.SendIntent.getType(), is("vnd.android.cursor.dir/email"));
	}

	@Test
	public void gets_email_share_intent_uri() throws ExternalStorageException {
		// arrange
		Recommendation recommendation = setupRecommendation("test_name", "test_by", "test_category", "test_notes", "test_uri","test_source");

		// act
		IntentShareResult result = sharer.getIntent(recommendation);

		// assert
		ArrayList<Uri> attachmentUris = result.SendIntent.getExtras().getParcelableArrayList(Intent.EXTRA_STREAM);
		assertThat(attachmentUris.get(0).toString(),
				is("file:BASE%20URIrecommendations_share.recommendation#fragment"));
	}

	@Test
	public void gets_email_share_intent_text() throws ExternalStorageException {
		// arrange
		Recommendation recommendation = setupRecommendation("test_name", "test_by", "test_category", "test_notes", "test_uri","test_source");

		// act
		IntentShareResult result = sharer.getIntent(recommendation);

		// assert
		assertThat(result.SendIntent.getExtras().get(Intent.EXTRA_SUBJECT).toString(),
				is("I Recommend"));
		assertThat(result.SendIntent.getExtras().get(Intent.EXTRA_TEXT).toString(),
				containsString(recommendation.getName()));
		assertThat(result.SendIntent.getExtras().get(Intent.EXTRA_TEXT).toString(),
				containsString(recommendation.getBy()));
		assertThat(result.SendIntent.getExtras().get(Intent.EXTRA_TEXT).toString(),
				containsString(recommendation.getNotes()));
		assertThat(result.SendIntent.getExtras().get(Intent.EXTRA_TEXT).toString(),
				containsString(recommendation.getUri()));
	}
}
