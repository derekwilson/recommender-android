package net.derekwilson.recommender.presenter.sharereceiverecommendationactivity;

import android.content.Intent;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.IPresenter;
import net.derekwilson.recommender.presenter.IView;

public interface IShareReceiveRecommendationActivityPresenter extends IPresenter<IShareReceiveRecommendationActivityView> {
	void unpackIntent(Intent intent);
}
