package net.derekwilson.recommender.tests.html.documentconverer;

		import net.derekwilson.recommender.model.Recommendation;

		import org.junit.Test;

		import java.io.IOException;
		import java.net.URL;

		import static org.hamcrest.MatcherAssert.assertThat;
		import static org.hamcrest.core.Is.is;

public class AmazonCoUkConverterTests extends BaseHtmlConverterTests {
	private static String AMAZON_URL = "http://www.amazon.co.uk/Sweet-Bells-Kate-Rusby/dp/B002S53LZG/ref=sr_1_4?ie=UTF8&qid=1442388218&sr=8-4&keywords=kate+rusby";
	private static String AMAZON_FILENAME = "test_amazon_uk.html";

	private URL testUrl;

	@Test
	public void gets_amazon_name() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getName(), is("The Peripheral"));
	}

	@Test
	public void gets_amazon_by() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getBy(), is("Gibson, William"));
	}

	@Test
	public void gets_amazon_category() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void gets_amazon_notes() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getNotes(), is("The Peripheral: Now a major new TV series with Amazon Prime eBook : Gibson, William: Amazon.co.uk: Kindle Store"));
	}

	@Test
	public void gets_amazon_url() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getUri(), is(AMAZON_URL));
	}
}


