package net.derekwilson.recommender.tests.presenter.editrecommendation;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class HasBeenEditedTests extends PresenterTestsSetup {

	@Before
	public void setUp() {
		super.setUp();
		setupPresenter(true, false);
	}

	@Test
	public void create_empty_recommendation_is_not_dirty() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("","","","","","");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(false));
	}

	@Test
	public void create_edited_recommendation_is_dirty() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri","source");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void create_edited_recommendation_is_dirty_name_changed() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("name", "", "", "", "", "");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void create_edited_recommendation_is_dirty_by_changed() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("", "by", "", "", "", "");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void create_edited_recommendation_is_dirty_category_changed() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("", "", "category", "", "", "");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void create_edited_recommendation_is_dirty_notes_changed() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("", "", "", "notes", "", "");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void create_edited_recommendation_is_dirty_uri_changed() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("", "", "", "", "uri", "");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void create_edited_recommendation_is_dirty_source_changed() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("", "", "", "", "", "source");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void edit_unchanged_recommendation_is_not_dirty() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri","source");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(false));
	}

	@Test
	public void edit_changed_recommendation_is_dirty() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name1", "by1", "category1", "notes1", "uri1","source1");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void edit_changed_recommendation_is_dirty_name_changed() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name1", "by", "category", "notes", "uri","source");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void edit_changed_recommendation_is_dirty_by_changed() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name", "by1", "category", "notes", "uri","source");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void edit_changed_recommendation_is_dirty_category_changed() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name", "by", "category1", "notes", "uri","source");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void edit_changed_recommendation_is_dirty_notes_changed() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes1", "uri","source");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void edit_changed_recommendation_is_dirty_uri_changed() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri1","source");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}

	@Test
	public void edit_changed_recommendation_is_dirty_source_changed() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri","source1");

		// act
		boolean edited = presenter.hasBeenEdited();

		// assert
		assertThat(edited, is(true));
	}
}
