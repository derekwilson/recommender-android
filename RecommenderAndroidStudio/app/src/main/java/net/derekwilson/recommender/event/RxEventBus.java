package net.derekwilson.recommender.event;

import javax.inject.Inject;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * this is the event bus for the app
 * it could be implemented using any library for example otto or eventbus
 * but for now we are using a light weight hand rolled RxAndroid implementation
 * inspired by this article
 * http://nerds.weddingpartyapp.com/tech/2014/12/24/implementing-an-event-bus-with-rxjava-rxbus/
 */
public class RxEventBus implements IEventBus {

	private final Subject<Object, Object> _bus = new SerializedSubject<>(PublishSubject.create());

	@Inject
	public RxEventBus() {
	}

	@Override
	public void send(Object event) {
		if (_bus.hasObservers()) {
			_bus.onNext(event);
		}
	}

	@Override
	public Observable<Object> toObservable() {
		return _bus;
	}
}
