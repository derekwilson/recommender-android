package net.derekwilson.recommender.tests.presenter.editrecommendationactivity;

import android.app.Activity;

import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.presenter.editrecommendationactivity.EditRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.editrecommendationactivity.IEditRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.editrecommendationactivity.IEditRecommendationActivityView;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.INdefMessageSharer;
import net.derekwilson.recommender.tests.BaseJunitTests;
import net.derekwilson.recommender.wrapper.ITextUtils;

import static org.mockito.Mockito.mock;

public class PresenterTestSetup extends BaseJunitTests {
	// mocks
	protected Activity mockActivity = mock(Activity.class);
	protected IEditRecommendationActivityView mockView = mock(IEditRecommendationActivityView.class);
	protected IRecommendationRepository mockRepository = mock(IRecommendationRepository.class);
	protected IPreferencesProvider mockPreferences = mock(IPreferencesProvider.class);
	protected ITextUtils mockTextUtils = mock(ITextUtils.class);
	protected IEmailIntentSharer mockEmailSharer = mock(IEmailIntentSharer.class);
	protected INdefMessageSharer mockMessageSharer = mock(INdefMessageSharer.class);
	protected IEventLogger mockEventLogger = mock(IEventLogger.class);
	protected ICrashReporter mockReporter = mock(ICrashReporter.class);

	// object under test
	protected IEditRecommendationActivityPresenter presenter;

	protected void setupPresenter(boolean bind) {
		presenter = new EditRecommendationActivityPresenter(mockPreferences, mockTextUtils, mockEmailSharer, mockMessageSharer, mockEventLogger, mockReporter);
		if (bind) {
			presenter.bindView(mockView);
		}
	}
}
