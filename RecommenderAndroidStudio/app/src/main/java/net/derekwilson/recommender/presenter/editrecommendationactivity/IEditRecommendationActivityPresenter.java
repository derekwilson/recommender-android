package net.derekwilson.recommender.presenter.editrecommendationactivity;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.IPresenter;

public interface IEditRecommendationActivityPresenter extends IPresenter<IEditRecommendationActivityView> {
	boolean canShare(Recommendation recommendation);

	boolean canDelete(Recommendation recommendation);

	boolean canMove(Recommendation recommendation);

	int getMoveMenuTextId(RecommendationType recommendationType);

	void shareRecommendation();

	void beamRecommendation();
}
