package net.derekwilson.recommender.tests.presenter.sharereceiverecommendationactivity;

import android.app.Activity;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.ioc.scopes.IShareReceiveRecommendationComponent;
import net.derekwilson.recommender.presenter.sharereceiverecommendationactivity.IShareReceiveRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.sharereceiverecommendationactivity.IShareReceiveRecommendationActivityView;
import net.derekwilson.recommender.presenter.sharereceiverecommendationactivity.ShareReceiveRecommendationActivityPresenter;
import net.derekwilson.recommender.receiving.IIntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromText;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromUrl;
import net.derekwilson.recommender.tests.BaseJunitTests;
import net.derekwilson.recommender.wrapper.ICalendarUtils;

import static org.mockito.Mockito.mock;

public class PresenterTestSetup extends BaseJunitTests {
	// mocks
	protected Activity mockActivity = mock(Activity.class);
	protected IShareReceiveRecommendationActivityView mockView = mock(IShareReceiveRecommendationActivityView.class);
	protected IRecommendationRepository mockRepository = mock(IRecommendationRepository.class);
	protected IIntentContentTypeFinder mockFinder = mock(IIntentContentTypeFinder.class);
	protected IIntentUnpackerFromUrl mockUrlUnpacker = mock(IIntentUnpackerFromUrl.class);
	protected IIntentUnpackerFromText mockTextUnpacker = mock(IIntentUnpackerFromText.class);
	protected ICalendarUtils mockCalendarUtils = mock(ICalendarUtils.class);

	// object under test
	protected IShareReceiveRecommendationActivityPresenter presenter;

	protected IShareReceiveRecommendationComponent component;

	protected void setupPresenter(boolean bind) {
		presenter = new ShareReceiveRecommendationActivityPresenter(
				mockLoggerFactory,
				mockFinder,
				mockUrlUnpacker,
				mockTextUnpacker,
				mockCalendarUtils);
		if (bind) {
			presenter.bindView(mockView);
		}
	}
}
