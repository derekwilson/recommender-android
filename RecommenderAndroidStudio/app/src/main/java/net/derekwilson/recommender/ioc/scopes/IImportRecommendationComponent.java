package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.activity.importRecommendation.ImportRecommendationActivity;
import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.presenter.importrecommendationactivity.IImportRecommendationActivityPresenter;

import dagger.Component;

@ActivityScope
@Component(dependencies = IApplicationComponent.class, modules = {BaseActivityModule.class, ImportRecommendationModule.class})
public interface IImportRecommendationComponent {
	// list any activities or fragments that use this component
	void inject(ImportRecommendationActivity activity);

	// used by the tests
	IImportRecommendationActivityPresenter getActivityPresenter();
}
