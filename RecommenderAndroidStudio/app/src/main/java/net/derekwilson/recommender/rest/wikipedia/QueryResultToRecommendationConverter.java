package net.derekwilson.recommender.rest.wikipedia;

import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.rest.wikipedia.model.QueryResult;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

public class QueryResultToRecommendationConverter implements IQueryResultToRecommendationConverter {

	private ILoggerFactory logger;

	@Inject
	public QueryResultToRecommendationConverter(ILoggerFactory logger) {
		this.logger = logger;
	}

	private LinkedHashMap<String, Object> getFirstPageMap(QueryResult result) {
		if (result != null && result.getQuery() != null && result.getQuery().getPages() != null && result.getQuery().getPages().getAdditionalProperties() != null) {
			Map<String, Object> pages = (result.getQuery().getPages().getAdditionalProperties());;
			return (LinkedHashMap<String, Object>) pages.values().iterator().next();
		}
		return null;
	}

	private String getFromMap(LinkedHashMap<String, Object> map, String key) {
		if (map != null && map.containsKey(key)) {
			return map.get(key).toString();
		}
		return null;
	}

	@Override
	public Recommendation convert(URL url, QueryResult wikipediaResult) {
		Recommendation recommendation = new Recommendation();
		recommendation.setUri(url.toString());
		LinkedHashMap<String, Object> pageData = getFirstPageMap(wikipediaResult);
		if (pageData != null) {
			recommendation.setName(getFromMap(pageData, "title"));
			recommendation.setNotes(getFromMap(pageData, "extract"));
			logger.getCurrentApplicationLogger().debug("found {}", recommendation.getName());
		}
		else {
			logger.getCurrentApplicationLogger().debug("cannot find page data from wikipedia result");
		}
		return recommendation;
	}
}
