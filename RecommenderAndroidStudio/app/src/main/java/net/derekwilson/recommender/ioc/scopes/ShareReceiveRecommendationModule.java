package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.presenter.sharereceiverecommendationactivity.IShareReceiveRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.sharereceiverecommendationactivity.ShareReceiveRecommendationActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ShareReceiveRecommendationModule {
	/**
	 * this is the presenter used by the shre receive activity
	 */
	@Provides
	@ActivityScope
	IShareReceiveRecommendationActivityPresenter provideActivityPresenter(ShareReceiveRecommendationActivityPresenter presenter) {
		return presenter;
	}
}
