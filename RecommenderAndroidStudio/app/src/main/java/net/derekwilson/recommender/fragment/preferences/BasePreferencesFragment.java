package net.derekwilson.recommender.fragment.preferences;

import android.app.Activity;
import android.preference.PreferenceFragment;

import com.squareup.sqlbrite.SqlBrite;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.ioc.DaggerIBaseActivityComponent;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.IBaseActivityComponent;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.logging.ILoggerFactory;

import javax.inject.Inject;

public class BasePreferencesFragment extends PreferenceFragment {

	@Inject
	protected ILoggerFactory logger;

	@Inject
	protected ICrashReporter crashReporter;

	@Inject
	protected SqlBrite db;

	protected IBaseActivityComponent activityComponent;

	protected IApplicationComponent getApplicationComponent(Activity activity) {
		return ((AndroidApplication) activity.getApplication()).getApplicationComponent();
	}

	protected IBaseActivityComponent getBaseActivityComponent(Activity activity) {
		if (activityComponent == null) {
			activityComponent = DaggerIBaseActivityComponent.builder()
									.iApplicationComponent(getApplicationComponent(activity))
									.baseActivityModule(new BaseActivityModule(activity))
									.build();
		}
		return activityComponent;
	}

	protected boolean isInjected() {
		return logger != null;
	}
}
