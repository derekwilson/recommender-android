package net.derekwilson.recommender.exceptions;

public class ExternalStorageException extends Exception {
	public ExternalStorageException(String filename, Throwable throwable) {
		super("File: " + filename, throwable);
	}
}

