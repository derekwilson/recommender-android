package net.derekwilson.recommender.presenter.editrecommendation;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.IPresenter;

import java.util.Calendar;

public interface IEditRecommendationPresenter extends IPresenter<IEditRecommendationView> {
	boolean hasBeenEdited();

	void deleteRecommendation(Recommendation recommendation);

	void saveRecommendation(Recommendation recommendation);

	void browseToUri();

	void moveRecommendation(Recommendation recommendation);

	void setIsFromShareReceiver(boolean aBoolean);

	String getDateForDisplay(Calendar date);
}
