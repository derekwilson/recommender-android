package net.derekwilson.recommender.receiving;

import android.content.Intent;
import android.net.Uri;

import net.derekwilson.recommender.logging.ILoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

import javax.inject.Inject;

public class IntentContentTypeFinder implements IIntentContentTypeFinder {
	private ILoggerFactory logger;

	@Inject
	public IntentContentTypeFinder(ILoggerFactory logger) {
		this.logger = logger;
	}

	@Override
	public IntentContentType getTypeOfContent(Intent intent) {
		logger.getCurrentApplicationLogger().debug("IntentContentTypeFinder intent action {} package {}, type {}", intent.getAction(), intent.getPackage(), intent.getType());

		String action = intent.getAction();
		if (action != null && action.equalsIgnoreCase("android.nfc.action.NDEF_DISCOVERED")) {
			return IntentContentType.RecommenderNfcBeam;
		}

		Uri intentUri = intent.getData();
		if (intentUri != null) {
			logger.getCurrentApplicationLogger().debug("IntentContentTypeFinder uri {}", intentUri.toString());
			if (intentUri.getScheme() != null && intentUri.getScheme().equalsIgnoreCase("content")) {
				return IntentContentType.ContentUri;
			}
			if (intentUri.getScheme() != null && intentUri.getScheme().equalsIgnoreCase("file")) {
				return IntentContentType.FileUri;
			}
		}

		String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
		if (sharedText != null) {
			logger.getCurrentApplicationLogger().debug("IntentContentTypeFinder string {} {}", intent.getType(), sharedText);
			try {
				URL url = new URL(sharedText);
				logger.getCurrentApplicationLogger().debug("IntentContentTypeFinder uri {}", url.toString());
				return IntentContentType.Url;
			}
			catch (MalformedURLException e) {
				// we cannot understand it assume it is text
				return IntentContentType.Text;
			}
		}

		return IntentContentType.Unknown;
	}
}
