package net.derekwilson.recommender.preferences;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;

public interface IPreferencesProvider {
	String getPreferenceString(String key);
	void setPreferenceString(String key, String value);

	IRecommendationRepository.SortBy getPreferencesSortOrder(String key);
}
