package net.derekwilson.recommender.rest.wikipedia.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
		"pageid",
		"ns",
		"title",
		"extract"
})
public class _3374 {

	@JsonProperty("pageid")
	private Integer pageid;
	@JsonProperty("ns")
	private Integer ns;
	@JsonProperty("title")
	private String title;
	@JsonProperty("extract")
	private String extract;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 *
	 * @return
	 * The pageid
	 */
	@JsonProperty("pageid")
	public Integer getPageid() {
		return pageid;
	}

	/**
	 *
	 * @param pageid
	 * The pageid
	 */
	@JsonProperty("pageid")
	public void setPageid(Integer pageid) {
		this.pageid = pageid;
	}

	/**
	 *
	 * @return
	 * The ns
	 */
	@JsonProperty("ns")
	public Integer getNs() {
		return ns;
	}

	/**
	 *
	 * @param ns
	 * The ns
	 */
	@JsonProperty("ns")
	public void setNs(Integer ns) {
		this.ns = ns;
	}

	/**
	 *
	 * @return
	 * The title
	 */
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	/**
	 *
	 * @param title
	 * The title
	 */
	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 *
	 * @return
	 * The extract
	 */
	@JsonProperty("extract")
	public String getExtract() {
		return extract;
	}

	/**
	 *
	 * @param extract
	 * The extract
	 */
	@JsonProperty("extract")
	public void setExtract(String extract) {
		this.extract = extract;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
