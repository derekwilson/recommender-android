package net.derekwilson.recommender.html;

import net.derekwilson.recommender.model.Recommendation;

import org.jsoup.nodes.Document;

import java.net.URL;

public interface IDocumentToRecommendationConverter {
	Recommendation convert(URL url, Document document);
}
