package net.derekwilson.recommender.exceptions;

public class UnknownHtmlShareException extends Exception {
	public UnknownHtmlShareException(String url) {
		super("Url: " + url);
	}
	public UnknownHtmlShareException(String reason, String url) {
		super("Reason: " + reason + " Url: " + url);
	}
}
