package net.derekwilson.recommender.event;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.model.RecommendationType;

public class DbSubscriptionRestartRequired {
	private String subscription;

	public DbSubscriptionRestartRequired(String subscription) {
		this.subscription = subscription;
	}

	public String getSubscription() {
		return subscription;
	}
}

