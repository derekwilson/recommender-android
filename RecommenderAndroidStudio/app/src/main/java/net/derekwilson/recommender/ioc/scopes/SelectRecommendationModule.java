package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.presenter.selectrecommendation.ISelectRecommendationPresenter;
import net.derekwilson.recommender.presenter.selectrecommendation.SelectRecommendationPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SelectRecommendationModule {
	@Provides
	@ActivityScope
	ISelectRecommendationPresenter providePresenter(SelectRecommendationPresenter presenter) {
		return presenter;
	}
}
