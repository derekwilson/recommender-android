package net.derekwilson.recommender.model;

import java.util.List;

import javax.inject.Inject;

public class FilterCollection implements IFilterCollection {
	private List<FilterSelectionItem> filters;

	private String searchFilter;

	public enum FilterType {
		None,
		Category,
		Search,
		CategoryAndSearch
	}

	@Inject
	public FilterCollection() {
		this.filters = null;
		this.searchFilter = null;
	}

	@Override
	public String getSearchFilter() {
		return searchFilter;
	}

	@Override
	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
	}

	@Override
	public FilterType getFilterType() {
		if (!isNoFilter() && searchFilter != null) {
			return FilterType.CategoryAndSearch;
		}
		else if (searchFilter != null) {
			return FilterType.Search;
		}
		else if (!isNoFilter()) {
			return FilterType.Category;
		}
		return FilterType.None;
	}

	@Override
	public List<FilterSelectionItem> getFilters() {
		return filters;
	}

	@Override
	public void setFilters(List<FilterSelectionItem> filters) {
		this.filters = filters;
	}

	@Override
	public boolean isNoFilter() {
		if (filters == null) {
			return true;
		}
		for (FilterSelectionItem item : filters) {
			if (item.isSelected() && item.isNofilter()) {
				return true;
			}
			if (item.isSelected()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public FilterSelectionItem getFilterByName(String name) {
		if (filters == null) {
			return null;
		}
		for (FilterSelectionItem item : filters) {
			if (name.equals(item.getText())) {
				return item;
			}
		}
		return null;
	}

	@Override
	public boolean isFilterSelected(String name) {
		FilterSelectionItem item = getFilterByName(name);
		if (item != null) {
			return item.isSelected();
		}
		return false;
	}

	@Override
	public boolean isFilterSelected(int index) {
		if (filters != null && index < filters.size()) {
			return filters.get(index).isSelected();
		} else {
			return false;
		}
	}

	@Override
	public void setFilterSelectedByName(String name, boolean selected) {
		FilterSelectionItem item = getFilterByName(name);
		if (item != null) {
			item.setSelected(selected);
		}
	}

	@Override
	public void toggleFilterSelectedByName(String name) {
		FilterSelectionItem item = getFilterByName(name);
		if (item != null) {
			item.setSelected(!item.isSelected());
		}
	}

	@Override
	public void setAllSelected(boolean selected) {
		if (filters != null) {
			for (FilterSelectionItem item : filters) {
				item.setSelected(selected);
			}
		}
	}

	@Override
	public int getNumberOfSelectedFilters() {
		if (filters == null || filters.size() < 1) {
			return 0;
		}
		int numberSelected = 0;
		for (FilterSelectionItem item : filters) {
			if (item.isSelected() && !item.isNofilter()) {
				numberSelected++;
			}
		}
		return numberSelected;
	}

	@Override
	public String[] getSelectedAsArray(RecommendationType type) {
		int numberSelected = getNumberOfSelectedFilters();
		if (numberSelected < 1) {
			if (searchFilter == null) {
				String[] result = new String[1];
				result[0] = type.getValueAsString();
				return result;
			}
			else {
				String[] result = new String[2];
				result[0] = type.getValueAsString();
				result[1] = searchFilter;
				return result;
			}
		}
		String[] result = new String[numberSelected + (searchFilter != null ?  2 : 1)];
		result[0] = type.getValueAsString();
		int index = 1;
		if (searchFilter != null) {
			result[1] = searchFilter;
			index++;
		}
		for (FilterSelectionItem item : filters) {
			if (item.isSelected() && !item.isNofilter()) {
				result[index] = item.getText();
				index++;
			}
		}
		return result;
	}

	@Override
	public String getSelectedAsParameterPlaceholder() {
		StringBuilder str = new StringBuilder(100);
		boolean first = true;
		if (filters != null) {
			for (FilterSelectionItem item : filters) {
				if (item.isSelected() && !item.isNofilter()) {
					if (first) {
						first = false;
					}
					else {
						str.append(",");
					}
					str.append(" ? ");
				}
			}
		}
		return str.toString();
	}
}
