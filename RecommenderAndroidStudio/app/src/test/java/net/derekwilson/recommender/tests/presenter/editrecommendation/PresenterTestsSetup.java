package net.derekwilson.recommender.tests.presenter.editrecommendation;

import android.app.Activity;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.ioc.scopes.IEditRecommendationComponent;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.editrecommendation.EditRecommendationPresenter;
import net.derekwilson.recommender.presenter.editrecommendation.IEditRecommendationPresenter;
import net.derekwilson.recommender.presenter.editrecommendation.IEditRecommendationView;
import net.derekwilson.recommender.tests.BaseJunitTests;
import net.derekwilson.recommender.tests.testobjects.TestRecommendationRepository;
import net.derekwilson.recommender.wrapper.ICalendarUtils;

import org.junit.Before;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PresenterTestsSetup extends BaseJunitTests {

	// mocks
	protected Activity mockActivity = mock(Activity.class);
	protected IEditRecommendationView mockView = mock(IEditRecommendationView.class);
	protected IRecommendationRepository mockRepository = mock(IRecommendationRepository.class);
	protected ICrashReporter mockReporter = mock(ICrashReporter.class);
	protected ICalendarUtils mockCalendarUtils = mock(ICalendarUtils.class);

	// will use a concrete object until we have to use a mock
	protected Recommendation recommendationPassedToActivity;
	protected Recommendation recommendationFromUi;
	protected TestRecommendationRepository realRepository = new TestRecommendationRepository();

	protected IEditRecommendationComponent component;

	// object under test
	protected IEditRecommendationPresenter presenter;

	@Before
	public void setUp() {
		// setup required behaviour
		when(mockView.getRecommendation()).thenAnswer(new Answer<Recommendation>() {
			@Override
			public Recommendation answer(InvocationOnMock invocation) throws Throwable {
				return recommendationPassedToActivity;
			}
		});
		when(mockView.getRecommendationFromUi()).thenAnswer(new Answer<Recommendation>() {
			@Override
			public Recommendation answer(InvocationOnMock invocation) throws Throwable {
				return recommendationFromUi;
			}
		});
		when(mockView.getRecommendationType()).thenReturn(RecommendationType.RECOMMENDATION);
	}

	protected void setupPresenter(boolean bind, boolean useRealRepository) {
		presenter = new EditRecommendationPresenter(
				mockLoggerFactory,
				(useRealRepository ? realRepository : mockRepository),
				mockActivity,
				mockEventBus,
				mockReporter,
				mockCalendarUtils);
		if (bind) {
			presenter.bindView(mockView);
		}
	}
}
