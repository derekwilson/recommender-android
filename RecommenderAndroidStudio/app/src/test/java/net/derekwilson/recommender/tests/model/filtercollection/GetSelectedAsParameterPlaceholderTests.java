package net.derekwilson.recommender.tests.model.filtercollection;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GetSelectedAsParameterPlaceholderTests extends TestSetup {

	@Test
	public void empty_gives_placeholder() {
		// arrange

		// act
		String placholder = collection.getSelectedAsParameterPlaceholder();

		// assert
		assertThat(placholder, is(""));
	}

	@Test
	public void none_selected_gives_placeholder() {
		// arrange
		collection.setFilters(getFilters(true, true));

		// act
		String placholder = collection.getSelectedAsParameterPlaceholder();

		// assert
		assertThat(placholder, is(""));
	}

	@Test
	public void one_selected_gives_placeholder() {
		// arrange
		collection.setFilters(getFilters(true, true));
		collection.setFilterSelectedByName("FILTER 1", true);

		// act
		String placholder = collection.getSelectedAsParameterPlaceholder();

		// assert
		assertThat(placholder, is(" ? "));
	}

	@Test
	public void two_selected_gives_placeholder() {
		// arrange
		collection.setFilters(getFilters(true, true));
		collection.setFilterSelectedByName("FILTER 1", true);
		collection.setFilterSelectedByName("FILTER 2", true);

		// act
		String placholder = collection.getSelectedAsParameterPlaceholder();

		// assert
		assertThat(placholder, is(" ? , ? "));
	}

	@Test
	public void two_selected_and_nofilter_gives_placeholder() {
		// arrange
		collection.setFilters(getFilters(true, true));
		collection.setFilterSelectedByName("NOFILTER", true);
		collection.setFilterSelectedByName("FILTER 1", true);
		collection.setFilterSelectedByName("FILTER 2", true);

		// act
		String placholder = collection.getSelectedAsParameterPlaceholder();

		// assert
		assertThat(placholder, is(" ? , ? "));
	}
}
