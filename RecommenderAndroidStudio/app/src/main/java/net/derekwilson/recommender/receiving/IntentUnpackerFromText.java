package net.derekwilson.recommender.receiving;

import android.content.Intent;

import net.derekwilson.recommender.BuildConfig;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.exceptions.UnknownTextShareException;
import net.derekwilson.recommender.file.ITextFileWriter;
import net.derekwilson.recommender.model.Recommendation;

import javax.inject.Inject;

public class IntentUnpackerFromText implements IIntentUnpackerFromText {
	private ITextFileWriter textFileWriter;
	private ICrashReporter crashReporter;

	@Inject
	public IntentUnpackerFromText(ITextFileWriter textFileWriter, ICrashReporter crashReporter) {
		this.textFileWriter = textFileWriter;
		this.crashReporter = crashReporter;
	}

	private boolean containsAmazonUrl(String sharedText) {
		if (sharedText.contains("http://www.amazon")) {
			return true;
		}
		if (sharedText.contains("http://amzn")) {
			return true;
		}
		if (sharedText.contains("https://amzn")) {
			return true;
		}
		return false;
	}

	/**
	 * gets a single recommendation from a text share intent
	 *
	 * @param intent
	 * @return the recommendation or null
	 */
	@Override
	public Recommendation getRecommendationFromText(Intent intent) {
		String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
		if (sharedText == null) {
			return null;
		}
		if (BuildConfig.DEBUG) {
			textFileWriter.writeStringToFile("LastSharedText.txt", sharedText);
		}

		if (containsAmazonUrl(sharedText) && sharedText.contains(" by")) {
			Recommendation kindleRecommendation = getRecommendationFromKindleText(sharedText);
			if (kindleRecommendation != null) {
				return kindleRecommendation;
			}
		} else if (sharedText.contains("http://") || sharedText.contains("https://")) {
			Recommendation urlTextRecommendation = getRecommendationFromUrlText(sharedText);
			if (urlTextRecommendation != null) {
				return urlTextRecommendation;
			}
		}

		// the default position is to just return all the text in the title
		Recommendation recommendation = new Recommendation();
		recommendation.setName(sharedText);
		crashReporter.logNonFatalException(new UnknownTextShareException(sharedText));
		return recommendation;
	}

	private Recommendation getRecommendationFromUrlText(String sharedText) {
		Recommendation recommendation = new Recommendation();
		int startUrl = sharedText.indexOf("http://");
		if (startUrl < 0) {
			startUrl = sharedText.indexOf("https://");
		}
		if (startUrl < 0) {
			return null;
		}
		String urlCandidate = sharedText.substring(startUrl);
		int endUrl = urlCandidate.indexOf(' ');
		if (endUrl >= 0) {
			urlCandidate = urlCandidate.substring(0, endUrl);
		}
		recommendation.setUri(urlCandidate);
		recommendation.setCategory("WebSite");
		if (urlCandidate.endsWith(".xml")) {
			recommendation.setCategory("Podcast");
		}
		if (urlCandidate.contains("//play.google.com/")) {
			recommendation.setCategory("App");
		}

		String nameCandidate = sharedText.substring(0, startUrl);
		nameCandidate = nameCandidate.replace(" - ", " ");
		recommendation.setName(nameCandidate.trim());
		return recommendation;
	}

	private Recommendation getRecommendationFromKindleText(String sharedText) {
		final String progressText = "this book and think you might like it -";
		final String recommendText = "this book, ";
		Recommendation recommendation = new Recommendation();
		if (sharedText.contains(" by")) {
			// probably only works in english but lets give it a go
			int positionOfBy = sharedText.indexOf(" by");
			if (positionOfBy >= 0 && sharedText.length() > positionOfBy + 4) {
				String nameCandidate = sharedText.substring(0, positionOfBy).trim();
				int position = nameCandidate.indexOf("through");
				if (position >= 0 && nameCandidate.length() > position + 8) {
					nameCandidate = nameCandidate.substring(position + 8).trim();
				} else {
					position = nameCandidate.indexOf("finished");
					if (position >= 0 && nameCandidate.length() > position + 9) {
						nameCandidate = nameCandidate.substring(position + 9).trim();
					}
				}
				if (nameCandidate.startsWith(progressText)) {
					nameCandidate = nameCandidate.substring(progressText.length() + 1).trim();
				}
				position = nameCandidate.indexOf(recommendText);
				if (position >= 0) {
					nameCandidate = nameCandidate.substring(recommendText.length() + position).trim();
				}
				if (nameCandidate.startsWith("\"")) {
					nameCandidate = nameCandidate.substring(1);
				}
				if (nameCandidate.endsWith("\"")) {
					nameCandidate = nameCandidate.substring(0,nameCandidate.length()-1);
				}
				recommendation.setName(nameCandidate);
				String byCandidate = sharedText.substring(positionOfBy + 4);
				position = byCandidate.indexOf(" on Kindle");
				if (position >= 0) {
					byCandidate = byCandidate.substring(0, position);
				}
				position = byCandidate.indexOf("\n");
				if (position >= 0) {
					byCandidate = byCandidate.substring(0, position);
				}
				position = byCandidate.indexOf(" and I think");
				if (position >= 0) {
					byCandidate = byCandidate.substring(0, position);
				}
				position = byCandidate.indexOf(" Start reading it");
				if (position >= 0) {
					byCandidate = byCandidate.substring(0, position).trim();
				}
				recommendation.setBy(byCandidate);
				recommendation.setCategory("Book");
				return recommendation;
			}
		}
		return null;
	}

}
