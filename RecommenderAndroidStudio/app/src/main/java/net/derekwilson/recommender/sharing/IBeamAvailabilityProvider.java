package net.derekwilson.recommender.sharing;

public interface IBeamAvailabilityProvider {
	boolean isBeamAvailable();
}
