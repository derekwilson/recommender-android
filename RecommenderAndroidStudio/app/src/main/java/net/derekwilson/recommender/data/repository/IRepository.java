package net.derekwilson.recommender.data.repository;

import android.content.ContentValues;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import java.util.List;

import rx.Subscription;
import rx.functions.Action1;

public interface IRepository<T> {
	void close();

	void create(ContentValues newItem);
	void update(IdAndValues item);
	void delete(String id);
	void deleteAll();
	Subscription getById(String id, Action1<List<Recommendation>> responseHandler);
	Subscription getAll(Action1<List<Recommendation>> responseHandler);
}
