package net.derekwilson.recommender.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import net.derekwilson.recommender.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HighlightTextView extends AppCompatTextView {

    private final static int DefaultHighlightTextColour = Color.RED;
    private final static int  DefaultHighlightBackgroundColour = Color.BLACK;

    private String text = null;

    private int highlightTextColour = DefaultHighlightTextColour;
    public void setHighlightedTextColour(int colour) {
        highlightTextColour = colour;
        updateText();
    }

    private int highlightBackgroundColour = DefaultHighlightBackgroundColour;
    public void setHighlightedBackgroundColour(int colour) {
        highlightBackgroundColour = colour;
        updateText();
    }

    private String highlightedText = null;
    public void setHighlightedText(String text) {
        highlightedText = text;
        updateText();
    }

    public HighlightTextView(Context context) {
        super(context);
        Init(context, null, 0);
    }

    public HighlightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Init(context, attrs, 0);
    }

    public HighlightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Init(context, attrs, defStyle);
    }

    private void Init(Context context, AttributeSet attrs, int defStyle)
    {
        LoadAttributes(attrs, defStyle);
    }

    private void LoadAttributes(AttributeSet attrs, int defStyle)
    {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HighlightTextView, defStyle, 0);
        highlightTextColour = a.getColor(R.styleable.HighlightTextView_highlightTextColour, highlightTextColour);
        highlightBackgroundColour = a.getColor(R.styleable.HighlightTextView_highlightBackgroundColour, highlightBackgroundColour);
        a.recycle();
    }

    @Override
    public void setText(CharSequence str, BufferType bufferType) {
        this.text = str.toString();
        updateText();
    }

    private void updateText() {
        SpannableString spannableText = new SpannableString(text);
        List<TextSpan> spans = getHighlightedSpans();
        for (TextSpan span : spans) {
            spannableText.setSpan(new ForegroundColorSpan(highlightTextColour), span.start, span.getEnd(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableText.setSpan(new BackgroundColorSpan(highlightBackgroundColour), span.start, span.getEnd(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        super.setText(spannableText, BufferType.SPANNABLE);
    }

    private List<TextSpan> getHighlightedSpans()  {
        List<TextSpan> result = new ArrayList<>();
        if (text == null || text.isEmpty() || highlightedText == null || highlightedText.isEmpty()) {
            return result;
        }

        String textLower = text.toLowerCase(Locale.getDefault());
        String highlightedTextLower = highlightedText.toLowerCase(Locale.getDefault());
        int startIndex = 0;
        int searchTextLength = highlightedTextLower.length();
        while (startIndex + searchTextLength <= text.length()) {
            int foundIndex = textLower.indexOf(highlightedTextLower, startIndex);
            if (foundIndex < 0) {
                break;
            }
            result.add(new TextSpan(foundIndex, searchTextLength));
            startIndex = foundIndex + searchTextLength;
        }
        return result;
    }


    public class TextSpan {
        public int start = 0;
        public int length = 0;

        public TextSpan(int start, int length) {
            this.start = start;
            this.length = length;
        }

        public int getEnd() {
            return start + length;
        }
    }

}


