package net.derekwilson.recommender.activity.importRecommendation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.BaseActivity;
import net.derekwilson.recommender.fragment.recommendation.RecommendationSelectFragment;
import net.derekwilson.recommender.fragment.utility.OkCancelDialogFragment;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.scopes.DaggerIImportRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.IImportRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.ImportRecommendationModule;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.importrecommendationactivity.IImportRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.importrecommendationactivity.IImportRecommendationActivityView;

import java.util.List;

import javax.inject.Inject;

public class ImportRecommendationActivity extends BaseActivity
		implements
			IImportRecommendationActivityView,
			OkCancelDialogFragment.Listener,
			View.OnClickListener
{
	private static final String TAG_SAVE_DISCARD = "save_discard_tag";

	private static final String TAG_SELECT_FRAGMENT = "select_fragment_tag";

	private IImportRecommendationComponent component;

	@Inject
	protected IImportRecommendationActivityPresenter presenter;

	protected FloatingActionButton fab;
	@Override
	protected void bindControls() {
		super.bindControls();
		fab = findViewById(R.id.fab);
	}

	protected IImportRecommendationComponent getActivityComponent(Activity activity) {
		if (component == null) {
			component = DaggerIImportRecommendationComponent.builder()
					.iApplicationComponent(getApplicationComponent())
					.baseActivityModule(new BaseActivityModule(activity))
					.importRecommendationModule(new ImportRecommendationModule())
					.build();
		}
		return component;
	}

	@Override
	protected void injectDependencies(IApplicationComponent applicationComponent) {
		getActivityComponent(this).inject(this);    // will inject into the base as well as this class
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_import_recommendations;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		toolbarBackButtonNeeded = true;
		super.onCreate(savedInstanceState);
		logger.getCurrentApplicationLogger().debug("ImportRecommendationActivity started");
		presenter.bindView(this);

		List<Recommendation> recommendations = presenter.unpackIntent(getIntent(), getContentResolver());

		if (savedInstanceState == null) {
			// display the recommendations so we can choose which ones to insert
			RecommendationSelectFragment fragment = new RecommendationSelectFragment();
			fragment.setArguments(recommendations);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.fragment_container, fragment, TAG_SELECT_FRAGMENT)
					.commit();
		}
		if (fab != null) {
			fab.setOnClickListener(this);
		}
		presenter.onCreate();
	}

	@Override
	public void onDestroy() {
		presenter.onDestroy();
		presenter.unbindView();
		logger.getCurrentApplicationLogger().debug("ImportRecommendationActivity.onDestroy()");
		super.onDestroy();
	}

	private RecommendationSelectFragment getSelectFragment() {
		return (RecommendationSelectFragment) getSupportFragmentManager().findFragmentByTag(TAG_SELECT_FRAGMENT);
	}

	private void importRecommendations()
	{
		final RecommendationSelectFragment fragment = getSelectFragment();
		if (fragment != null) {
			List<Recommendation> recommendations = fragment.getSelected();
			presenter.importRecommendations(recommendations);
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.fab:
				importRecommendations();
				// the message display will trigger the exit
				break;
		}
	}

	@Override
	public void onBackPressed() {
		showSaveDiscardDialog();
	}

	protected void showSaveDiscardDialog() {
		DialogFragment newFragment = OkCancelDialogFragment.newInstance(
				getString(R.string.save_discard_import_message),
				getString(R.string.ok_cancel_save),
				getString(R.string.ok_cancel_discard)
		);
		newFragment.show(getSupportFragmentManager(), TAG_SAVE_DISCARD);
	}

	@Override
	public void ok(String tag) {
		importRecommendations();
		// the message display will trigger the exit
	}

	@Override
	public void cancel(String tag) {
		endAndGotoMain();
	}

	public void endAndGotoMain() {
		finish();
		gotoMain();
	}

	@Override
	public void end() {
		finish();
	}

	@Override
	public void showError(Throwable throwable) {
		displayMessage(throwable.getMessage());
	}

	@Override
	public void showMessage(int message) {
		displayMessage(getString(message));
	}

	@Override
	public void showImportSuccessMessage(int numberInserted) {
		displayMessage(Integer.toString(numberInserted) + getString(R.string.import_suffix),
				new Snackbar.Callback() {
					@Override
					public void onDismissed(Snackbar snackbar, int event) {
						logger.getCurrentApplicationLogger().debug("ImportRecommendationActivity snackbar onDismissed");
						// wait for the message to disappear
						// we dont care what the event was that triggered the dismissal
						endAndGotoMain();
					}
				}
			);
	}

	@Override
	public void setTitle(int id) {
		super.setTitle(id);
	}
}
