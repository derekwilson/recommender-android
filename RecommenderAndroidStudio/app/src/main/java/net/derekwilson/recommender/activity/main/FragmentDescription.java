package net.derekwilson.recommender.activity.main;

import androidx.fragment.app.Fragment;

public class FragmentDescription {
	private Fragment fragment;
	private String title;
	private Integer itemCount;

	public FragmentDescription(Fragment fragment, String title, Integer itemCount) {
		this.fragment = fragment;
		this.title = title;
		this.itemCount = itemCount;
	}

	public Fragment getFragment() {
		return fragment;
	}

	public String getTitle() {
		return title + " (" + itemCount.toString() + ")";
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
}
