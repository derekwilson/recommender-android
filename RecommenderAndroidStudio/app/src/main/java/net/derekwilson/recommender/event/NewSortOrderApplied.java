package net.derekwilson.recommender.event;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;

public class NewSortOrderApplied {
    private String sortBy;

    public NewSortOrderApplied(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortBy() {
        return sortBy;
    }
}
