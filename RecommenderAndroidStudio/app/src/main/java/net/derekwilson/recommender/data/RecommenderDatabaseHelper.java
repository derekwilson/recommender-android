package net.derekwilson.recommender.data;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.derekwilson.recommender.RecommenderBuildConfig;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.schema.RecommendationTable;
import net.derekwilson.recommender.logging.ILoggerFactory;

import javax.inject.Inject;

public class RecommenderDatabaseHelper extends SQLiteOpenHelper {
	private ILoggerFactory _logger;

	protected SQLiteDatabase _database;

	protected ICrashReporter _crashReporter;

	public static final String DATABASE_FILE_NAME = "recommender.db";
	private static final int DATABASE_VERSION = 4;

	// Schema changes
	//
	// 	Schema Version		Release	Notes
	//
	//	1					n/a		Initial dev version
	//	2							Added default values to the recommendations table
	//	3					v1.3.0	Fixed upgrade transaction
	//  4                   v1.5.0  Added source column

	@Inject
	public RecommenderDatabaseHelper(Context context, ILoggerFactory logger, ICrashReporter crashReporter) {
		super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
		_logger = logger;
		_crashReporter = crashReporter;
	}

	private void initialiseData(SQLiteDatabase database) {
		if (RecommenderBuildConfig.PRODUCTION) {
			return; // we do not plant example data in the release build
		}
		RecommendationTable.onInitialiseData(database);
	}

	public static boolean isSchemaUpgradeThroughVersion(int oldVersion, int newVersion, int versionToCheckFor) {
		return
				oldVersion < versionToCheckFor &&
						newVersion >= versionToCheckFor;
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		_logger.getCurrentApplicationLogger().debug("Creating database");
		RecommendationTable.onCreate(database);

		initialiseData(database);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		_logger.getCurrentApplicationLogger().debug("Upgrading database from version {} to {}", oldVersion, newVersion);
		RecommendationTable.onUpgrade(database, oldVersion, newVersion, _logger);

		initialiseData(database);
	}

	public SQLiteDatabase getDatabase() {
		return _database;
	}

	public SQLiteDatabase open() throws SQLException {
		_database = getWritableDatabase();
		//database.setForeignKeyConstraintsEnabled(true);
		if (!_database.isReadOnly()) {
			// Enable foreign key constraints
			_database.execSQL("PRAGMA foreign_keys=ON;");
		}
		return _database;
	}

	public void close() {
		_database.close();
	}

	public void openAndCloseDatabase() {
		synchronized (this) {
			try {
				_logger.getCurrentApplicationLogger().debug("DB opened");
				open();
			} catch (Exception e) {
				_logger.getCurrentApplicationLogger().error("DB Error ", e);
				_crashReporter.logNonFatalException(e);
			} finally {
				close();
				_logger.getCurrentApplicationLogger().debug("DB closed");
			}
		}
	}
}

