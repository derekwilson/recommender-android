package net.derekwilson.recommender.rest.wikipedia;

import net.derekwilson.recommender.rest.wikipedia.model.QueryResult;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface IWikipediaService {
	@GET("/w/api.php")
	public Observable<QueryResult> action(
			@Query("action") String action,
			@Query("prop") String prop,
			@Query("format") String format,
			@Query("exchars") int chars,
			@Query("explaintext") int explaintext,
			@Query("titles") String titles
	);
}
