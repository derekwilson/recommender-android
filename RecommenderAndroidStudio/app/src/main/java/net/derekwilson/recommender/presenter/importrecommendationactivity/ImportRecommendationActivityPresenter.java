package net.derekwilson.recommender.presenter.importrecommendationactivity;

import android.content.ContentResolver;
import android.content.Intent;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.RecommendationAddedEvent;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.BasePresenter;
import net.derekwilson.recommender.receiving.IInserter;
import net.derekwilson.recommender.receiving.IIntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromContentResolver;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromNfcBeam;
import net.derekwilson.recommender.receiving.IntentContentType;

import java.util.List;

import javax.inject.Inject;

public class ImportRecommendationActivityPresenter extends BasePresenter<IImportRecommendationActivityView> implements IImportRecommendationActivityPresenter {

	private ILoggerFactory logger;
	private IIntentContentTypeFinder intentTypeFinder;
	private IIntentUnpackerFromContentResolver contentUnpacker;
	private IIntentUnpackerFromNfcBeam nfcUnpacker;
	private IInserter inserter;
	private IEventBus eventBus;
	private IEventLogger eventLogger;

	@Inject
	public ImportRecommendationActivityPresenter(ILoggerFactory logger, IIntentContentTypeFinder intentTypeFinder, IIntentUnpackerFromContentResolver contentUnpacker, IIntentUnpackerFromNfcBeam nfcUnpacker, IInserter inserter, IEventBus eventBus, IEventLogger eventLogger) {
		this.logger = logger;
		this.intentTypeFinder = intentTypeFinder;
		this.contentUnpacker = contentUnpacker;
		this.nfcUnpacker = nfcUnpacker;
		this.inserter = inserter;
		this.eventBus = eventBus;
		this.eventLogger = eventLogger;
	}

	@Override
	public List<Recommendation> unpackIntent(Intent intent, ContentResolver contentResolver) {
		List<Recommendation> recommendations = null;
		IntentContentType intentType = intentTypeFinder.getTypeOfContent(intent);
		logger.getCurrentApplicationLogger().debug("unpack from intent type {}", intentType);
		switch (intentType) {
			case ContentUri:
			case FileUri:
				recommendations = contentUnpacker.getRecommendationsFromContent(intent, contentResolver);
				break;
			case RecommenderNfcBeam:
				recommendations = nfcUnpacker.getRecommendationsFromPayload(intent);
				break;
		}

		if (recommendations == null || recommendations.size() < 1) {
			view.showMessage(R.string.cannot_import);
			view.end();
		}

		if (allRecommendationsAreOfType(RecommendationType.TRY_THIS, recommendations)) {
			view.setTitle(R.string.title_activity_add_try_this);
		} else {
			view.setTitle(R.string.title_activity_import);
		}

		return recommendations;
	}

	private boolean allRecommendationsAreOfType(RecommendationType type, List<Recommendation> recommendations) {
		if (recommendations == null || recommendations.size() < 1) {
			return false;
		}
		for (Recommendation thisRecommendation : recommendations) {
			if (thisRecommendation.getType() != type) {
				return false;
			}
		};
		return true;
	}

	@Override
	public void importRecommendations(List<Recommendation> recommendations) {
		int numberInserted = inserter.insertRecommendations(recommendations);
		eventLogger.logReceivedRecommendations(recommendations, "import");
		eventBus.send(new RecommendationAddedEvent(RecommendationType.TRY_THIS));
		view.showImportSuccessMessage(numberInserted);
	}
}
