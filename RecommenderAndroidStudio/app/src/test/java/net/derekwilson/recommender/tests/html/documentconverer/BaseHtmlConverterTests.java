package net.derekwilson.recommender.tests.html.documentconverer;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.html.DocumentToRecommendationConverter;
import net.derekwilson.recommender.tests.BaseJunitTests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;

import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Mockito.mock;

public class BaseHtmlConverterTests extends BaseJunitTests {

	protected Document testDocument;

	// object under test
	protected DocumentToRecommendationConverter converter;

	protected ICrashReporter mockCrashReporter = mock(ICrashReporter.class);

	@Before
	public void setup() {
		converter = new DocumentToRecommendationConverter(mockLoggerFactory, mockCrashReporter);
	}

	protected Document loadHtml(String filename) throws IOException {
		InputStream stream = getClass().getClassLoader().getResourceAsStream("html/" + filename);
		return Jsoup.parse(stream, "UTF-8", "");
	}
}


