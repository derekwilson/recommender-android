# Recommender Release Process #

This is the process for building, testing and deploying Recommender to the app store.

## Prerequisite

1. **LocalOnly folder**. This folder contains the files needed to sign a release APK.
1. **A build machine**. Either a CI build server or your local developer machine can be used.

### What is the folder "LocalOnly"?

This folder contains the files needed to sign a release APK. The folder is present on developer machines as well as any build servers however it is not checked into GIT, hence the name. This folder is not needed to use Android Studio or Gradle to produce debug build of the app, it is only used with the release build type.

#### What files need to be in the folder?

The folder needs to contain.

1. derek.keystore - this is the upload signing key
1. keystore.proerties - this contains the gradle build configurations for release signing
1. appcenter.proerties - this contains the AppCenter configurations for logging and analytics

#### What happens if I dont have "LocalOnly"?

You will be able to build the debug variant as well as run all the tests, the only thing you will not be able to do is build the release APK, if you try you will see this error

```
FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':app:validateSigningRelease'.
> Keystore file not set for signing config release
```

Note: you will need to comment out the call to `AppCenter.start()` even for debug builds as you don't have the secrets needed to send data to AppCenter


### Command line tools

If you want to build and work with the code form the command line you will need to add the following locations to your path

Windows

1. Android SDK =  `%localappdata%\Android\sdk\platform-tools`
1. Java JDK (Embedded) = `"%ProgramFiles%\Android\Android Studio\jre\bin"`
or
1. Java JDK (Stand Alone) = `"%ProgramFiles%\Java\jdk1.8.0_92\bin"`

Mac

1. Android SDK = `~/Library/Android/sdk/platform-tools`
1. Java JDK (Embedded) = `/Applications/Android Studio.app/Contents/jre/jdk/Contents/Home`
or
1. Java JDK (Stand Alone) = `/Library/Java/JavaVirtualMachines/jdk1.8.0_172.jdk/Contents/Home`

## Deployment Process

The process to deploy a new app is as follows

1. Ensure all code to be deployed is in the `master` branch
1. Create a new branch called `release/1.0.1` from `master` locally (where 1.0.1 is the version name we are releasing)
1. Add release notes to `AndroidSupport\releases\README.md`
1. Edit the file `app/build.gradle`. In the section `android -> defaultConfig` we need to edit the `versionName` and `versionCode` values
1. CD to the folder `RecommenderAndroidStudio`
1. Ensure that all files have been committed before building the release APK.
1. Build the release APK and AAB, essentially this means running `build.cmd` or `build_set_jdk` - fix any broken tests
1. Run the script `CopyBuild` 
1. CD to the folder `..\AndroidSupport\deploy` 
1. Install the APK like this `adb install -r recommender-1.6.0-34-b208c00-release.apk`, the APK name will reflect the version and the commit.
1. Run the smoke tests on the device. If the test fails check the existing production app if the problem already exists then log the issue and continue, if it is a new problem then you will need to fix it or abandon the release.
1. Copy the APK to the release archive `AndroidSupport\releases\vX.Y.Z`
1. Upload the APK to the [play store console][play-console-url] or [amazon app store console][amazon-console-url]
1. Refresh the release notes
1. Release to Production
1. Tag the commit with the version number, for example `1.0.1(2)`, which is `versionName(versionCode)`
1. Merge into `master` locally and push to the repo, ensure the tag is pushed by using `git push origin "1.0.1(2)"`
1. When approved, check the correct app is delivered to a handset (goto Settings page and look at the version)

## Smoke Tests

Check the following, estimated execution time should be less than 5 minutes

1. If its a first time install then grant access to external storage
1. Goto Settings, does the Version match this build?
1. Goto Open-source Licenses
1. Goto Privacy statement
1. Goto Help
1. Category Filter should only have `None`
1. Main screen - both tabs should have zero recommendations with a message to add or restore
1. Select Backup - should get an error
1. Select Backup and Share All - should get an error
1. Select Restore and select a file - check the import was successful
1. Check categories are populated
1. Filter by multiple categories
1. Search
1. Clear filter by selecting `None`
1. Search
1. Select a recommendation and share it
1. Check categories, who and website buttons
1. Back to main screen
1. Adjust sorting to be LAST UPDATED, in settings
1. Select a recommendation
1. Select save
1. Check it jumps to the top of the list
1. Select multiple
1. Move them to the other tab
1. select multiple 
1. Share them
1. select multiple
1. Delete them
1. Delete all
1. Goto a web browser and share a recommendation into the app
1. Add a new entry for who
1. Save it
1. Check it gets created and check the category bottomsheet is populated
1. Select Backup and Share all - email to self - check its correct
1. Delete All
1. Open backup from email and import
1. Goto [Crashlytics][crashlytics-url] and check for crashes
1. Goto [Mixpanel][analytics-url] and check that analytics are arriving


[play-console-url]:		https://play.google.com/apps/publish
[crashlytics-url]:      https://console.firebase.google.com/project/recommender-9956a/crashlytics/app/android:net.derekwilson.recommender/issues?state=all&time=last-seven-days&types=crash&tag=all&sort=eventCount
[analytics-url]:        https://eu.mixpanel.com/project/3326973/view/3834997/app/boards#id=7416805
[amazon-console-url]:	https://developer.amazon.com/apps-and-games/console/apps/list.html

