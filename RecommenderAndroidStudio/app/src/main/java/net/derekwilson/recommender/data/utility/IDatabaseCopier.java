package net.derekwilson.recommender.data.utility;

public interface IDatabaseCopier {
	String getDestinationPathname();
	boolean copyDatabaseFile(String fullPathToDestination);
}
