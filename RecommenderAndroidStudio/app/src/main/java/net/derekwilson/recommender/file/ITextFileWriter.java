package net.derekwilson.recommender.file;

public interface ITextFileWriter {
	void writeStringToFile(String filename, String text);
}
