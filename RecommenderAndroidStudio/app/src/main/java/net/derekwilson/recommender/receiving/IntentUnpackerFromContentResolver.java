package net.derekwilson.recommender.receiving;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.json.IImporter;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;

import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

public class IntentUnpackerFromContentResolver implements IIntentUnpackerFromContentResolver {

	private ILoggerFactory logger;
	private IImporter importer;
	private ICrashReporter crashReporter;

	@Inject
	public IntentUnpackerFromContentResolver(ILoggerFactory logger, IImporter importer, ICrashReporter crashReporter) {
		this.logger = logger;
		this.importer = importer;
		this.crashReporter = crashReporter;
	}

	/**
	 * gets recommendations from a file using an importer, the file is passed via a content resolver in an intent
	 *
	 * @param intent
	 * @param contentResolver
	 * @return recommendations or null
	 */
	@Override
	public List<Recommendation> getRecommendationsFromContent(Intent intent, ContentResolver contentResolver) {
		List<Recommendation> recommendations = null;
		Uri intentUri = intent.getData();
		if (intentUri != null) {
			try {
				InputStream inputStream = contentResolver.openInputStream(intentUri);
				if (inputStream == null) {
					return null;
				}

				recommendations = importer.importRecommendations(inputStream);
				inputStream.close();
			} catch (Exception e) {
				logger.getCurrentApplicationLogger().error("Error getting intent content", e);
				crashReporter.logNonFatalException(e);
			} finally {
			}
		}
		return recommendations;
	}
}

