package net.derekwilson.recommender.view;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class SnackbarHelper {

    private static final int SNACKBAR_MAX_LINES = 8; // Increase maximum SnackBar line limit above 2

    public static boolean displayMessageAsSnackbar(Activity activity, String message) {
        return displayMessageAsSnackbar(activity, message, null);
    }

    public static boolean displayMessageAsSnackbar(Activity activity, String message, Snackbar.Callback callback) {
        if (activity == null) {
            return false;
        }

        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
        if (viewGroup == null) {
            return false;
        }

        Snackbar snackbar = Snackbar.make(viewGroup, message, Snackbar.LENGTH_LONG);
        if (snackbar == null) {
            return false;
        }
        TextView textView = (TextView) snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        if (textView == null) {
            return false;
        }
        textView.setMaxLines(SNACKBAR_MAX_LINES);
        if (callback != null) {
            snackbar.addCallback(callback);
        }
        snackbar.show();
        return true;
    }

}
