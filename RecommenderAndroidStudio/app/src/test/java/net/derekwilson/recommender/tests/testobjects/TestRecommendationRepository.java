package net.derekwilson.recommender.tests.testobjects;

import android.content.ContentValues;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.data.repository.IdAndValues;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import java.util.List;

import rx.Subscription;
import rx.functions.Action1;

import static org.junit.Assert.assertNotNull;

public class TestRecommendationRepository implements IRecommendationRepository {
	Action1<List<Recommendation>> recommendationResponseHandler = null;
	Action1<List<String>> stringResponseHandler = null;

	public void callRecommendationResponseHandler(List<Recommendation> recommendations) {
		assertNotNull("response handler has not been set by the code under test but we are attempting to call it", recommendationResponseHandler);
		recommendationResponseHandler.call(recommendations);
	}

	public void callStringResponseHandler(List<String> strings) {
		assertNotNull("response handler has not been set by the code under test but we are attempting to call it", stringResponseHandler);
		stringResponseHandler.call(strings);
	}

	@Override
	public Subscription getByType(RecommendationType type, SortBy sortby, Action1<List<Recommendation>> responseHandler) {
		this.recommendationResponseHandler = responseHandler;
		return null;
	}

	@Override
	public Subscription getByTypeWithFilter(RecommendationType type, SortBy sortby, IFilterCollection filters, Action1<List<Recommendation>> responseHandler) {
		this.recommendationResponseHandler = responseHandler;
		return null;
	}

	@Override
	public Subscription getCategories(Action1<List<String>> responseHandler) {
		this.stringResponseHandler = responseHandler;
		return null;
	}

	@Override
	public Subscription getSources(Action1<List<String>> responseHandler) {
		this.stringResponseHandler = responseHandler;
		return null;
	}

	@Override
	public void close() {

	}

	@Override
	public void create(ContentValues newItem) {

	}

	@Override
	public void update(IdAndValues item) {

	}

	@Override
	public void delete(String id) {

	}

	@Override
	public void deleteAll() {

	}

	@Override
	public Subscription getById(String id, Action1<List<Recommendation>> responseHandler) {
		this.recommendationResponseHandler = responseHandler;
		return null;
	}

	@Override
	public Subscription getAll(Action1<List<Recommendation>> responseHandler) {
		this.recommendationResponseHandler = responseHandler;
		return null;
	}
}
