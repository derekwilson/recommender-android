package net.derekwilson.recommender.sharing;

import android.content.Intent;
import android.net.Uri;

public class IntentShareResult {
	public int NumberOfRecommendations;
	public Intent SendIntent;
	public Uri[] FileUris;
}

