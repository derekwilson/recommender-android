package net.derekwilson.recommender.event;

import rx.Observable;

/**
 * this is the event bus for the app
 * it could be implemented using any library for example otto or eventbus
 * but for now we are using a light weight hand rolled RxAndroid implementation
 */
public interface IEventBus {
	void send(Object event);
	Observable<Object> toObservable();
}
