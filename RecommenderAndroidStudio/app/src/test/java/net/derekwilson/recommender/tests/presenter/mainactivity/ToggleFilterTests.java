package net.derekwilson.recommender.tests.presenter.mainactivity;

import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.IFilterCollection;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ToggleFilterTests extends PresenterTestsSetup {

	@Before
	public void setUp() {
		setupPresenter(true, true);
	}

	private void verifyFilterCollection(IFilterCollection collection, int numberSelected, List<String> selected) {
		assertThat("incorrect number of selected filters", collection.getNumberOfSelectedFilters(), is(numberSelected));
		for (String filterName : selected) {
			assertThat("filter should be selected: " + filterName, collection.isFilterSelected(filterName), is(true));
		}
	}

	@Test
	public void we_can_select_one_category() {
		// arrange
		presenter.onCreate();
		List<String> categories = getTestCategories();
		realRepository.callStringResponseHandler(categories);

		// act
		presenter.toggleFilterSelectionByName("2");

		// assert
		ArrayList<String> selected = new ArrayList<>(2);
		selected.add("2");

		// dont forget that there is only one filtercollection so it will br in its final state after all the capturers
		// we call showCategoryFilter once to setup the initial list
		ArgumentCaptor<FilterCollection> argument1 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(1)).showCategoryFilter(argument1.capture());
		verifyFilterCollection(argument1.getValue(), 1, selected);

		// we call setCategoryFilterUiState once when the state changes
		ArgumentCaptor<FilterCollection> argument2 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(1)).setCategoryFilterUiState(argument2.capture());
		verifyFilterCollection(argument2.getValue(), 1, selected);

		// applyFIlter gets called by onCreate and toggle
		ArgumentCaptor<FilterCollection> argument3 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(2)).applyFilterToAllTabs(argument3.capture());
		verifyFilterCollection(argument3.getValue(), 1, selected);
	}

	@Test
	public void we_can_select_two_categories() {
		// arrange
		presenter.onCreate();
		List<String> categories = getTestCategories();
		realRepository.callStringResponseHandler(categories);

		// act
		presenter.toggleFilterSelectionByName("2");
		presenter.toggleFilterSelectionByName("4");

		// assert
		ArrayList<String> selected = new ArrayList<>(2);
		selected.add("2");
		selected.add("4");

		// dont forget that there is only one filtercollection so it will br in its final state after all the capturers
		// we call showCategoryFilter once to setup the initial list
		ArgumentCaptor<FilterCollection> argument1 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(1)).showCategoryFilter(argument1.capture());
		verifyFilterCollection(argument1.getValue(), 2, selected);

		// we call setCategoryFilterUiState once when the state changes
		ArgumentCaptor<FilterCollection> argument2 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(2)).setCategoryFilterUiState(argument2.capture());
		verifyFilterCollection(argument2.getValue(), 2, selected);

		// applyFIlter gets called by onCreate and toggle
		ArgumentCaptor<FilterCollection> argument3 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(3)).applyFilterToAllTabs(argument3.capture());
		verifyFilterCollection(argument3.getValue(), 2, selected);
	}

	@Test
	public void we_can_clear_the_selections() {
		// arrange
		presenter.onCreate();
		List<String> categories = getTestCategories();
		realRepository.callStringResponseHandler(categories);
		presenter.toggleFilterSelectionByName("2");
		presenter.toggleFilterSelectionByName("4");

		// act
		presenter.clearFilter();

		// assert
		ArrayList<String> selected = new ArrayList<>(2);

		// dont forget that there is only one filtercollection so it will br in its final state after all the capturers
		// we call showCategoryFilter once to setup the initial list
		ArgumentCaptor<FilterCollection> argument1 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(1)).showCategoryFilter(argument1.capture());
		verifyFilterCollection(argument1.getValue(), 0, selected);

		// we call setCategoryFilterUiState once when the state changes
		ArgumentCaptor<FilterCollection> argument2 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(3)).setCategoryFilterUiState(argument2.capture());
		verifyFilterCollection(argument2.getValue(), 0, selected);

		// applyFIlter gets called by onCreate and toggle and clear
		ArgumentCaptor<FilterCollection> argument3 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(4)).applyFilterToAllTabs(argument3.capture());
		verifyFilterCollection(argument3.getValue(), 0, selected);
	}

}
