package net.derekwilson.recommender.tests.model.filtercollection;

import net.derekwilson.recommender.model.RecommendationType;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GetSelectedAsArrayTests extends TestSetup {

	@Test
	public void empty_gives_replacement_params() {
		// arrange

		// act
		String[] array = collection.getSelectedAsArray(RecommendationType.TRY_THIS);

		// assert
		assertThat(array.length, is(1));
		assertThat(array[0], is("1"));
	}

	@Test
	public void none_selected_gives_replacement_params() {
		// arrange
		collection.setFilters(getFilters(true, true));

		// act
		String[] array = collection.getSelectedAsArray(RecommendationType.TRY_THIS);

		// assert
		assertThat(array.length, is(1));
		assertThat(array[0], is("1"));
	}

	@Test
	public void one_selected_gives_replacement_params() {
		// arrange
		collection.setFilters(getFilters(true, true));
		collection.setFilterSelectedByName("FILTER 1", true);

		// act
		String[] array = collection.getSelectedAsArray(RecommendationType.TRY_THIS);

		// assert
		assertThat(array.length, is(2));
		assertThat(array[0], is("1"));
		assertThat(array[1], is("FILTER 1"));
	}

	@Test
	public void two_selected_gives_replacement_params() {
		// arrange
		collection.setFilters(getFilters(true, true));
		collection.setFilterSelectedByName("FILTER 1", true);
		collection.setFilterSelectedByName("FILTER 2", true);

		// act
		String[] array = collection.getSelectedAsArray(RecommendationType.TRY_THIS);

		// assert
		assertThat(array.length, is(3));
		assertThat(array[0], is("1"));
		assertThat(array[1], is("FILTER 1"));
		assertThat(array[2], is("FILTER 2"));
	}

	@Test
	public void two_selected_and_nofilter_gives_replacement_params() {
		// arrange
		collection.setFilters(getFilters(true, true));
		collection.setFilterSelectedByName("NOFILTER", true);
		collection.setFilterSelectedByName("FILTER 1", true);
		collection.setFilterSelectedByName("FILTER 2", true);

		// act
		String[] array = collection.getSelectedAsArray(RecommendationType.TRY_THIS);

		// assert
		assertThat(array.length, is(3));
		assertThat(array[0], is("1"));
		assertThat(array[1], is("FILTER 1"));
		assertThat(array[2], is("FILTER 2"));
	}

}
