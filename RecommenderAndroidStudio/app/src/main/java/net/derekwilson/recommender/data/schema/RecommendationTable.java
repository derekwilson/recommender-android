package net.derekwilson.recommender.data.schema;

import android.database.sqlite.SQLiteDatabase;

import net.derekwilson.recommender.data.RecommenderDatabaseHelper;
import net.derekwilson.recommender.logging.ILoggerFactory;

public class RecommendationTable {
	// Database table
	public static final String TABLE_RECOMMENDATION = "recommendation";
	public static final String TABLE_RECOMMENDATION_BACKUP = "recommendation_backup";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TYPE = "type";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_BY = "by";
	public static final String COLUMN_CATEGORY = "category";
	public static final String COLUMN_UPDATED = "updated";
	public static final String COLUMN_NOTES = "notes";
	public static final String COLUMN_URI = "uri";
	public static final String COLUMN_SOURCE = "source";

	// used to find the "IN" clause values so we can dynamically generate them
	public static final String INSERT_POINT = "--INSERT--";

	// used to find the "SORT BY" clause parameters so we can dynamically change the sorting
	public static final String SORT_INSERT_POINT = "--INSERT-SORT_PARAM--";

	// Database creation SQL statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_RECOMMENDATION
			+ "("
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_TYPE + " integer not null, "
			+ COLUMN_NAME + " varchar(255) not null, "
			+ COLUMN_BY + " varchar(255) not null default '', "
			+ COLUMN_CATEGORY + " varchar(255) not null default '', "
			+ COLUMN_UPDATED + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
			+ COLUMN_NOTES + " varchar(255) not null default '', "
			+ COLUMN_URI + " varchar(255) not null default '', "
			+ COLUMN_SOURCE + " varchar(255) not null default '' "
			+ ");";

	public static final String LIST_RECOMMENDATIONS_BY_TYPE_QUERY = "SELECT * "
			+ ", (strftime('%s', " + COLUMN_UPDATED + ") * 1000) AS " + COLUMN_UPDATED
			+ " FROM "
			+ TABLE_RECOMMENDATION
			+ " WHERE "
			+ COLUMN_TYPE
			+ " = ? "
			+ " ORDER BY "     // we need to add the parameter for the sort
			+ SORT_INSERT_POINT
			;

	public static final String LIST_RECOMMENDATIONS_BY_TYPE_WITH_CATEGORY_FILTER_QUERY = "SELECT * "
			+ ", (strftime('%s', " + COLUMN_UPDATED + ") * 1000) AS " + COLUMN_UPDATED
			+ " FROM "
			+ TABLE_RECOMMENDATION
			+ " WHERE "
			+ COLUMN_TYPE
			+ " = ?  and "
			+ COLUMN_CATEGORY
			+ " IN ( "
			+ INSERT_POINT
			+ " ) "            // we need to add a ? for each of the selected filters
			+ " ORDER BY "     // we need to add the parameter for the sort
			+ SORT_INSERT_POINT
			;

	public static final String LIST_RECOMMENDATIONS_BY_TYPE_WITH_SEARCH_QUERY = "SELECT * "
			+ ", (strftime('%s', " + COLUMN_UPDATED + ") * 1000) AS " + COLUMN_UPDATED
			+ " FROM "
			+ TABLE_RECOMMENDATION
			+ " WHERE "
			+ COLUMN_TYPE
			+ " = ?  and "
			+ COLUMN_NAME
			+ " LIKE '%' || ? || '%' "
			+ " ORDER BY "     // we need to add the parameter for the sort
			+ SORT_INSERT_POINT
			;

	public static final String LIST_RECOMMENDATIONS_BY_TYPE_WITH_CATEGORY_FILTER_AND_SEARCH_QUERY = "SELECT * "
			+ ", (strftime('%s', " + COLUMN_UPDATED + ") * 1000) AS " + COLUMN_UPDATED
			+ " FROM "
			+ TABLE_RECOMMENDATION
			+ " WHERE "
			+ COLUMN_TYPE
			+ " = ?  and "
			+ COLUMN_NAME
			+ " LIKE '%' || ? || '%' and "
			+ COLUMN_CATEGORY
			+ " IN ( "
			+ INSERT_POINT
			+ " ) "            // we need to add a ? for each of the selected filters
			+ " ORDER BY "     // we need to add the parameter for the sort
			+ SORT_INSERT_POINT
			;

	public static final String LIST_RECOMMENDATIONS_BY_ID_QUERY = "SELECT * "
			+ ", (strftime('%s', " + COLUMN_UPDATED + ") * 1000) AS " + COLUMN_UPDATED
			+ " FROM "
			+ TABLE_RECOMMENDATION
			+ " WHERE "
			+ COLUMN_ID
			+ " = ? "
			+ " ORDER BY "
			+ COLUMN_NAME
			+ " ASC";

	public static final String LIST_RECOMMENDATIONS = "SELECT * "
			+ ", (strftime('%s', " + COLUMN_UPDATED + ") * 1000) AS " + COLUMN_UPDATED
			+ " FROM "
			+ TABLE_RECOMMENDATION
			+ " ORDER BY "
			+ COLUMN_NAME
			+ " ASC";

	public static final String LIST_RECOMMENDATION_CATEGORIES = "SELECT DISTINCT " + COLUMN_CATEGORY
			+ " FROM "
			+ TABLE_RECOMMENDATION
			+ " ORDER BY "
			+ COLUMN_CATEGORY
			+ " ASC";

	public static final String LIST_RECOMMENDATION_SOURCES = "SELECT DISTINCT " + COLUMN_SOURCE
			+ " FROM "
			+ TABLE_RECOMMENDATION
			+ " ORDER BY "
			+ COLUMN_SOURCE
			+ " ASC";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion, ILoggerFactory _logger) {
		if (RecommenderDatabaseHelper.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 3)) {
			_logger.getCurrentApplicationLogger().debug("adding default values to the recommendation table");
			// version 2 - added default values
			// no ALTER COLUMN in SQLite so we have to go the long way around
			database.beginTransaction();
			try {
				database.execSQL(
						"ALTER TABLE " + TABLE_RECOMMENDATION + " RENAME TO "
								+ TABLE_RECOMMENDATION_BACKUP + ";"
				);
				database.execSQL(
						"UPDATE " + TABLE_RECOMMENDATION_BACKUP + " SET "
								+ COLUMN_BY + " = '' where " + COLUMN_BY + " is null;"
				);
				database.execSQL(
						"UPDATE " + TABLE_RECOMMENDATION_BACKUP + " SET "
								+ COLUMN_CATEGORY + " = '' where " + COLUMN_CATEGORY + " is null;"
				);
				database.execSQL(
						"UPDATE " + TABLE_RECOMMENDATION_BACKUP + " SET "
								+ COLUMN_NOTES + " = '' where " + COLUMN_NOTES + " is null;"
				);
				database.execSQL(
						"UPDATE " + TABLE_RECOMMENDATION_BACKUP + " SET "
								+ COLUMN_URI + " = '' where " + COLUMN_URI + " is null;"
				);
				database.execSQL(DATABASE_CREATE);  // create the new style table
				database.execSQL(
						"INSERT INTO " + TABLE_RECOMMENDATION + " SELECT _id,type,name,by,category,updated,notes,uri from "
								+ TABLE_RECOMMENDATION_BACKUP + ";"
				);
				database.execSQL(
						"DROP TABLE "
								+ TABLE_RECOMMENDATION_BACKUP + ";"
				);
				database.setTransactionSuccessful();
			}
			finally {
				database.endTransaction();
			}
		}
		if (RecommenderDatabaseHelper.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 4)) {
			_logger.getCurrentApplicationLogger().debug("adding source column recommendation table");
			database.execSQL(
					"ALTER TABLE " + TABLE_RECOMMENDATION + " ADD COLUMN "
							+ COLUMN_SOURCE + " varchar(255) not null default ''"
			);
		}
	}

	public static void onInitialiseData(SQLiteDatabase database) {
		database.execSQL("INSERT or REPLACE INTO " + TABLE_RECOMMENDATION + "(" + COLUMN_ID + "," + COLUMN_TYPE + "," + COLUMN_NAME + "," + COLUMN_BY + "," + COLUMN_CATEGORY + "," + COLUMN_URI + "," + COLUMN_SOURCE + ")" +
				" VALUES(1,0,'Blood on the Tracks','Bob Dylan','CD','http://en.wikipedia.org/wiki/Blood_on_the_Tracks','builtin')");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_RECOMMENDATION + "(" + COLUMN_ID + "," + COLUMN_TYPE + "," + COLUMN_NAME + "," + COLUMN_BY + "," + COLUMN_CATEGORY + "," + COLUMN_URI + "," + COLUMN_SOURCE + ")" +
				" VALUES(2,0,'Brazil','Terry Gillian','Film','http://en.wikipedia.org/wiki/Brazil_%281985_film%29','builtin')");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_RECOMMENDATION + "(" + COLUMN_ID + "," + COLUMN_TYPE + "," + COLUMN_NAME + "," + COLUMN_BY + "," + COLUMN_CATEGORY + "," + COLUMN_URI + "," + COLUMN_SOURCE + ")" +
				" VALUES(3,0,'Contacts2go',null,'Shop','http://www.contacts2go.co.nz','builtin')");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_RECOMMENDATION + "(" + COLUMN_ID + "," + COLUMN_TYPE + "," + COLUMN_NAME + "," + COLUMN_BY + "," + COLUMN_CATEGORY + "," + COLUMN_URI + "," + COLUMN_SOURCE + ")" +
				" VALUES(4,0,'Torpor',null,'Restaurant','http://plimmerton.org.nz/business-directory/cafes-and-restaurants/topor-bistro-bar/','builtin')");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_RECOMMENDATION + "(" + COLUMN_ID + "," + COLUMN_TYPE + "," + COLUMN_NAME + "," + COLUMN_BY + "," + COLUMN_CATEGORY + "," + COLUMN_URI + "," + COLUMN_SOURCE + ")" +
				" VALUES(5,0,'ES File Explorer',null,'App','https://play.google.com/store/apps/details?id=com.estrongs.android.pop','builtin')");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_RECOMMENDATION + "(" + COLUMN_ID + "," + COLUMN_TYPE + "," + COLUMN_NAME + "," + COLUMN_BY + "," + COLUMN_CATEGORY + "," + COLUMN_URI + "," + COLUMN_SOURCE + ")" +
				" VALUES(6,0,'Wind waves and weather',null,'WebSite','http://www.windfinder.com/forecast/wellington','builtin')");
	}
}
