package net.derekwilson.recommender.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum RecommendationType implements Parcelable {
	RECOMMENDATION(0),
	TRY_THIS(1);

	private final int value;

	private RecommendationType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public String getValueAsString() {
		return String.valueOf(value);
	}

	public RecommendationType toggleType() {
		// this only works while there are two types
		if (value == 0) {
			return RecommendationType.TRY_THIS;
		}
		return RecommendationType.RECOMMENDATION;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(ordinal());
	}

	public static final Creator<RecommendationType> CREATOR = new Creator<RecommendationType>() {
		@Override
		public RecommendationType createFromParcel(final Parcel source) {
			return RecommendationType.values()[source.readInt()];
		}

		@Override
		public RecommendationType[] newArray(final int size) {
			return new RecommendationType[size];
		}
	};
}
