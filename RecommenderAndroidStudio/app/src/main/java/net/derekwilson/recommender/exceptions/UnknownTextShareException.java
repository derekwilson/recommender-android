package net.derekwilson.recommender.exceptions;

public class UnknownTextShareException extends Exception {
	public UnknownTextShareException(String text) {
		super("Text: " + text);
	}
}

