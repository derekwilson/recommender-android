package net.derekwilson.recommender.tests.presenter.editrecommendationactivity;

import android.content.Intent;

import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.IntentShareResult;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;

import static junit.framework.TestCase.fail;
import static net.derekwilson.recommender.tests.testobjects.Matchers.orNull;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ShareTests extends PresenterTestSetup {
	@Captor
	private ArgumentCaptor<Recommendation> emailSharerCaptor;

	private static final String NICKNAME = "TEST NICKNAME";
	private Recommendation recomendation = new Recommendation();

	@Before
	public void setUp() {
		setupPresenter(true);
		when(mockView.getRecommendationType()).thenReturn(RecommendationType.RECOMMENDATION);
		when(mockView.getRecommendationFromPassedIntent()).thenReturn(recomendation);
		when(mockPreferences.getPreferenceString(orNull(anyString()))).thenReturn(NICKNAME);
		presenter.onCreate();

		attachCaptor();
	}

	private void attachCaptor() {
		// so we can use generic captors
		MockitoAnnotations.initMocks(this);
		try {
			when(mockEmailSharer.getIntent(emailSharerCaptor.capture())).thenReturn(new IntentShareResult());
		} catch (ExternalStorageException e) {
			fail();
		}
	}

	@Test
	public void sharing_sets_source() {
		// arrange
		recomendation.setSource("TEST SOURCE");

		// act
		presenter.shareRecommendation();

		// assert
		Recommendation recommendationToShare = getAllCapturedValuesAndAssertExpectedCount(emailSharerCaptor,1).get(0);
		assertThat("source has not been set", recommendationToShare.getSource(), is(NICKNAME));
	}

	@Test
	public void sharing_logs_event() {
		// arrange

		// act
		presenter.shareRecommendation();

		// assert
		verify(mockEventLogger, times(1)).logShareRecommendation(any(Recommendation.class),anyString());
	}

	@Test
	public void sharing_starts_intent() {
		// arrange

		// act
		presenter.shareRecommendation();

		// assert
		verify(mockView, times(1)).startIntent(orNull(any(Intent.class)), anyInt());
	}
}
