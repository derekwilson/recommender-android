package net.derekwilson.recommender.receiving;

import android.content.Intent;

import net.derekwilson.recommender.model.Recommendation;

public interface IIntentUnpackerFromText {
	Recommendation getRecommendationFromText(Intent intent);
}
