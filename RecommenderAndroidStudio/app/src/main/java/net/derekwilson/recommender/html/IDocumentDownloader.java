package net.derekwilson.recommender.html;

import net.derekwilson.recommender.tasks.ITaskResultHandler;

import org.jsoup.nodes.Document;

import java.net.URL;

import rx.Subscription;

public interface IDocumentDownloader {
	Subscription getDocument(URL url, ITaskResultHandler<Document> handler);
}
