package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.presenter.listrecommendations.IListRecommendationsPresenter;
import net.derekwilson.recommender.presenter.listrecommendations.ListRecommendationsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListRecommendationsModule {

	@Provides
	@ActivityScope
	IListRecommendationsPresenter providePresenter(ListRecommendationsPresenter presenter) {
		return presenter;
	}
}
