package net.derekwilson.recommender.ioc;

import android.app.Activity;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.fragment.preferences.MainPreferencesFragment;

import dagger.Component;

@ActivityScope @Component(dependencies = IApplicationComponent.class, modules = BaseActivityModule.class)
public interface IBaseActivityComponent {
	// things that need an activiry scope component but dont have their own presenter
	void inject(MainPreferencesFragment fragment);

	// we only need to put things in here that are exposed to subgraphs
	// Expose the activity to sub-graphs.
	Activity getActivityContext();
	// repositories should not be shared between different activities - so we do not put it in the application component
	IRecommendationRepository getRecommendationRepository();
}
