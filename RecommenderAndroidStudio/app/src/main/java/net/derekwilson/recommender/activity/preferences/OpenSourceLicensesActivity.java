package net.derekwilson.recommender.activity.preferences;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.BaseActivity;
import net.derekwilson.recommender.ioc.IApplicationComponent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class OpenSourceLicensesActivity extends BaseActivity {

	protected TextView jsouplicense;
	protected TextView retrofitlicense;
	protected TextView rxandroidlicense;
	protected TextView sqlbritelicense;
	protected TextView daggerlicense;
	protected TextView jacksonlicense;
	protected TextView v7license;
	protected TextView slf4jlicense;
	protected TextView analyticslicense;

	@Override
	protected void bindControls() {
		super.bindControls();
		jsouplicense = findViewById(R.id.txtjsoup);
		retrofitlicense = findViewById(R.id.txtretrofit);
		rxandroidlicense = findViewById(R.id.txtrxandroid);
		sqlbritelicense = findViewById(R.id.txtSqlbriteLicense);
		daggerlicense = findViewById(R.id.txtDaggerLicense);
		jacksonlicense = findViewById(R.id.txtjackson);
		v7license = findViewById(R.id.txtAndroiSupportLibraryLicense);
		slf4jlicense = findViewById(R.id.txtSlf4jLicense);
		analyticslicense = findViewById(R.id.txtAnalyticsLicense);
	}

	@Override
	protected void injectDependencies(IApplicationComponent applicationComponent) {
		applicationComponent.inject(this);      // will inject into the base as well as this class
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_open_source_licenses;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		logger.getCurrentApplicationLogger().debug("OSLicensesActivity.onCreate()");

		getTextFromFile(this.getApplicationContext(), "license/sqlbrite.txt", sqlbritelicense);
		getTextFromFile(this.getApplicationContext(), "license/retrofit.txt", retrofitlicense);
		getTextFromFile(this.getApplicationContext(), "license/jsoup.txt", jsouplicense);
		getTextFromFile(this.getApplicationContext(), "license/rxandroid.txt", rxandroidlicense);
		getTextFromFile(this.getApplicationContext(), "license/dagger2.txt", daggerlicense);
		getTextFromFile(this.getApplicationContext(), "license/androidsupportlibraryv7.txt", v7license);
		getTextFromFile(this.getApplicationContext(), "license/jackson.txt", jacksonlicense);
		getTextFromFile(this.getApplicationContext(), "license/slf4j.txt", slf4jlicense);
		getTextFromFile(this.getApplicationContext(), "license/mixpanel.txt", analyticslicense);
	}

	private void getTextFromFile(Context context, String filename, TextView view) {
		InputStream inputStream = null;
		BufferedReader reader = null;
		StringBuffer buf = new StringBuffer();
		try {
			inputStream = context.getAssets().open(filename);
			logger.getCurrentApplicationLogger().debug("Opened {} Available {}", filename, inputStream.available());
			reader = new BufferedReader(new InputStreamReader(inputStream));

			String str;
			if (inputStream != null) {
				while ((str = reader.readLine()) != null) {
					buf.append(str + "\n");
				}
			}
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error reading {}", filename, e);
			crashReporter.logNonFatalException(e);
		}
		finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				logger.getCurrentApplicationLogger().error("Error closing {}", filename, e);
				crashReporter.logNonFatalException(e);
			}
		}

		view.setText(buf.toString());
	}
}
