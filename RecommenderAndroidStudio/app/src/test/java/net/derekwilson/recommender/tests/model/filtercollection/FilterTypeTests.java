package net.derekwilson.recommender.tests.model.filtercollection;

import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.FilterSelectionItem;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class FilterTypeTests extends TestSetup {

	@Test
	public void empty_collection_is_none() {
		// arrange

		// act
		FilterCollection.FilterType type = collection.getFilterType();

		// assert
		assertThat("incorrect type set", type, is(FilterCollection.FilterType.None));
	}

	@Test
	public void populated_collection_is_none() {
		// arrange
		collection.setFilters(getFilters(true,true));

		// act
		FilterCollection.FilterType type = collection.getFilterType();

		// assert
		assertThat("incorrect type set", type, is(FilterCollection.FilterType.None));
	}

	@Test
	public void populated_collection_with_category_selected() {
		// arrange
		collection.setFilters(getFilters(true,true));
		FilterSelectionItem item = collection.getFilterByName("FILTER 1");
		item.setSelected(true);

		// act
		FilterCollection.FilterType type = collection.getFilterType();

		// assert
		assertThat("incorrect type set", type, is(FilterCollection.FilterType.Category));
	}

	@Test
	public void populated_collection_with_category_selected_and_search() {
		// arrange
		collection.setFilters(getFilters(true,true));
		collection.setSearchFilter("SEARCH FILTER");
		FilterSelectionItem item = collection.getFilterByName("FILTER 1");
		item.setSelected(true);

		// act
		FilterCollection.FilterType type = collection.getFilterType();

		// assert
		assertThat("incorrect type set", type, is(FilterCollection.FilterType.CategoryAndSearch));
	}


	@Test
	public void populated_collection_with_search() {
		// arrange
		collection.setFilters(getFilters(true,true));
		collection.setSearchFilter("SEARCH FILTER");

		// act
		FilterCollection.FilterType type = collection.getFilterType();

		// assert
		assertThat("incorrect type set", type, is(FilterCollection.FilterType.Search));
	}


}
