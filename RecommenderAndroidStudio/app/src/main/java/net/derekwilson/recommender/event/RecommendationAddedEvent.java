package net.derekwilson.recommender.event;

import net.derekwilson.recommender.model.RecommendationType;

public class RecommendationAddedEvent {
	private RecommendationType destination;

	public RecommendationAddedEvent(RecommendationType destination) {
		this.destination = destination;
	}

	public RecommendationType getDestination() {
		return destination;
	}
}
