package net.derekwilson.recommender.sharing;

import android.content.Context;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.model.Recommendation;

import java.util.List;

public class BaseSharer implements ISharer {

	protected String getNewline() {
		return "\n";
	}

	@Override
	public String getShareText(Context context, List<Recommendation> recommendations) {
		StringBuilder text = new StringBuilder();
		text.append("I Recommend").append(getNewline());
		for (Recommendation recommendation : recommendations) {
			text.append(getNewline())
			.append(recommendation.getTitle()).append(getNewline())
			.append(recommendation.getUri()).append(getNewline())
			.append(recommendation.getNotes()).append(getNewline())
			.append("------").append(getNewline())
			;
		}
		text.append(getNewline())
				.append(getViralityText(context))
				.append(getNewline());
		return text.toString();
	}

	private String getViralityText(Context context) {
		StringBuilder text = new StringBuilder();
		text.append(context.getString(R.string.virality_line_1)).append(getNewline())
				.append(context.getString(R.string.virality_google_title)).append(getNewline())
				.append(context.getString(R.string.virality_google_url)).append(getNewline())
				.append(context.getString(R.string.virality_amazon_title)).append(getNewline())
				.append(context.getString(R.string.virality_amazon_url)).append(getNewline());
		return text.toString();
	}
}

