package net.derekwilson.recommender.presenter.sharereceiverecommendationactivity;

import android.content.Intent;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.BasePresenter;
import net.derekwilson.recommender.receiving.IIntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromText;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromUrl;
import net.derekwilson.recommender.tasks.ITaskErrorResult;
import net.derekwilson.recommender.tasks.ITaskResultHandler;
import net.derekwilson.recommender.wrapper.ICalendarUtils;

import javax.inject.Inject;

import rx.Subscription;

public class ShareReceiveRecommendationActivityPresenter extends BasePresenter<IShareReceiveRecommendationActivityView> implements IShareReceiveRecommendationActivityPresenter {
	private ILoggerFactory logger;
	private IIntentContentTypeFinder intentTypeFinder;
	private IIntentUnpackerFromUrl unpackerFromUrl;
	private IIntentUnpackerFromText unpackerFromText;
	private ICalendarUtils calendarUtils;
	private Subscription subscription;

	@Inject
	public ShareReceiveRecommendationActivityPresenter(ILoggerFactory logger, IIntentContentTypeFinder intentTypeFinder, IIntentUnpackerFromUrl unpackerFromUrl, IIntentUnpackerFromText unpackerFromText, ICalendarUtils calendarUtils) {
		this.logger = logger;
		this.intentTypeFinder = intentTypeFinder;
		this.unpackerFromUrl = unpackerFromUrl;
		this.unpackerFromText = unpackerFromText;
		this.calendarUtils = calendarUtils;
	}

	@Override
	public void onDestroy() {
		if (subscription != null) {
			subscription.unsubscribe();
			subscription = null;
		}
	}

	private void unpackFromUrlAsync(Intent intent) {
		view.showProgress(R.string.progress_unpacking_intent);
		subscription = unpackerFromUrl.getRecommendationFromUrl(intent, new ITaskResultHandler<Recommendation>() {
			@Override
			public void onSuccess(Recommendation result) {
				view.hideProgress();
				result.setUpdated(calendarUtils.getNow());
				view.showRecommendation(result);
			}

			@Override
			public void onError(ITaskErrorResult error) {
				view.hideProgress();
				view.showErrorMessage(error);
				view.end();
			}
		});
	}

	@Override
	public void unpackIntent(Intent intent) {
		Recommendation recommendation = null;
		switch (intentTypeFinder.getTypeOfContent(intent)) {
			case Url:
				unpackFromUrlAsync(intent);
				return;
			case Text:
				recommendation = unpackerFromText.getRecommendationFromText(intent);
				if (recommendation != null) {
					// set the updated stamp
					recommendation.setUpdated(calendarUtils.getNow());
					view.showRecommendation(recommendation);
				}
				else {
					view.showMessage(R.string.error_bad_share_receive);
					view.end();
				}
				return;
		}
		view.showMessage(R.string.unknown_intent_type);
		view.end();
	}
}
