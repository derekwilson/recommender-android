package net.derekwilson.recommender.tests.model.filtercollection;

import net.derekwilson.recommender.model.FilterSelectionItem;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class GetFilterByNameTests extends TestSetup {

	@Test
	public void empty_collection_does_not_find_name() {
		// arrange

		// act
		FilterSelectionItem item = collection.getFilterByName("FILTER 1");

		// assert
		assertNull(item);
	}

	@Test
	public void populated_collection_can_find_name() {
		// arrange
		collection.setFilters(getFilters(true,true));

		// act
		FilterSelectionItem item = collection.getFilterByName("FILTER 1");

		// assert
		assertThat("should be able to find the filter", item.getText(), is("FILTER 1"));
	}

	@Test
	public void populated_collection_does_not_find_unknown_name() {
		// arrange
		collection.setFilters(getFilters(true,true));

		// act
		FilterSelectionItem item = collection.getFilterByName("UNKNOWN");

		// assert
		assertNull(item);
	}

}
