package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.activity.shareReceiveRecommendation.ShareReceiveRecommendationActivity;
import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.presenter.sharereceiverecommendationactivity.IShareReceiveRecommendationActivityPresenter;

import dagger.Component;

@ActivityScope
@Component(dependencies = IApplicationComponent.class, modules = {BaseActivityModule.class, ShareReceiveRecommendationModule.class})
public interface IShareReceiveRecommendationComponent {
	// list any activities or fragments that use this component
	void inject(ShareReceiveRecommendationActivity activity);

	// used by the tests
	IShareReceiveRecommendationActivityPresenter getActivityPresenter();
}
