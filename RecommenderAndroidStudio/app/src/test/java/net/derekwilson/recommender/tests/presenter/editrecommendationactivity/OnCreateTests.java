package net.derekwilson.recommender.tests.presenter.editrecommendationactivity;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.model.RecommendationType;

import org.junit.Test;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OnCreateTests extends PresenterTestSetup {
	@Test
	public void sets_the_title_for_recommendation() {
		// arrange
		setupPresenter(true);
		when(mockView.getRecommendationType()).thenReturn(RecommendationType.RECOMMENDATION);

		// act
		presenter.onCreate();

		// assert
		verify(mockView, times(1)).setMainTitleTextId(R.string.list_title_recommendation);
	}

	@Test
	public void sets_the_title_for_trythis() {
		// arrange
		setupPresenter(true);
		when(mockView.getRecommendationType()).thenReturn(RecommendationType.TRY_THIS);

		// act
		presenter.onCreate();

		// assert
		verify(mockView, times(1)).setMainTitleTextId(R.string.list_title_trythis);
	}
}
