package net.derekwilson.recommender.tests.html.documentconverer;

import net.derekwilson.recommender.model.Recommendation;

import org.junit.Test;

import java.io.IOException;
import java.net.URL;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class Amazon1ConverterTests extends BaseHtmlConverterTests {
	private static String AMAZON_URL = "http://www.amazon.co.uk/Sweet-Bells-Kate-Rusby/dp/B002S53LZG/ref=sr_1_4?ie=UTF8&qid=1442388218&sr=8-4&keywords=kate+rusby";
	private static String AMAZON_1_FILENAME = "test_amazon_1.html";

	private URL testUrl;

	@Test
	public void gets_amazon_name() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getName(), is("Sweet Bells"));
	}

	@Test
	public void gets_amazon_by() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getBy(), is("Kate Rusby"));
	}

	@Test
	public void gets_amazon_category() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void gets_amazon_notes() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getNotes(), is("Kate Rusby - Sweet Bells - Amazon.com Music. This is not your regular, run of the mill Christmas album - not even by folk music standards. THE VOCALS : KR's vocals are delightful (they always are), and her Yorkshire accent is a real treat to listen to (and this is coming from someone with a Liverpudlian accent!). She doesn't have a powerful voice, but it's a pure voice with a dainty girlish timbre. Whilst I was disappointed that there were no harmony vocals on this album, it may have been a wise choice not to have them - I suspect that they might have clashed with instrumental accompaniment. THE PLAYING : This is first rate stuff - acoustic guitars, cittern, stand-up bass, fiddle on one track, and diatonic accordion (melodeon) on all tracks with some notable short solos. Also, on five tracks, a variety of brass band instruments played by members of the Grimethorpe Colliery Band and the band of the Coldstream Guards. The sound produced by the musicians has a very rich texture. There is no electric guitar or percussion on this album. THE SONGS : All of the songs are traditional/festive English songs/carols arranged or adapted by KR - including some arrangements with a regional influence. The songs feature a good mix of tempos. Some brief comments about a selection : HERE WE COME A-WASSAILING - An up-tempo song with a strong lilt to it, good lyrics, fine vocals from KR, and some outstanding playing from Andy Cutting (accordion) and Anna Massie (cittern). Great music - traditional English folk music par excellence. POOR OLD HORSE - I don't think this is a great song, but it touched a nerve with me (I have a 'soft spot' for horses). It's a sad song about a horse who, now that he has grown old, has become increasingly rejected and neglected by his master. Read more �"));
	}

	@Test
	public void gets_amazon_url() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getUri(), is(AMAZON_URL));
	}
}
