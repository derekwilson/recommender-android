package net.derekwilson.recommender.receiving;

import android.content.Intent;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.tasks.ITaskResultHandler;
import net.derekwilson.recommender.tasks.TaskErrorResult;

import java.net.MalformedURLException;
import java.net.URL;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Subscription;

public class IntentUnpackerFromUrl implements IIntentUnpackerFromUrl {

	private ILoggerFactory logger;
	private IAsyncConvertUrlToRecommendation wikipediaAsyncConverter;
	private IAsyncConvertUrlToRecommendation htmlAsyncConverter;
	private ICrashReporter crashReporter;

	@Inject
	public IntentUnpackerFromUrl(
			ILoggerFactory logger,
			@Named("wikipedia") IAsyncConvertUrlToRecommendation wikipediaAsyncConverter,
			@Named("html") IAsyncConvertUrlToRecommendation htmlAsyncConverter, ICrashReporter crashReporter)
	{
		this.logger = logger;
		this.wikipediaAsyncConverter = wikipediaAsyncConverter;
		this.htmlAsyncConverter = htmlAsyncConverter;
		this.crashReporter = crashReporter;
	}

	/**
	 * get a single recommendation from a url shared via an intent
	 * @param intent the intent containing the url
	 * @param handler a callback that will provide the recommendation (or signal an error) when the downloading and processing is complete
	 * @return a subscription as this is an astnc method
	 */
	@Override
	public Subscription getRecommendationFromUrl(Intent intent, ITaskResultHandler<Recommendation> handler) {
		String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
		if (sharedText != null) {
			logger.getCurrentApplicationLogger().debug("IntentUnpacker string {} {}", intent.getType(), sharedText);
			try {
				URL url = new URL(sharedText);
				logger.getCurrentApplicationLogger().debug("ShareReceiveRecommendationActivity uri {}", url.toString());

				if (url.getHost().endsWith("wikipedia.org")) {
					return wikipediaAsyncConverter.getFromAsyncService(url, handler);
				}
				return htmlAsyncConverter.getFromAsyncService(url,handler);
			}
			catch (MalformedURLException e) {
				// we cannot understand it assume it is text
				logger.getCurrentApplicationLogger().warn("cannot read from url", e);
				crashReporter.logNonFatalException(e);
				handler.onError(new TaskErrorResult(TaskErrorResult.INTENT_UNPACK_BAD_URL));
			}
		}
		return null;
	}
}
