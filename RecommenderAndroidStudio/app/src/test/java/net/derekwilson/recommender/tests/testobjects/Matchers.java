package net.derekwilson.recommender.tests.testobjects;

import static org.mockito.AdditionalMatchers.or;
import static org.mockito.ArgumentMatchers.isNull;

public class Matchers {

	public static <T> T orNull(T first) {
		// mockito v2 does not match null to any() eg. null does not match anyString()
		// sometimes we want to be able to say orNull(anyString())
		return (T) or(first, isNull());
	}

}
