package net.derekwilson.recommender.fragment.recommendation;

import android.app.Dialog;
import android.graphics.Point;
import android.view.Display;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.wrapper.ITextUtils;

import javax.inject.Inject;

public abstract class BaseBottomSheetFragment extends BottomSheetDialogFragment {

	@Inject
	protected ILoggerFactory logger;

	@Inject
	protected ITextUtils textUtils;

	private BottomSheetBehavior bottomSheetBehavior;

	private final BottomSheetBehavior.BottomSheetCallback bottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
		@Override
		public void onStateChanged(@NonNull View bottomSheet, int newState) {
			if (newState == BottomSheetBehavior.STATE_HIDDEN) {
				dismiss();
			}
		}

		@Override
		public void onSlide(@NonNull View bottomSheet, float slideOffset) {
		}
	};

	protected abstract int getLayout();

	private Point getScreenSize() {
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		return size;
	}

	@Override
	public void setupDialog(@NonNull Dialog dialog, int style) {
		super.setupDialog(dialog, style);
		View contentView = View.inflate(getContext(), getLayout(), null);
		dialog.setContentView(contentView);

		// lets set the size of the dialog to be half the height of the screen
		Point size = getScreenSize();
		contentView.getLayoutParams().height = size.y / 2;

		CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
		CoordinatorLayout.Behavior behavior = params.getBehavior();

		if( behavior != null && behavior instanceof BottomSheetBehavior ) {
			bottomSheetBehavior = (BottomSheetBehavior) behavior;
			bottomSheetBehavior.setBottomSheetCallback(bottomSheetBehaviorCallback);
		} else {
			throw new UnsupportedOperationException("CategoryBottomSheetFragment: cannot find bottomSheetBehaviour:");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
	}

	public void hideBottomSheet() {
		bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
	}
}
