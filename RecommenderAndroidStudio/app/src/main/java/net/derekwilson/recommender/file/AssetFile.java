package net.derekwilson.recommender.file;

import android.content.Context;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.logging.ILoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.inject.Inject;

public class AssetFile implements IAssetFile {

	private ILoggerFactory logger;
	private ICrashReporter crashReporter;

	@Inject
	public AssetFile(ILoggerFactory logger, ICrashReporter crashReporter) {
		this.logger = logger;
		this.crashReporter = crashReporter;
	}

	@Override
	public String getFileContents(Context context, String filename) {

		InputStream inputStream = null;
		BufferedReader reader = null;
		StringBuffer buf = new StringBuffer();
		try {
			inputStream = context.getAssets().open(filename);
			logger.getCurrentApplicationLogger().debug("Opened {} Available {}", filename, inputStream.available());
			reader = new BufferedReader(new InputStreamReader(inputStream));

			String str;
			if (inputStream != null) {
				while ((str = reader.readLine()) != null) {
					buf.append(str);
				}
			}
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error reading {}", filename, e);
			crashReporter.logNonFatalException(e);
		}
		finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				logger.getCurrentApplicationLogger().error("Error closing {}", filename, e);
				crashReporter.logNonFatalException(e);
			}
		}

		return buf.toString();
	}
}
