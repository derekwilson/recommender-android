package net.derekwilson.recommender.presenter.editrecommendationactivity;

import android.content.Intent;
import android.nfc.NdefMessage;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.IView;

public interface IEditRecommendationActivityView extends IView {
	RecommendationType getRecommendationType();

	void setMainTitleTextId(int id);

	Recommendation getRecommendationFromPassedIntent();

	String getNicknameKey();

	void sendNfcMessage(NdefMessage message);

	void startIntent(Intent sendIntent, int email_share_text);
}
