package net.derekwilson.recommender.tests.presenter.importrecommendationactivity;

import android.app.Activity;

import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.ioc.scopes.IImportRecommendationComponent;
import net.derekwilson.recommender.presenter.importrecommendationactivity.IImportRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.importrecommendationactivity.IImportRecommendationActivityView;
import net.derekwilson.recommender.presenter.importrecommendationactivity.ImportRecommendationActivityPresenter;
import net.derekwilson.recommender.receiving.IInserter;
import net.derekwilson.recommender.receiving.IIntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromContentResolver;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromNfcBeam;
import net.derekwilson.recommender.tests.BaseJunitTests;

import static org.mockito.Mockito.mock;

public class PresenterTestSetup extends BaseJunitTests {
	// mocks
	protected Activity mockActivity = mock(Activity.class);
	protected IImportRecommendationActivityView mockView = mock(IImportRecommendationActivityView.class);
	protected IRecommendationRepository mockRepository = mock(IRecommendationRepository.class);
	protected IIntentContentTypeFinder mockFinder = mock(IIntentContentTypeFinder.class);
	protected IIntentUnpackerFromContentResolver mockUnpackerFromContent = mock(IIntentUnpackerFromContentResolver.class);
	protected IIntentUnpackerFromNfcBeam mockUnpackerFromBeam = mock(IIntentUnpackerFromNfcBeam.class);
	protected IInserter mockInserter = mock(IInserter.class);
	protected IEventLogger mockEventLogger = mock(IEventLogger.class);

	// object under test
	protected IImportRecommendationActivityPresenter presenter;

	protected IImportRecommendationComponent component;

	protected void setupPresenter(boolean bind) {
		presenter = new ImportRecommendationActivityPresenter(
				mockLoggerFactory,
				mockFinder,
				mockUnpackerFromContent,
				mockUnpackerFromBeam,
				mockInserter,
				mockEventBus,
				mockEventLogger);
		if (bind) {
			presenter.bindView(mockView);
		}
	}
}
