package net.derekwilson.recommender.activity.shareReceiveRecommendation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.BaseActivity;
import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.fragment.recommendation.CategoryBottomSheetFragment;
import net.derekwilson.recommender.fragment.recommendation.RecommendationEditFragment;
import net.derekwilson.recommender.fragment.recommendation.SourceBottomSheetFragment;
import net.derekwilson.recommender.fragment.utility.OkCancelDialogFragment;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.scopes.DaggerIShareReceiveRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.IShareReceiveRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.ShareReceiveRecommendationModule;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.IBottomSheetView;
import net.derekwilson.recommender.presenter.sharereceiverecommendationactivity.IShareReceiveRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.sharereceiverecommendationactivity.IShareReceiveRecommendationActivityView;
import net.derekwilson.recommender.tasks.ITaskErrorResult;
import net.derekwilson.recommender.view.ProgressDialogHelper;

import javax.inject.Inject;

public class ShareReceiveRecommendationActivity extends BaseActivity
		implements
			IShareReceiveRecommendationActivityView,
			IBottomSheetView,
			OkCancelDialogFragment.Listener,
			View.OnClickListener
{
	private static final String KEY_EDIT_FRAGMENT_TAG = "edit_fragment_key";
	private static final String TAG_SAVE_DISCARD = "save_discard_tag";
	private static final String TAG_EDIT_FRAGMENT = "edit_fragment_tag";

	private IShareReceiveRecommendationComponent component;

	@Inject
	protected IShareReceiveRecommendationActivityPresenter presenter;

	@Inject
	protected IEventLogger eventLogger;

	private ProgressDialogHelper progressHelper;

	private RecommendationEditFragment editFragment;
	private CategoryBottomSheetFragment categoryFragment;
	private SourceBottomSheetFragment sourceFragment;

	protected FloatingActionButton fab;
	@Override
	protected void bindControls() {
		super.bindControls();
		fab = findViewById(R.id.fab);
	}

	protected IShareReceiveRecommendationComponent getActivityComponent(Activity activity) {
		if (component == null) {
			component = DaggerIShareReceiveRecommendationComponent.builder()
					.iApplicationComponent(getApplicationComponent())
					.baseActivityModule(new BaseActivityModule(activity))
					.shareReceiveRecommendationModule(new ShareReceiveRecommendationModule())
					.build();
		}
		return component;
	}

	@Override
	protected void injectDependencies(IApplicationComponent applicationComponent) {
		getActivityComponent(this).inject(this);    // will inject into the base as well as this class
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_share_receiver;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		if (editFragment != null) {
			String fragmentTag = editFragment.getTag();
			if (fragmentTag != null) {
				outState.putString(KEY_EDIT_FRAGMENT_TAG, fragmentTag);
			}
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		toolbarBackButtonNeeded = true;
		super.onCreate(savedInstanceState);
		logger.getCurrentApplicationLogger().debug("ShareReceiveRecommendationActivity started");
		presenter.bindView(this);
		progressHelper = new ProgressDialogHelper(this);
		if (savedInstanceState == null) {
			// only unpack if we are being started rather than restored
			presenter.unpackIntent(getIntent());
		} else {
			// we are being restored - go find the fragment
			String fragmentTag = savedInstanceState.getString(KEY_EDIT_FRAGMENT_TAG);
			if (fragmentTag != null) {
				editFragment = (RecommendationEditFragment)getSupportFragmentManager().findFragmentByTag(fragmentTag);
			}
		}
		categoryFragment = new CategoryBottomSheetFragment();
		sourceFragment = new SourceBottomSheetFragment();
		if (fab != null) {
			fab.setOnClickListener(this);
		}
		presenter.onCreate();
	}

	@Override
	public void showRecommendation(Recommendation recommendation) {
		editFragment = new RecommendationEditFragment();
		// we have shared from another app so at the moment assume its going on the I Recommend list
		editFragment.setArguments(RecommendationType.RECOMMENDATION, recommendation, true);
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, editFragment, TAG_EDIT_FRAGMENT)
				.commit();
	}

	@Override
	public void showErrorMessage(ITaskErrorResult error) {
		displayMessage(error.getMessage(this));
	}

	@Override
	public void onDestroy() {
		logger.getCurrentApplicationLogger().debug("ShareReceiveRecommendationActivity.onDestroy()");
		presenter.onDestroy();
		presenter.unbindView();
		super.onDestroy();
	}

	private RecommendationEditFragment getEditFragment() {
		return (RecommendationEditFragment) getSupportFragmentManager().findFragmentByTag(TAG_EDIT_FRAGMENT);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.fab:
				saveRecommendation();
				finish();
				gotoMain();
				break;
		}
	}

	@Override
	public void onBackPressed() {
		showSaveDiscardDialog();
		return;
	}

	protected void showSaveDiscardDialog() {
		DialogFragment newFragment = OkCancelDialogFragment.newInstance(
				getString(R.string.save_discard_add_message),
				getString(R.string.ok_cancel_save),
				getString(R.string.ok_cancel_discard)
		);
		newFragment.show(getSupportFragmentManager(), TAG_SAVE_DISCARD);
	}

	@Override
	public void ok(String tag) {
		saveRecommendation();
		finish();
		gotoMain();
	}

	@Override
	public void cancel(String tag) {
		finish();
		gotoMain();
	}

	private void saveRecommendation() {
		final RecommendationEditFragment fragment = getEditFragment();
		if (fragment != null) {
			eventLogger.logReceivedRecommendation(fragment.getRecommendation(), "share");
			fragment.saveRecommendation();
		}
	}

	@Override
	public void end() {
		finish();
	}

	@Override
	public void showError(Throwable throwable) {
		displayMessage(throwable.getMessage());
	}

	@Override
	public void showMessage(int message) {
		displayMessage(getString(message));
	}

	@Override
	public void showProgress(int progressMessage) {
		progressHelper.showProgress(getString(progressMessage));
	}

	@Override
	public void hideProgress() {
		progressHelper.hideProgress();
	}

	@Override
	public void showCategoryBottomSheet() {
		categoryFragment.showBottomSheet(getSupportFragmentManager(), editFragment);
	}

	@Override
	public void showSourceBottomSheet() {
		sourceFragment.showBottomSheet(getSupportFragmentManager(), editFragment);
	}
}
