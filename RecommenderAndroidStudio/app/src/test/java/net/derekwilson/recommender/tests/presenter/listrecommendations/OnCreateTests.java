package net.derekwilson.recommender.tests.presenter.listrecommendations;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import rx.functions.Action1;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class OnCreateTests extends PresenterTestSetup {

	@Before
	public void setUp() {
		super.setUp();
		setupPresenter(true, false);
	}

	@Test
	public void there_is_no_filter() {
		// arrange

		// act
		presenter.onCreate();

		// assert
		// the empty text is set correctly
		verify(mockView, times(1)).setNonFilteredEmptyText();
		verify(mockView, never()).setFilteredEmptyText();
		// we call the repository correctly
		verify(mockRepository, times(1)).getByType((RecommendationType) any(), any(), (Action1<List<Recommendation>>) any());
	}

}
