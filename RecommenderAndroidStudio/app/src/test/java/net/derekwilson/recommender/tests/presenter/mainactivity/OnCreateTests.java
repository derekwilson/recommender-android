package net.derekwilson.recommender.tests.presenter.mainactivity;

import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.IFilterCollection;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class OnCreateTests extends PresenterTestsSetup {

	@Before
	public void setUp() {
		setupPresenter(true, true);
	}

	private void verifyEmptyFilterCollection(IFilterCollection collection) {
		// there is only one item in the collection and its the no filter object
		assertThat(collection.getFilters().size(), is(1));
		assertThat(collection.getFilters().get(0).isNofilter(), is(true));
	}

	@Test
	public void there_are_null_categories() {
		// arrange
		presenter.onCreate();

		// act
		realRepository.callStringResponseHandler(null);

		// assert
		ArgumentCaptor<FilterCollection> argument1 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(1)).showCategoryFilter(argument1.capture());
		verifyEmptyFilterCollection(argument1.getValue());

		ArgumentCaptor<FilterCollection> argument2 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(1)).applyFilterToAllTabs(argument2.capture());
		verifyEmptyFilterCollection(argument2.getValue());
	}

	@Test
	public void there_are_no_categories() {
		// arrange
		presenter.onCreate();
		ArrayList<String> categories = new ArrayList<>(5);

		// act
		realRepository.callStringResponseHandler(categories);

		// assert
		ArgumentCaptor<FilterCollection> argument1 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(1)).showCategoryFilter(argument1.capture());
		verifyEmptyFilterCollection(argument1.getValue());

		ArgumentCaptor<FilterCollection> argument2 = ArgumentCaptor.forClass(FilterCollection.class);
		verify(mockView, times(1)).applyFilterToAllTabs(argument2.capture());
		verifyEmptyFilterCollection(argument2.getValue());
	}

	private void verifyPopulatedFilterCollection(IFilterCollection collection) {
		assertThat(collection.getFilters().size(), is(6));
		assertThat(collection.getFilters().get(0).isNofilter(), is(true));
		assertThat(collection.getFilters().get(1).isNofilter(), is(false));
		assertThat(collection.getFilters().get(1).getText(), is("1"));
		assertThat(collection.getFilters().get(2).isNofilter(), is(false));
		assertThat(collection.getFilters().get(2).getText(), is("2"));
		assertThat(collection.getFilters().get(3).isNofilter(), is(false));
		assertThat(collection.getFilters().get(3).getText(), is("3"));
		assertThat(collection.getFilters().get(4).isNofilter(), is(false));
		assertThat(collection.getFilters().get(4).getText(), is("4"));
		assertThat(collection.getFilters().get(5).isNofilter(), is(false));
		assertThat(collection.getFilters().get(5).getText(), is("5"));
	}

	@Test
	public void there_are_categories() {
		// arrange
		presenter.onCreate();
		List<String> categories = getTestCategories();

		// act
		realRepository.callStringResponseHandler(categories);

		// assert
		ArgumentCaptor<IFilterCollection> argument1 = ArgumentCaptor.forClass(IFilterCollection.class);
		verify(mockView, times(1)).showCategoryFilter(argument1.capture());
		verifyPopulatedFilterCollection(argument1.getValue());

		ArgumentCaptor<IFilterCollection> argument2 = ArgumentCaptor.forClass(IFilterCollection.class);
		verify(mockView, times(1)).applyFilterToAllTabs(argument2.capture());
		verifyPopulatedFilterCollection(argument2.getValue());
	}
}
