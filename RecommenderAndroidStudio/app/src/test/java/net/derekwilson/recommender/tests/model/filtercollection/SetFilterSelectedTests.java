package net.derekwilson.recommender.tests.model.filtercollection;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class SetFilterSelectedTests extends TestSetup {

	private void verifySelectedState(boolean isSelected) {
		assertThat(collection.isFilterSelected("NOFILTER"), is(false));
		assertThat(collection.isFilterSelected(0), is(false));

		assertThat(collection.isFilterSelected("FILTER 1"), is(isSelected));
		assertThat(collection.isFilterSelected(1), is(isSelected));

		assertThat(collection.isFilterSelected("FILTER 2"), is(false));
		assertThat(collection.isFilterSelected(2), is(false));

		assertThat(collection.isFilterSelected("FILTER 3"), is(false));
		assertThat(collection.isFilterSelected(3), is(false));

		if (isSelected) {
			assertThat(collection.getNumberOfSelectedFilters(), is(1));
		}
		else {
			assertThat(collection.getNumberOfSelectedFilters(), is(0));
		}
	}

	@Test
	public void can_select_unknown_item() {
		// arrange
		collection.setFilters(getFilters(true, true));

		// act
		collection.setFilterSelectedByName("UNKNOWN", true);

		// assert
		verifySelectedState(false);
	}

	@Test
	public void can_select_item() {
		// arrange
		collection.setFilters(getFilters(true, true));

		// act
		collection.setFilterSelectedByName("FILTER 1", true);

		// assert
		verifySelectedState(true);
	}

	@Test
	public void can_toggle_item() {
		// arrange
		collection.setFilters(getFilters(true, true));

		// act
		collection.toggleFilterSelectedByName("FILTER 1");

		// assert
		verifySelectedState(true);
	}

	@Test
	public void can_select_all() {
		// arrange
		collection.setFilters(getFilters(true, true));

		// act
		collection.setAllSelected(true);

		// assert
		// the no filer does not count
		assertThat(collection.getNumberOfSelectedFilters(), is(3));
	}

	@Test
	public void can_unselect_all() {
		// arrange
		collection.setFilters(getFilters(true, true));

		// act
		collection.setAllSelected(false);

		// assert
		assertThat(collection.getNumberOfSelectedFilters(), is(0));
	}
}
