package net.derekwilson.recommender.ioc;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.Html;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.RecommenderBuildConfig;
import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.analytics.MixpanelEventLogger;
import net.derekwilson.recommender.crashes.CrashlyticsCrashReporter;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.RecommenderDatabaseHelper;
import net.derekwilson.recommender.data.utility.DatabaseCopier;
import net.derekwilson.recommender.data.utility.IDatabaseCopier;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.RxEventBus;
import net.derekwilson.recommender.file.AssetFile;
import net.derekwilson.recommender.file.IAssetFile;
import net.derekwilson.recommender.file.ITextFileWriter;
import net.derekwilson.recommender.file.ImageGetter;
import net.derekwilson.recommender.file.TextFileWriter;
import net.derekwilson.recommender.html.DocumentDownloader;
import net.derekwilson.recommender.html.DocumentToRecommendationConverter;
import net.derekwilson.recommender.html.IDocumentDownloader;
import net.derekwilson.recommender.html.IDocumentToRecommendationConverter;
import net.derekwilson.recommender.json.ExportToJson;
import net.derekwilson.recommender.json.IExporter;
import net.derekwilson.recommender.json.IImporter;
import net.derekwilson.recommender.json.ImportFromJson;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.logging.SlfLoggerFactory;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.preferences.PreferencesProvider;
import net.derekwilson.recommender.receiving.AsyncConvertHtmlToRecommendation;
import net.derekwilson.recommender.receiving.AsyncConvertWikipediaToRecommendation;
import net.derekwilson.recommender.receiving.DatabaseInserter;
import net.derekwilson.recommender.receiving.IAsyncConvertUrlToRecommendation;
import net.derekwilson.recommender.receiving.IInserter;
import net.derekwilson.recommender.receiving.IIntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromContentResolver;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromNfcBeam;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromText;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromUrl;
import net.derekwilson.recommender.receiving.IntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IntentUnpackerFromContentResolver;
import net.derekwilson.recommender.receiving.IntentUnpackerFromNfcBeam;
import net.derekwilson.recommender.receiving.IntentUnpackerFromText;
import net.derekwilson.recommender.receiving.IntentUnpackerFromUrl;
import net.derekwilson.recommender.rest.wikipedia.IQueryResultToRecommendationConverter;
import net.derekwilson.recommender.rest.wikipedia.IQueryService;
import net.derekwilson.recommender.rest.wikipedia.QueryResultToRecommendationConverter;
import net.derekwilson.recommender.rest.wikipedia.QueryService;
import net.derekwilson.recommender.sharing.BeamAvailabilityProvider;
import net.derekwilson.recommender.sharing.EmailSharer;
import net.derekwilson.recommender.sharing.IBeamAvailabilityProvider;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.INdefMessageSharer;
import net.derekwilson.recommender.sharing.NdefMessageSharer;
import net.derekwilson.recommender.wrapper.CalendarUtils;
import net.derekwilson.recommender.wrapper.ICalendarUtils;
import net.derekwilson.recommender.wrapper.ITextUtils;
import net.derekwilson.recommender.wrapper.TextUtilsWrapper;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.converter.Converter;
import retrofit.converter.JacksonConverter;
import rx.schedulers.Schedulers;

/**
 * do not forget to add any new object providers into the module for the tests
 */

@Module
public class ApplicationModule {
	private static final String PREF_NAME = "net.derekwilson.recommender";

	private final AndroidApplication application;
	private final ILoggerFactory logger = new SlfLoggerFactory();
	private final IEventLogger eventLogger;
	private final ICrashReporter crashReporter;

	public ApplicationModule(AndroidApplication application) {
		this.application = application;
		this.crashReporter = new CrashlyticsCrashReporter(logger, application);
		this.eventLogger = new MixpanelEventLogger(logger, this.crashReporter, application);
	}

	@Provides
	@Singleton
	Context provideApplicationContext() {
		return this.application;
	}

	@Provides
	@Singleton
	SharedPreferences provideSharedPreferences() {
		return this.application.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
	}

	@Provides
	@Singleton
	ILoggerFactory provideLogger(SlfLoggerFactory loggerFactory) {
		return loggerFactory;
	}

	@Provides
	@Singleton
	SQLiteOpenHelper provideOpenHelper() {
		return new RecommenderDatabaseHelper(provideApplicationContext(), logger, crashReporter);
	}

	@Provides
	@Singleton
	SqlBrite provideSqlBrite() {
		return SqlBrite.create(new SqlBrite.Logger() {
			@Override public void log(String message) {
				if (!RecommenderBuildConfig.PRODUCTION) {
					logger.getCurrentApplicationLogger().debug("Database " + message);
				}
			}
		});
	}

	@Provides
	@Singleton
	BriteDatabase provideDatabase(SqlBrite sqlBrite, SQLiteOpenHelper helper) {
		BriteDatabase db = sqlBrite.wrapDatabaseHelper(helper, Schedulers.io());
		if (!RecommenderBuildConfig.PRODUCTION) {
			db.setLoggingEnabled(true);
		}
		return db;
	}

	@Provides
	@Singleton
	IPreferencesProvider providePreferences(PreferencesProvider prefs) {
		return prefs;
	}

	@Provides
	@Singleton
	ITextUtils provideTextUtils(TextUtilsWrapper textUtilities) {
		return textUtilities;
	}

	@Provides
	@Singleton
	ICalendarUtils provideCalendarUtils(CalendarUtils calendarUtils) {
		return calendarUtils;
	}

	@Provides
	@Singleton
	IDatabaseCopier provideCopier(DatabaseCopier copier) {
		return copier;
	}

	@Provides
	@Singleton
	IEventLogger provideEventLogger() { return eventLogger; }

	@Provides
	@Singleton
	ICrashReporter provideCrashReporter() { return crashReporter; }

	@Provides
	@Singleton
	IBeamAvailabilityProvider provideBeamAvailabilityProvider(BeamAvailabilityProvider provider) { return provider; }

	@Provides
	@Singleton
	IEmailIntentSharer provideEmailSharer(EmailSharer sharer) {
		return sharer;
	}

	@Provides
	@Singleton
	INdefMessageSharer provideNdefMessageSharer(NdefMessageSharer shaerer) { return shaerer; }

	@Provides
	@Singleton
	IExporter provideExporter(ExportToJson exporter) {
		return exporter;
	}

	@Provides
	@Singleton
	IImporter provideImporter(ImportFromJson importer) {
		return importer;
	}

	@Provides
	@Singleton
	IInserter provideInserter(DatabaseInserter inserter) {
		return inserter;
	}

	@Provides
	@Singleton
	IEventBus provideEventBus(RxEventBus eventBus) { return eventBus; }

	@Provides
	@Singleton
	IIntentContentTypeFinder provideIntentContentTypeFinder(IntentContentTypeFinder finder) { return finder; }

	@Provides
	@Singleton
	IIntentUnpackerFromContentResolver provideIntentUnpackerFromContentResolver(IntentUnpackerFromContentResolver unpacker) { return unpacker; }

	@Provides
	@Singleton
	IIntentUnpackerFromNfcBeam provideIntentUnpackerFromNfcBeam(IntentUnpackerFromNfcBeam unpacker) { return unpacker; }

	@Provides
	@Singleton
	IIntentUnpackerFromText provideIntentUnpackerFromText(IntentUnpackerFromText unpacker) { return unpacker; }

	@Provides
	@Singleton
	IIntentUnpackerFromUrl provideIntentUnpackerUrl(IntentUnpackerFromUrl unpacker) { return unpacker; }

	@Provides
	@Singleton
	@Named("wikipedia")
	IAsyncConvertUrlToRecommendation provideWikipediaAsyncConverter(AsyncConvertWikipediaToRecommendation converter) { return converter; }

	@Provides
	@Singleton
	@Named("html")
	IAsyncConvertUrlToRecommendation provideHtmlAsyncConvert(AsyncConvertHtmlToRecommendation converter) { return converter; }

	@Provides
	@Singleton
	ObjectMapper provideObjectMapper() {
		// map java objects to json - more efficient if there is only one per app
		ObjectMapper mapper = new ObjectMapper();
		// we want to ignore any unknown properties
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}

	@Provides
	@Singleton
	Converter provideRetrofitConverter() {
		return new JacksonConverter(provideObjectMapper());
	}

	@Provides
	@Singleton
	IDocumentDownloader provideDocumentDownloader(DocumentDownloader downloader) {
		return downloader;
	}

	@Provides
	@Singleton
	IDocumentToRecommendationConverter provideDocumentConverter(DocumentToRecommendationConverter converter) {
		return converter;
	}

	@Provides
	@Singleton
	IQueryService provideWikipediaService(QueryService service) {
		return service;
	}

	@Provides
	@Singleton
	IQueryResultToRecommendationConverter provideWikipediaConverter(QueryResultToRecommendationConverter converter) {
		return converter;
	}

	@Provides
	@Singleton
	IAssetFile provideAssetFile(AssetFile file) {
		return file;
	}

	@Provides
	@Singleton
	Html.ImageGetter provideGetter(ImageGetter getter) {
		return getter;
	}

	@Provides
	@Singleton
	ITextFileWriter provideTextFileWriter(TextFileWriter writer) { return writer; }

	// do not forget to add any new object providers into the module for the tests
}