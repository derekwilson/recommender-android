package net.derekwilson.recommender.receiving;

import android.content.ContentResolver;
import android.content.Intent;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.tasks.ITaskResultHandler;

import java.util.List;

import rx.Subscription;

public interface IIntentUnpackerFromContentResolver {
	List<Recommendation> getRecommendationsFromContent(Intent intent, ContentResolver contentResolver);
}

