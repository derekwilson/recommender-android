package net.derekwilson.recommender.presenter;

public interface IBottomSheetView {

	void showCategoryBottomSheet();

	void showSourceBottomSheet();

}
