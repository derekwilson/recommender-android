package net.derekwilson.recommender.presenter.sharereceiverecommendationactivity;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.IView;
import net.derekwilson.recommender.tasks.ITaskErrorResult;

public interface IShareReceiveRecommendationActivityView extends IView {

	void showProgress(int progress_unpacking_intent);

	void hideProgress();

	void showRecommendation(Recommendation result);

	void showErrorMessage(ITaskErrorResult error);
}
