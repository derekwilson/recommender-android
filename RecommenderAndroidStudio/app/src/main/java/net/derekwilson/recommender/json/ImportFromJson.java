package net.derekwilson.recommender.json;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

public class ImportFromJson extends BaseImportExport implements IImporter {

	private ICrashReporter crashReporter;

	@Inject
	public ImportFromJson(ObjectMapper mapper, ILoggerFactory logger, ITextUtils textUtils, Context context, ICrashReporter crashReporter) {
		super(mapper, logger, textUtils, context);
		this.crashReporter = crashReporter;
	}

	public String getImportUri() {
		return getExportPath();
	}

	@Override
	public List<Recommendation> importRecommendations() throws ExternalStorageException {
		return importRecommendations("");
	}

	@Override
	public List<Recommendation> importRecommendations(String importFilename) throws ExternalStorageException {
		List<Recommendation> recommendations = null;
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			String filename = (!textUtils.isEmpty(importFilename) ? importFilename : getImportUri());
			logger.getCurrentApplicationLogger().debug("Reading from {}", filename);
			File file = new File(filename);
			if (file.exists()) {
				recommendations = readRecommendations(file);
			}
			else {
				logger.getCurrentApplicationLogger().error("recommendations import file not found {}", filename);
			}
		} else {
			logger.getCurrentApplicationLogger().error("There is no memory card available. This application requires a memory card to store the files on.");
		}
		logRecommendations(recommendations);
		return recommendations;
	}

	@Override
	public List<Recommendation> importRecommendations(InputStream stream) {
		List<Recommendation> recommendations = null;
		try {
			TypeFactory typeFactory = mapper.getTypeFactory();
			recommendations = mapper.readValue(stream,typeFactory.constructCollectionType(List.class, Recommendation.class) );
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error reading JSON", e);
			crashReporter.logNonFatalException(e);
		}
		logRecommendations(recommendations);
		return recommendations;
	}

	@Override
	public List<Recommendation> importRecommendationsFromString(String content) {
		List<Recommendation> recommendations = null;
		try {
			TypeFactory typeFactory = mapper.getTypeFactory();
			recommendations = mapper.readValue(content,typeFactory.constructCollectionType(List.class, Recommendation.class) );
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error reading JSON", e);
			crashReporter.logNonFatalException(e);
		}
		logRecommendations(recommendations);
		return recommendations;
	}

	private List<Recommendation> readRecommendations(File file) throws ExternalStorageException {
		List<Recommendation> recommendations = null;
		try {
			TypeFactory typeFactory = mapper.getTypeFactory();
			recommendations = mapper.readValue(file,typeFactory.constructCollectionType(List.class, Recommendation.class) );
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error reading JSON", e);
			crashReporter.logNonFatalException(e);
			throw new ExternalStorageException(file.getAbsolutePath(), e);
		}
		return recommendations;
	}

	private void logRecommendations(List<Recommendation> recommendations) {
		if (recommendations != null) {
			logger.getCurrentApplicationLogger().debug("{} recommendations imported from JSON", recommendations.size());
		}
		else {
			logger.getCurrentApplicationLogger().debug("No recommendations loaded from JSON");
		}
	}
}
