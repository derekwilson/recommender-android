package net.derekwilson.recommender.tests.html.documentconverer;

		import net.derekwilson.recommender.model.Recommendation;

		import org.junit.Test;

		import java.io.IOException;
		import java.net.URL;

		import static org.hamcrest.MatcherAssert.assertThat;
		import static org.hamcrest.core.Is.is;

public class GoodreadsConverterTests extends BaseHtmlConverterTests {
	private static String URL = "https://www.goodreads.com/book/show/24611819-the-peripheral";
	private static String FILENAME = "test_goodreads.html";

	private URL testUrl;

	@Test
	public void gets_name() throws IOException {
		// arrange
		testUrl = new URL(URL);
		testDocument = loadHtml(FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getName(), is("The Peripheral"));
	}

	@Test
	public void gets_by() throws IOException {
		// arrange
		testUrl = new URL(URL);
		testDocument = loadHtml(FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getBy(), is("William Gibson"));
	}

	@Test
	public void gets_category() throws IOException {
		// arrange
		testUrl = new URL(URL);
		testDocument = loadHtml(FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void gets_notes() throws IOException {
		// arrange
		testUrl = new URL(URL);
		testDocument = loadHtml(FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getNotes(), is(
				"Flynne Fisher lives down a country road, in a rural near-future America where jobs are scarce, unless you count illegal drug manufacture, which she’s trying to avoid. Her brother Burton lives, or tries to, on money from the Veterans Administration, for neurological damage suffered in the Marines’ elite Haptic Recon unit. Flynne earns what she can by assembling product at the local 3D printshop. She made more as a combat scout in an online game, playing for a rich man, but she’s had to let the shooter games go. Wilf Netherton lives in London, seventy-some years later, on the far side of decades of slow-motion apocalypse. Things are pretty good now, for the haves, and there aren’t many have-nots left. Wilf, a high-powered publicist and celebrity-minder, fancies himself a romantic misfit, in a society where reaching into the past is just another hobby.  Burton’s been moonlighting online, secretly working security in some game prototype, a virtual world that looks vaguely like London, but a lot weirder. He’s got Flynne taking over shifts, promised her the game’s not a shooter. Still, the crime she witnesses there is plenty bad. Flynne and Wilf are about to meet one another. Her world will be altered utterly, irrevocably, and Wilf’s, for all its decadence and power, will learn that some of these third-world types from the past can be badass."
				));
	}

	@Test
	public void gets_url() throws IOException {
		// arrange
		testUrl = new URL(URL);
		testDocument = loadHtml(FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getUri(), is(URL));
	}
}



