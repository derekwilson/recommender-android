package net.derekwilson.recommender.logging;

import net.derekwilson.recommender.AndroidApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class SlfLoggerFactory implements ILoggerFactory {

	@Inject
	public SlfLoggerFactory() {}

	@Override
	public Logger getCurrentApplicationLogger() {
		return LoggerFactory.getLogger(AndroidApplication.class);
	}
}
