package net.derekwilson.recommender.sharing;

import net.derekwilson.recommender.model.Recommendation;

import java.util.List;

public interface INdefMessageSharer extends ISharer {
	NdefMessageShareResult getMessage(Recommendation recommendation);
	NdefMessageShareResult getMessage(List<Recommendation> recommendations);
}
