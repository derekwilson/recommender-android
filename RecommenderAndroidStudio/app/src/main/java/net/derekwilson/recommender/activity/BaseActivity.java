package net.derekwilson.recommender.activity;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.sqlbrite.SqlBrite;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.main.MainActivity;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.ioc.DaggerIBaseActivityComponent;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.IBaseActivityComponent;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.sharing.IBeamAvailabilityProvider;
import net.derekwilson.recommender.view.SnackbarHelper;

import javax.inject.Inject;

public abstract class BaseActivity extends AppCompatActivity {

	protected abstract void injectDependencies(IApplicationComponent applicationComponent);
	protected abstract int getLayoutResource();

	protected boolean toolbarBackButtonNeeded = false;
	protected boolean toolbarMenuButtonNeeded = false;
	protected IBaseActivityComponent activityComponent;

	@Inject
	protected ILoggerFactory logger;

	@Inject
	protected ICrashReporter crashReporter;

	@Inject
	protected SqlBrite db;

	@Inject
	protected IBeamAvailabilityProvider beamAvailability;

	protected NfcAdapter nfcAdapter;

	@Nullable
	protected Toolbar toolbar;
	protected void bindControls() {
		toolbar = findViewById(R.id.toolbar);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		injectDependencies(getApplicationComponent());
		super.onCreate(savedInstanceState);
		int resourceId = getLayoutResource();
		if (resourceId != 0) {
			setContentView(getLayoutResource());
		}
		bindControls();

		nfcAdapter = null;
		if (beamAvailability.isBeamAvailable()) {
			nfcAdapter = NfcAdapter.getDefaultAdapter(this);
		}

		if (toolbar != null) {
			setSupportActionBar(toolbar);
			final ActionBar actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(toolbarBackButtonNeeded || toolbarMenuButtonNeeded);
			if (toolbarBackButtonNeeded) {
				toolbar.setNavigationOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						onBackPressed();
					}
				});
			}
			if (toolbarMenuButtonNeeded) {
				actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
			}
		}
	}

	protected IApplicationComponent getApplicationComponent() {
		return ((AndroidApplication) getApplication()).getApplicationComponent();
	}

	protected IBaseActivityComponent getBaseActivityComponent(Activity activity) {
		if (activityComponent == null) {
			activityComponent = DaggerIBaseActivityComponent.builder()
					.iApplicationComponent(getApplicationComponent())
					.baseActivityModule(new BaseActivityModule(activity))
					.build();
		}
		return activityComponent;
	}

	protected void setActionBarIcon(int iconRes) {
		toolbar.setNavigationIcon(iconRes);
	}

	protected void gotoMain() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}

	protected void displayMessage(String message) {
		displayMessage(message, null);
	}

	protected void displayMessage(String message, Snackbar.Callback callback) {
		if (!SnackbarHelper.displayMessageAsSnackbar(this, message, callback)) {
			displayMessageAsToast(message);
		}
	}

	protected void displayMessageAsToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}
}
