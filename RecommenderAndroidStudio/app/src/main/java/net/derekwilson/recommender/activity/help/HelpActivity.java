package net.derekwilson.recommender.activity.help;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.BaseActivity;
import net.derekwilson.recommender.file.IAssetFile;
import net.derekwilson.recommender.ioc.IApplicationComponent;

import javax.inject.Inject;

public class HelpActivity extends BaseActivity {

	@Inject
	IAssetFile assetFile;

	@Inject
	Html.ImageGetter getter;

	protected TextView txtHelp;
	@Override
	protected void bindControls() {
		super.bindControls();
		txtHelp = findViewById(R.id.txtHelp);
	}

	@Override
	protected void injectDependencies(IApplicationComponent applicationComponent) {
		applicationComponent.inject(this);      // will inject into the base as well as this class
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_help;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		toolbarBackButtonNeeded = true;
		super.onCreate(savedInstanceState);
		logger.getCurrentApplicationLogger().debug("HelpActivity started");

		// make links clickable
		txtHelp.setMovementMethod(LinkMovementMethod.getInstance());
		getHtmlFromFile(this.getApplicationContext(), "help/help.html", txtHelp);
	}

	private void getHtmlFromFile(Context context, String filename, TextView view) {
		String html = assetFile.getFileContents(context, filename);
		view.setText(Html.fromHtml(html, getter, null));
	}
}
