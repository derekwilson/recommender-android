package net.derekwilson.recommender.ioc;

import android.app.Activity;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.data.repository.RecommendationRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class BaseActivityModule {
	private final Activity activityContext;

	public BaseActivityModule(Activity activityContext) {
		this.activityContext = activityContext;
	}

	@Provides
	@ActivityScope
	Activity getActivityContext() {
		return this.activityContext;
	}

	@Provides
	@ActivityScope
	IRecommendationRepository provideRecommendationRepository(RecommendationRepository repository) {
		return repository;
	}
}
