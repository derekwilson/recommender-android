package net.derekwilson.recommender.fragment.recommendation;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.FragmentManager;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.R;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.scopes.DaggerIEditRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.EditRecommendationModule;
import net.derekwilson.recommender.ioc.scopes.IEditRecommendationComponent;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.functions.Action1;

public class CategoryBottomSheetFragment extends BaseBottomSheetFragment {

	private IEditRecommendationComponent component;

	@Inject
	protected IRecommendationRepository repository;

	@Inject
	protected ILoggerFactory logger;

	@Inject
	protected ITextUtils textUtils;

	private CategoryAdapter categoryAdapter;

	private Subscription categorySubscription = null;

	private RecommendationEditFragment editFragment;

	private IEditRecommendationComponent getComponent() {
		if (component == null) {
			component = DaggerIEditRecommendationComponent.builder()
					.iApplicationComponent(((AndroidApplication) getActivity().getApplication()).getApplicationComponent())
					.baseActivityModule(new BaseActivityModule(getActivity()))
					.editRecommendationModule(new EditRecommendationModule())
					.build();
		}
		return component;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getComponent().inject(this);
	}

	@Override
	protected int getLayout() {
		return R.layout.fragment_category_bottomsheet;
	}

	protected ListView categoryList;
	@Override
	public void setupDialog(Dialog dialog, int style) {
		super.setupDialog(dialog, style);
		categoryList = dialog.findViewById(R.id.category_list);

		categoryAdapter = new CategoryAdapter(this.getActivity(), textUtils);
		categoryList.setAdapter(categoryAdapter);
		categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				if (editFragment != null) {
					editFragment.setCategory(categoryAdapter.getItem(position));
				} else {
					logger.getCurrentApplicationLogger().warn("CategoryBottomSheetFragment: cannot fire click event as the edit fragment is null");
				}
				dismiss();
			}
		});
	}

	@Override public void onResume() {
		super.onResume();
		categorySubscription = repository.getCategories(new Action1<List<String>>() {
			@Override
			public void call(List<String> categories) {
				logger.getCurrentApplicationLogger().debug("CategoryBottomSheetFragment: bottomsheet getCategories updated - {}", categories.size());
				categoryAdapter.call(categories);
			}
		});
	}

	@Override public void onPause() {
		super.onPause();
		// stop any subscriptions
		if (categorySubscription != null) {
			categorySubscription.unsubscribe();
			categorySubscription = null;
		}
	}

	public void showBottomSheet(FragmentManager supportFragmentManager, RecommendationEditFragment editFragment) {
		this.editFragment =  editFragment;
		show(supportFragmentManager, getTag());
	}
}

