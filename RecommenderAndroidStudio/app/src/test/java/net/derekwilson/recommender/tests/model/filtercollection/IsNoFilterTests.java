package net.derekwilson.recommender.tests.model.filtercollection;

import android.os.Bundle;

import net.derekwilson.recommender.model.Recommendation;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class IsNoFilterTests extends TestSetup {

	@Test
	public void empty_collection_is_nofilter() {
		// arrange

		// act
		boolean isNoFilter = collection.isNoFilter();

		// assert
		assertThat(isNoFilter, is(true));
	}

	@Test
	public void populated_collection_is_nofilter() {
		// arrange
		collection.setFilters(getFilters(true,true));

		// act
		boolean isNoFilter = collection.isNoFilter();

		// assert
		assertThat(isNoFilter, is(true));
	}

	@Test
	public void populated_collection_nofilter_selected_is_nofilter() {
		// arrange
		collection.setFilters(getFilters(true,true));
		noFilterItem.setSelected(true);

		// act
		boolean isNoFilter = collection.isNoFilter();

		// assert
		assertThat(isNoFilter, is(true));
	}

	@Test
	public void populated_collection_item_selected_is_filter() {
		// arrange
		collection.setFilters(getFilters(true,true));
		testItem1.setSelected(true);

		// act
		boolean isNoFilter = collection.isNoFilter();

		// assert
		assertThat(isNoFilter, is(false));
	}

	@Test
	public void populated_collection_multiple_items_selected_is_filter() {
		// arrange
		collection.setFilters(getFilters(true,true));
		testItem1.setSelected(true);
		testItem2.setSelected(true);
		testItem3.setSelected(true);

		// act
		boolean isNoFilter = collection.isNoFilter();

		// assert
		assertThat(isNoFilter, is(false));
	}

	@Test
	public void populated_collection_all_items_selected_is_nofilter() {
		// arrange
		collection.setFilters(getFilters(true,true));
		noFilterItem.setSelected(true);
		testItem1.setSelected(true);
		testItem2.setSelected(true);
		testItem3.setSelected(true);

		// act
		boolean isNoFilter = collection.isNoFilter();

		// assert
		assertThat(isNoFilter, is(true));
	}
}
