package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.fragment.recommendation.RecommendationListFragment;
import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.presenter.listrecommendations.IListRecommendationsPresenter;

import dagger.Component;

@ActivityScope
@Component(dependencies = IApplicationComponent.class, modules = {BaseActivityModule.class, ListRecommendationsModule.class})
public interface IListRecommendationsComponent {
	// list any activities or fragments that use this component
	void inject(RecommendationListFragment fragment);

	// used by the tests
	IListRecommendationsPresenter getPresenter();
}
