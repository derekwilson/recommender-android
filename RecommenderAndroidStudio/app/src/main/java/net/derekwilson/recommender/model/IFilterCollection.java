package net.derekwilson.recommender.model;

import java.util.List;

public interface IFilterCollection {
	FilterCollection.FilterType getFilterType();

	List<FilterSelectionItem> getFilters();

	String getSearchFilter();

	void setSearchFilter(String searchFilter);

	void setFilters(List<FilterSelectionItem> filters);

	boolean isNoFilter();

	FilterSelectionItem getFilterByName(String name);

	boolean isFilterSelected(String name);

	boolean isFilterSelected(int index);

	int getNumberOfSelectedFilters();

	void setFilterSelectedByName(String name, boolean selected);

	void toggleFilterSelectedByName(String name);

	void setAllSelected(boolean selected);

	String[] getSelectedAsArray(RecommendationType type);

	String getSelectedAsParameterPlaceholder();
}
