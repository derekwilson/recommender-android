package net.derekwilson.recommender.presenter.mainactivity;

import android.content.Intent;

import net.derekwilson.recommender.fragment.recommendation.RecommendationListFragment;
import net.derekwilson.recommender.presenter.IPresenter;

public interface IMainActivityPresenter extends IPresenter<IMainActivityView> {
	void deleteAllRecommendations();

	void clearFilter();

	void toggleFilterSelectionByName(String name);

	RecommendationListFragment createTabListFragment(int tabIndex);

	int getTabListTitleId(int tabIndex);

	Intent getNewRecommendationIntent();

	void updateSearchFilter(String s);
}
