package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.activity.editRecommendation.EditRecommendationActivity;
import net.derekwilson.recommender.fragment.recommendation.CategoryBottomSheetFragment;
import net.derekwilson.recommender.fragment.recommendation.RecommendationEditFragment;
import net.derekwilson.recommender.fragment.recommendation.SourceBottomSheetFragment;
import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.IBaseActivityComponent;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.presenter.editrecommendation.IEditRecommendationPresenter;
import net.derekwilson.recommender.presenter.editrecommendationactivity.IEditRecommendationActivityPresenter;

import dagger.Component;

@ActivityScope
@Component(dependencies = IApplicationComponent.class, modules = {BaseActivityModule.class, EditRecommendationModule.class})
public interface IEditRecommendationComponent extends IBaseActivityComponent {
	// list any activities or fragments that use this component
	void inject(EditRecommendationActivity activity);
	void inject(RecommendationEditFragment fragment);
	void inject(CategoryBottomSheetFragment fragment);
	void inject(SourceBottomSheetFragment fragment);

	// used by the tests
	IEditRecommendationPresenter getFragmentPresenter();
	IEditRecommendationActivityPresenter getActivityPresenter();
}
