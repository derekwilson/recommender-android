package net.derekwilson.recommender.sharing;

import android.content.Context;

import net.derekwilson.recommender.model.Recommendation;

import java.util.List;

public interface ISharer {
	String getShareText(Context context, List<Recommendation> recommendations);
}
