package net.derekwilson.recommender.activity.preferences;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.widget.Toolbar;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.fragment.preferences.MainPreferencesFragment;

public class PreferencesActivity extends PreferenceActivity {
	private Toolbar toolbar;

	@Override
	protected void onCreate(final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		attachToolbar();
		fixToolbarTextAndIconColor(0xFFFFFFFF);

		// todo - remove this when we start to use preference headers and add onBuildHeaders, isValidFragment
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().add(R.id.prefs_wrapper, new MainPreferencesFragment()).commit();
		}
	}

	private void attachToolbar() {
		ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
		View content = root.getChildAt(0);
		LinearLayout toolbarContainer = (LinearLayout) View.inflate(this, R.layout.activity_preferences, null);

		root.removeAllViews();
		toolbarContainer.addView(content);      // this will preserve whatever was created for this class
		root.addView(toolbarContainer);

		toolbar = (Toolbar) toolbarContainer.findViewById(R.id.toolbar);
		toolbar.setTitle(getTitle());
		toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void fixToolbarTextAndIconColor(int iconColor) {
		// there is a problem with the text / icon colour when we put the toolbar on a preferences page like this
		// so we do it ourselves
		toolbar.setTitleTextColor(iconColor);
		toolbar.setSubtitleTextColor(iconColor);
		final PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(iconColor, PorterDuff.Mode.MULTIPLY);
		toolbar.getNavigationIcon().setColorFilter(colorFilter);
	}
}
