package net.derekwilson.recommender.fragment.recommendation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.fragment.BaseFragment;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.scopes.DaggerIEditRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.EditRecommendationModule;
import net.derekwilson.recommender.ioc.scopes.IEditRecommendationComponent;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.IBottomSheetView;
import net.derekwilson.recommender.presenter.editrecommendation.IEditRecommendationPresenter;
import net.derekwilson.recommender.presenter.editrecommendation.IEditRecommendationView;
import net.derekwilson.recommender.presenter.editrecommendationactivity.IEditRecommendationActivityView;

import javax.inject.Inject;

public class RecommendationEditFragment extends BaseFragment implements IEditRecommendationView {
	private static final String RECOMMENDATION_TYPE = "recommendation_type_edit";
	private static final String RECOMMENDATION = "recommendation_edit";
	private static final String FROM_SHARE_RECEIVER = "from_share_receiver";

	private IEditRecommendationComponent component;

	@Inject
	protected IEditRecommendationPresenter presenter;

	// params passed in the bundle
	private RecommendationType recommendationType;
	private Recommendation recommendation;

	public void setArguments(RecommendationType type, Recommendation recommendation, boolean fromShareReceiver) {
		Bundle args = new Bundle();
		args.putParcelable(RECOMMENDATION_TYPE, type);
		args.putParcelable(RECOMMENDATION, recommendation);
		args.putBoolean(FROM_SHARE_RECEIVER, fromShareReceiver);
		setArguments(args);
	}

	private IEditRecommendationComponent getComponent() {
		if (component == null) {
			component = DaggerIEditRecommendationComponent.builder()
					.iApplicationComponent(getApplicationComponent(getActivity()))
					.baseActivityModule(new BaseActivityModule(getActivity()))
					.editRecommendationModule(new EditRecommendationModule())
					.build();
		}
		return component;
	}

	private IEditRecommendationActivityView getActivityView() {
		if (getActivity() instanceof IEditRecommendationActivityView) {
			return (IEditRecommendationActivityView) getActivity();
		}
		return null;
	}

	private IBottomSheetView getBottomSheetView() {
		if (getActivity() instanceof IBottomSheetView) {
			return (IBottomSheetView) getActivity();
		}
		return null;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getComponent().inject(this);

		final Bundle args = getArguments();
		recommendationType = args.getParcelable(RECOMMENDATION_TYPE);
		recommendation = args.getParcelable(RECOMMENDATION);

		presenter.bindView(this);
		presenter.setIsFromShareReceiver(args.getBoolean(FROM_SHARE_RECEIVER));
	}

	protected EditText txtName;
	protected EditText txtBy;
	protected EditText txtCategory;
	protected ImageButton btnCategory;
	protected EditText txtSource;
	protected ImageButton btnSource;
	protected EditText txtNotes;
	protected EditText txtUri;
	protected ImageButton btnBrowse;
	protected TextView txtLastUpdated;
	private void bindControls(View view) {
		txtName = view.findViewById(R.id.txt_recommendation_name);
		txtBy = view.findViewById(R.id.txt_recommendation_by);
		txtCategory = view.findViewById(R.id.txt_recommendation_category);
		btnCategory = view.findViewById(R.id.expand_categories);
		btnCategory.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getBottomSheetView().showCategoryBottomSheet();
			}
		});
		txtSource = view.findViewById(R.id.txt_recommendation_source);
		btnSource = view.findViewById(R.id.expand_sources);
		btnSource.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getBottomSheetView().showSourceBottomSheet();
			}
		});
		txtNotes = view.findViewById(R.id.txt_recommendation_notes);
		txtUri = view.findViewById(R.id.txt_recommendation_uri);
		btnBrowse = view.findViewById(R.id.launch_browser_button);
		btnBrowse.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				presenter.browseToUri();
			}
		});
		txtLastUpdated = view.findViewById(R.id.txt_recommendation_last_updated);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_recommendation_edit, container, false);
		bindControls(view);
		presenter.onCreate();
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isInjected()) {
			getComponent().inject(this);
		}
	}

	@Override
	public void onDestroy() {
		presenter.onDestroy();
		super.onDestroy();
	}

	public boolean isDirty() {
		return presenter.hasBeenEdited();
	}

	public void deleteRecommendation() {
		presenter.deleteRecommendation(recommendation);
	}

	public void saveRecommendation() {
		presenter.saveRecommendation(recommendation);
	}

	public void moveRecommendation() {
		presenter.moveRecommendation(recommendation);
	}

	@Override
	public void end() {
		getActivity().finish();
	}

	@Override
	public void showError(Throwable throwable) {
		displayMessage(throwable.getMessage());
	}

	@Override
	public void showMessage(int message) {
		displayMessage(getString(message));
	}

	@Override
	public Recommendation getRecommendation() {
		return recommendation;
	}

	@Override
	public RecommendationType getRecommendationType() {
		return recommendationType;
	}

	@Override
	public void hideBrowseButton() {
		btnBrowse.setVisibility(View.GONE);
	}

	@Override
	public void copyToUi(Recommendation recommendation) {
		if (recommendation == null) {
			return;
		}
		txtName.setText(recommendation.getName());
		txtBy.setText(recommendation.getBy());
		txtCategory.setText(recommendation.getCategory());
		txtSource.setText(recommendation.getSource());
		txtNotes.setText(recommendation.getNotes());
		txtUri.setText(recommendation.getUri());
		txtLastUpdated.setText(presenter.getDateForDisplay(recommendation.getUpdated()));
	}

	@Override
	public Recommendation getRecommendationFromUi() {
		Recommendation recommendation = new Recommendation();
		recommendation.setName(txtName.getText().toString());
		recommendation.setBy(txtBy.getText().toString());
		recommendation.setCategory(txtCategory.getText().toString());
		recommendation.setSource(txtSource.getText().toString());
		recommendation.setNotes(txtNotes.getText().toString());
		recommendation.setUri(txtUri.getText().toString());
		// updated time is set in code and not edited in the UI
		return recommendation;
	}

	public void setCategory(String category) {
		txtCategory.setText(category);
	}

	public void setSource(String source) {
		txtSource.setText(source);
	}
}
