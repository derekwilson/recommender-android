package net.derekwilson.recommender.presenter.listrecommendations;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.data.repository.IdAndValues;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.NewFilterAppliedEvent;
import net.derekwilson.recommender.event.RecommendationMovedEvent;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.presenter.BasePresenter;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.INdefMessageSharer;
import net.derekwilson.recommender.sharing.IntentShareResult;
import net.derekwilson.recommender.sharing.NdefMessageShareResult;
import net.derekwilson.recommender.wrapper.ICalendarUtils;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.functions.Action1;

public class ListRecommendationsPresenter extends BasePresenter<IListRecommendationsView> implements IListRecommendationsPresenter {

	private IRecommendationRepository repository;
	private IEventBus eventBus;
	private IEmailIntentSharer emailSender;
	private INdefMessageSharer nfcSender;
	private IEventLogger eventLogger;
	private ICrashReporter crashReporter;
	private IPreferencesProvider preferences;
	private ITextUtils txtUtils;
	private ICalendarUtils calendarUtils;

	private Subscription recommendationsSubscription;
	private String currentSearchString = null;

	@Inject
	public ListRecommendationsPresenter(IRecommendationRepository repository, IEventBus eventBus, IEmailIntentSharer emailSender, INdefMessageSharer nfcSender, IEventLogger eventLogger, ICrashReporter crashReporter, IPreferencesProvider preferences, ITextUtils txtUtils, ICalendarUtils calendarUtils) {
		this.repository = repository;
		this.eventBus = eventBus;
		this.emailSender = emailSender;
		this.nfcSender = nfcSender;
		this.eventLogger = eventLogger;
		this.crashReporter = crashReporter;
		this.preferences = preferences;
		this.txtUtils = txtUtils;
		this.calendarUtils = calendarUtils;
	}

	@Override
	public void onCreate() {
		setFilter(null);
	}

	@Override
	public void onDestroy() {
		if (recommendationsSubscription != null) {
			recommendationsSubscription.unsubscribe();
			recommendationsSubscription = null;
		}
		// stop any subscriptions
		repository.close();
	}

	@Override
	public String getCurrentSearchString() {
		return currentSearchString;
	}

	@Override
	public String getRecommendationTitle(Recommendation recommendation) {
		return recommendation.getTitle();
	}

	@Override
	public String getRecommendationSubtitle(Recommendation recommendation) {
		return calendarUtils.convertCalendarToDateString(recommendation.getUpdated()) + " - " + recommendation.getSubTitle();
	}

	private List<Recommendation> getRecommendationsToShare() {
		// this list is a copy so we can play around with it without changing the DB
		List<Recommendation> recommendations = view.copySelectedRecommendations();
		// set the current user as the source
		String nickname = preferences.getPreferenceString(view.getNicknameKey());
		if (!txtUtils.isEmpty(nickname)) {
			for (Recommendation recommendation : recommendations ) {
				recommendation.setSource(nickname);
			}
		}
		// when we make a recommendation we are suggesting that the receiver "Try This"
		for (Recommendation recommendation : recommendations) {
			recommendation.setType(RecommendationType.TRY_THIS);
		};
		return recommendations;
	}

	@Override
	public void shareRecommendations() {
		List<Recommendation> recommendations = getRecommendationsToShare();
		eventLogger.logShareRecommendations(recommendations, "main-multi");
		IntentShareResult result = null;
		try {
			result = emailSender.getIntent(recommendations);
			view.startIntent(result.SendIntent, R.string.email_share_text);
		} catch (ExternalStorageException e) {
			crashReporter.logNonFatalException(e);
			view.showMessage(R.string.storage_error);
		}
	}

	@Override
	public void beamRecommendations() {
		List<Recommendation> recommendations = getRecommendationsToShare();
		eventLogger.logShareRecommendations(recommendations, "main-multi-nfc");
		NdefMessageShareResult result = nfcSender.getMessage(recommendations);
		view.sendNfcMessage(result.Message);
		view.showMessage(R.string.nfc_beam);
	}

	@Override
	public void moveRecommendations() {
		List<Recommendation> recommendations = view.copySelectedRecommendations();
		RecommendationType source = RecommendationType.RECOMMENDATION;
		for (Recommendation recommendation : recommendations) {
			source = recommendation.getType();
			recommendation.setType(recommendation.getType().toggleType());
			IdAndValues idAndValues = new IdAndValues();
			idAndValues.id = recommendation.getId();
			idAndValues.contentValues = new Recommendation.Builder()
					.fromRecommendation(recommendation)
					.build();
			repository.update(idAndValues);
		}
		if (recommendations.size() > 0 && recommendations.get(0) != null) {
			// we can fire one event as we know they are all in the same list
			eventBus.send(new RecommendationMovedEvent(source, recommendations.get(0).getType()));
		}
	}

	@Override
	public void deleteRecommendations() {
		List<Recommendation> recommendations = view.copySelectedRecommendations();
		for (Recommendation recommendation : recommendations) {
			repository.delete(String.valueOf(recommendation.getId()));
		}
	}

	@Override
	public void setFilter(IFilterCollection currentFilter) {
		if (recommendationsSubscription != null) {
			recommendationsSubscription.unsubscribe();
			recommendationsSubscription = null;
		}
		currentSearchString = null;
		if (currentFilter == null || currentFilter.getFilterType() == FilterCollection.FilterType.None) {
			// no categories and no search
			view.setNonFilteredEmptyText();
			recommendationsSubscription = repository.getByType(
					view.getRecommendationType(),
					preferences.getPreferencesSortOrder(view.getSortByKey()),
					new Action1<List<Recommendation>>() {
						@Override
						public void call(List<Recommendation> recommendations) {
							view.setRecommendations(recommendations);
							eventBus.send(new NewFilterAppliedEvent(view.getRecommendationType()));
						}
					});
		}
		else {
			currentSearchString = currentFilter.getSearchFilter();
			view.setFilteredEmptyText();
			recommendationsSubscription = repository.getByTypeWithFilter(
					view.getRecommendationType(),
					preferences.getPreferencesSortOrder(view.getSortByKey()),
					currentFilter,
					new Action1<List<Recommendation>>() {
						@Override
						public void call(List<Recommendation> recommendations) {
							view.setRecommendations(recommendations);
							eventBus.send(new NewFilterAppliedEvent(view.getRecommendationType()));
						}
					});
		}
	}
}
