package net.derekwilson.recommender.presenter;

public abstract class BasePresenter<V extends IView> implements IPresenter<V> {
	protected V view;
	@Override public void bindView(V view) { this.view = view; }
	@Override public void unbindView() { this.view = null; }
	@Override public void onCreate() {}
	@Override public void onDestroy() {}
}