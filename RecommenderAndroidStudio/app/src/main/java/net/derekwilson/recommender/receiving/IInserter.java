package net.derekwilson.recommender.receiving;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import java.util.List;

public interface IInserter {
	int insertRecommendations(List<Recommendation> recommendations);
	int insertRecommendationsIntoType(List<Recommendation> recommendations, RecommendationType type);
}
