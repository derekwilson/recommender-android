package net.derekwilson.recommender.tests.html.documentconverer;

import net.derekwilson.recommender.model.Recommendation;

import org.junit.Test;

import java.io.IOException;
import java.net.URL;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class Amazon2ConverterTests extends BaseHtmlConverterTests {
	private static String AMAZON_URL = "http://www.amazon.co.uk/Sweet-Bells-Kate-Rusby/dp/B002S53LZG/ref=sr_1_4?ie=UTF8&qid=1442388218&sr=8-4&keywords=kate+rusby";
	private static String AMAZON_2_FILENAME = "test_amazon_2.html";

	private URL testUrl;

	@Test
	public void gets_amazon_name() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_2_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getName(), is("The Martian: A Novel"));
	}

	@Test
	public void gets_amazon_by() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_2_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getBy(), is("Andy Weir"));
	}

	@Test
	public void gets_amazon_category() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_2_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void gets_amazon_notes() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_2_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getNotes(), is("Amazon.com: The Martian: A Novel eBook: Andy Weir: Kindle Store. I'm a hard-science science fiction fan and would rather read hard sc-fi than almost anything. I love stories and movies about Mars, and I'm a fan of survival, castaway, and man-against-the elements stories. I loved Robinson Crusoe, so it should not surprise you that I loved the movie, Robinson Crusoe on Mars. I realize it's not Academy Award material, but to me, it's everything I want it to be, as was this book, The Martian. The main character, Watney, presumed dead, is accidentally left by his crew mates when an intense Martian dust storm forces them to abort their mission. What follows for part of the book is a logbook style narrative that describes in great technical detail Watney's efforts to extend his life until the next scheduled mission arrives in 4 years. After reading just the first 20% of the book (my Kindle has no page numbers) one can't help but be impressed by the author's depth of knowledge in this regard. In fact, the entire book is an astronaut's primer on extraterrestrial and deep space survival and rescue. The Martian isn't without its typos and editorial glitches, and I'm not sure if this was a result of a bad Kindle conversion or just a shortsighted editor. For me, though, typos and editing issues paled in comparison to the snowballing storyline, which I gladly admit is not for everyone. This is not a touchy-feely book about love, romance or relationships. There is no overpowering angle between characters. No good guys in white hats and bad guys in black hats. There's no room for cliches. It's all very business like and scientific. So, if you're looking for Twilight in Space. Or Fifty Shades of Mars. Or Tom Hanks making himself a friend by drawing a face on a soccer ball, you'll probably want to skip this one. Read more ›"));
	}

	@Test
	public void gets_amazon_url() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_2_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getUri(), is(AMAZON_URL));
	}
}
