package net.derekwilson.recommender.tests.presenter.listrecommendations;

import android.app.Activity;

import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.presenter.listrecommendations.IListRecommendationsPresenter;
import net.derekwilson.recommender.presenter.listrecommendations.IListRecommendationsView;
import net.derekwilson.recommender.presenter.listrecommendations.ListRecommendationsPresenter;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.INdefMessageSharer;
import net.derekwilson.recommender.tests.BaseJunitTests;
import net.derekwilson.recommender.tests.testobjects.TestRecommendationRepository;
import net.derekwilson.recommender.wrapper.ICalendarUtils;
import net.derekwilson.recommender.wrapper.ITextUtils;

import org.junit.Before;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PresenterTestSetup extends BaseJunitTests {

	// mocks
	protected Activity mockActivity = mock(Activity.class);
	protected IListRecommendationsView mockView = mock(IListRecommendationsView.class);
	protected IRecommendationRepository mockRepository = mock(IRecommendationRepository.class);
	protected IPreferencesProvider mockPreferences = mock(IPreferencesProvider.class);
	protected ITextUtils mockTextUtils = mock(ITextUtils.class);
	protected IEmailIntentSharer mockEmailSharer = mock(IEmailIntentSharer.class);
	protected INdefMessageSharer mockMessageSharer = mock(INdefMessageSharer.class);
	protected IEventLogger mockEventLogger = mock(IEventLogger.class);
	protected ICrashReporter mockReporter = mock(ICrashReporter.class);
	protected ICalendarUtils mockCalendarUtils = mock(ICalendarUtils.class);

	// will use a concrete object until we have to use a mock
	protected TestRecommendationRepository realRepository = new TestRecommendationRepository();

	// object under test
	protected IListRecommendationsPresenter presenter;

	@Before
	public void setUp() {
		when(mockView.getRecommendationType()).thenReturn(RecommendationType.RECOMMENDATION);
	}

	protected void setupPresenter(boolean bind, boolean useRealRepository) {
		presenter = new ListRecommendationsPresenter(
				(useRealRepository ? realRepository : mockRepository),
				mockEventBus,
				mockEmailSharer,
				mockMessageSharer,
				mockEventLogger,
                mockReporter,
				mockPreferences,
				mockTextUtils,
                mockCalendarUtils);
		if (bind) {
			presenter.bindView(mockView);
		}
	}
}
