package net.derekwilson.recommender.tests.html.documentconverer;

import net.derekwilson.recommender.model.Recommendation;

import org.junit.Test;

import java.io.IOException;
import java.net.URL;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class Amazon4ConverterTests extends BaseHtmlConverterTests {
	private static String AMAZON_URL = "http://www.amazon.co.uk/Sweet-Bells-Kate-Rusby/dp/B002S53LZG/ref=sr_1_4?ie=UTF8&qid=1442388218&sr=8-4&keywords=kate+rusby";
	private static String AMAZON_4_FILENAME = "test_amazon_4.html";

	private URL testUrl;

	@Test
	public void gets_amazon_name() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_4_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getName(), is("The Portable Door"));
	}

	@Test
	public void gets_amazon_by() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_4_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getBy(), is("Tom Holt"));
	}

	@Test
	public void gets_amazon_category() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_4_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void gets_amazon_notes() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_4_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getNotes(), is("The Portable Door. This is a brilliant book from a writer I enjoy but find rather hit and miss. Like others have mentioned, this is Tom Holt very much 'on form', with a well-written story full..."));
	}

	@Test
	public void gets_amazon_url() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_4_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getUri(), is(AMAZON_URL));
	}
}

