package net.derekwilson.recommender.presenter.editrecommendation;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.IView;

public interface IEditRecommendationView extends IView {
	Recommendation getRecommendation();

	void copyToUi(Recommendation recommendation);

	Recommendation getRecommendationFromUi();

	RecommendationType getRecommendationType();

	void hideBrowseButton();
}
