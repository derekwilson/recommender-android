package net.derekwilson.recommender.html;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.tasks.ITaskResultHandler;
import net.derekwilson.recommender.tasks.TaskErrorResult;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class DocumentDownloader implements IDocumentDownloader {
	private ILoggerFactory logger;
	private ICrashReporter crashReporter;
	private Action1<? super Document> downloadComplete;
	private Func1<Throwable,? extends Document> downloadError;

	@Inject
	public DocumentDownloader(ILoggerFactory logger, ICrashReporter crashReporter) {
		this.logger = logger;
		this.crashReporter = crashReporter;
	}

	private Observable<Document> downloadDocument(final URL url) {
		return Observable.create(new Observable.OnSubscribe<Document>() {
                 @Override
                 public void call(Subscriber<? super Document> subscriber) {
	                 try {
		                 logger.getCurrentApplicationLogger().debug("downloading from {}", url.toString());
		                 // Connect to the web site
		                 Document document = Jsoup.connect(url.toString())
				                                    .timeout(30 * 1000)
				                                    .get();
		                 subscriber.onNext(document);
		                 subscriber.onCompleted();
	                 } catch (IOException e) {
		                 logger.getCurrentApplicationLogger().warn("cannot download html ", e);
						 crashReporter.logNonFatalException(e);
		                 subscriber.onError(e);
	                 }
	                 catch (Exception e) {
		                 logger.getCurrentApplicationLogger().warn("cannot download html ", e);
		                 crashReporter.logNonFatalException(e);
		                 subscriber.onError(e);
	                 }
                 }
             }
		);
	}

	@Override
	public Subscription getDocument(final URL url, final ITaskResultHandler<Document> handler) {

		Observable<Document> downloader = downloadDocument(url)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread());

		return downloader.subscribe(new Subscriber<Document>() {
			@Override
			public void onCompleted() {
			}

			@Override
			public void onError(Throwable e) {
				logger.getCurrentApplicationLogger().debug("downloading error from {}", url.toString());
				handler.onError(new TaskErrorResult(TaskErrorResult.INTENT_UNPACK_BAD_HTTP_READ));
			}

			@Override
			public void onNext(Document document) {
				logger.getCurrentApplicationLogger().debug("downloading complete from {}", url.toString());
				handler.onSuccess(document);
			}
		});
	}
}
