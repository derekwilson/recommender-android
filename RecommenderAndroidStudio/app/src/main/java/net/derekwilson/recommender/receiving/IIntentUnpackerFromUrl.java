package net.derekwilson.recommender.receiving;

import android.content.Intent;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.tasks.ITaskResultHandler;

import java.net.URL;

import rx.Subscription;

public interface IIntentUnpackerFromUrl {
	Subscription getRecommendationFromUrl(Intent intent, ITaskResultHandler<Recommendation> handler);
}

