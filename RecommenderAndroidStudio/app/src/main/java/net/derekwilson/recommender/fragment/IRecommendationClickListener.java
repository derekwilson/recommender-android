package net.derekwilson.recommender.fragment;

import net.derekwilson.recommender.model.Recommendation;

public interface IRecommendationClickListener {
	void onRecommendationClicked(Recommendation selected);
	void onRecommendationLongClicked(Recommendation selected);
	void onRecommendationSelectionChanged(Recommendation selected);
}


