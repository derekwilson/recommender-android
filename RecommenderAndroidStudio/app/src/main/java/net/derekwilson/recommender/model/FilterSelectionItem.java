package net.derekwilson.recommender.model;

public class FilterSelectionItem {
	private String text;
	private int iconId;
	private boolean selected;
	private boolean nofilter;

	public FilterSelectionItem(String text, int iconId, boolean selected, boolean nofilter) {
		this.text = text;
		this.iconId = iconId;
		this.selected = selected;
		this.nofilter = nofilter;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isNofilter() {
		return nofilter;
	}

	public void setNofilter(boolean nofilter) {
		this.nofilter = nofilter;
	}
}
