package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.presenter.mainactivity.IMainActivityPresenter;
import net.derekwilson.recommender.presenter.mainactivity.MainActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

	@Provides
	@ActivityScope
	IFilterCollection provideFilter(FilterCollection filter) {
		return filter;
	}

	@Provides
	@ActivityScope
	IMainActivityPresenter providePresenter(MainActivityPresenter presenter) {
		return presenter;
	}
}
