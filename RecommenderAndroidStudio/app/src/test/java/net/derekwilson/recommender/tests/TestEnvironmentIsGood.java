package net.derekwilson.recommender.tests;


import org.junit.Test;
import org.slf4j.Logger;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestEnvironmentIsGood {

	@Test
	public void mockito_can_verify() {
		// arrange
		Logger mockLogger = mock(Logger.class);

		// act
		mockLogger.debug("TEST");

		// assert
		verify(mockLogger).debug("TEST");
	}
}
