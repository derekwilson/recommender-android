package net.derekwilson.recommender.receiving;

import android.content.Intent;

import net.derekwilson.recommender.model.Recommendation;

import java.util.List;

public interface IIntentUnpackerFromNfcBeam {
	List<Recommendation> getRecommendationsFromPayload(Intent intent);
}
