package net.derekwilson.recommender.crashes;

import android.content.Context;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.logging.ILoggerFactory;

import javax.inject.Inject;

public class CrashlyticsCrashReporter implements ICrashReporter {
	// to see crashes go to
	// https://console.firebase.google.com/project/recommender-9956a/crashlytics/

	private ILoggerFactory logger;
	private Context context;

	@Inject
	public CrashlyticsCrashReporter(ILoggerFactory logger, Context context) {
		this.logger = logger;
        this.context = context;
        logger.getCurrentApplicationLogger().debug("CrashlyticsCrashReporter: ctor");
		try {
			String scaling = " Font scale: " + Float.toString(context.getResources().getConfiguration().fontScale);
			FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
			crashlytics.setCustomKey("fontScaling", scaling /* string value */);
			crashlytics.setCustomKey("uiMode", context.getString(R.string.ui_mode) /* string value */);
		} catch (Exception ex) {
			logger.getCurrentApplicationLogger().error("CrashlyticsCrashReporter: ctor - error", ex);
		}
	}

	@Override
	public void testReporting() {
		logger.getCurrentApplicationLogger().debug("CrashlyticsCrashReporter: testReporting");
		throw new RuntimeException("Test Crash Reporting");
	}

	@Override
	public void logNonFatalException(Throwable ex) {
		FirebaseCrashlytics.getInstance().recordException(ex);
	}
}

/*
 * - cannot leave this code here as we have removed the dependencies
 *
public class AppCenterCrashReporter implements ICrashReporter {
	// to see crashes go to
	// https://appcenter.ms/users/derekwilson/apps/RecommenderDebug/crashes/errors

	@Inject
	public AppCenterCrashReporter() {
	}

	@Override
	public void testReporting() {
		// Note - this will only do anything in a debug build
		Crashes.generateTestCrash();
	}

	@Override
	public void logNonFatalException(Throwable ex) {
		Crashes.trackError(ex);
	}
}

*/