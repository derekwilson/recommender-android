package net.derekwilson.recommender.presenter.mainactivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.editRecommendation.EditRecommendationActivity;
import net.derekwilson.recommender.event.DbSubscriptionRestartRequired;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.NewFilterAppliedEvent;
import net.derekwilson.recommender.event.NewSortOrderApplied;
import net.derekwilson.recommender.event.RecommendationAddedEvent;
import net.derekwilson.recommender.event.RecommendationMovedEvent;
import net.derekwilson.recommender.fragment.recommendation.RecommendationListFragment;
import net.derekwilson.recommender.model.FilterSelectionItem;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

public class MainActivityPresenter extends BasePresenter<IMainActivityView> implements IMainActivityPresenter {

	private Subscription categorySubscription = null;
	private Subscription eventBusSubscription = null;

	private IFilterCollection currentFilter;
	private IRecommendationRepository repository;
	private ILoggerFactory logger;
	private Context context;
	private IEventBus eventBus;
	private Activity activityContext;

	@Inject
	public MainActivityPresenter(IFilterCollection currentFilter, IRecommendationRepository repository, ILoggerFactory logger, Context context, IEventBus eventBus, Activity activityContext) {
		this.currentFilter = currentFilter;
		this.repository = repository;
		this.logger = logger;
		this.context = context;
		this.eventBus = eventBus;
		this.activityContext = activityContext;
	}

	@Override
	public void onCreate() {
		addCategoriesToFilterList();
		Observable<Object> bus = eventBus.toObservable();
		if (bus != null) {
			eventBusSubscription = bus.subscribe(new Action1<Object>() {
						@Override
						public void call(Object event) {
							logger.getCurrentApplicationLogger().debug("MainActivityPresenter Event received: {}", event.getClass());
							if (event instanceof RecommendationAddedEvent) {
								setActiveTab(((RecommendationAddedEvent) event).getDestination());
							} else if (event instanceof RecommendationMovedEvent) {
								setActiveTab(((RecommendationMovedEvent) event).getDestination());
							} else if (event instanceof NewFilterAppliedEvent) {
								view.updateTabTitles(convertTypeToTabIndex(((NewFilterAppliedEvent) event).getRecommendationType()));
							} else if (event instanceof DbSubscriptionRestartRequired ||
										event instanceof NewSortOrderApplied)
							{
								logger.getCurrentApplicationLogger().debug("subscription restart signalled");
								if (categorySubscription != null) {
									categorySubscription.unsubscribe();
								}
								// this will also force the lists to reapply their filter
								// which will restart their subscription
								addCategoriesToFilterList();
							}
						}
					});
		}
	}

	private int convertTypeToTabIndex(RecommendationType type) {
		// this logic needs to match the order of the fragments inserted into the viewpager in the view
		// we are assuming that the tabs are in the following order
		// 0 == I Recommend, 1 == Try This
		// if that ever changes then we will need to rewrite this logic
		switch (type) {
			case RECOMMENDATION:
				return 0;
			case TRY_THIS:
				return 1;
		}
		throw new IllegalArgumentException("Unknown list type: " + type);
	}

	private RecommendationType convertTabIndexToType(int tabIndex) {
		if (tabIndex == 0) {
			return RecommendationType.RECOMMENDATION;
		}
		if (tabIndex == 1) {
			return RecommendationType.TRY_THIS;
		}
		throw new IllegalArgumentException("Unknown tab index: " + tabIndex);
	}

	private void setActiveTab(RecommendationType type) {
		RecommendationType currentType = convertTabIndexToType(view.getActiveTab());
		if (type.equals(currentType)) {
			logger.getCurrentApplicationLogger().debug("setActiveTab supressed as not needed {}", type);
		}
		view.setActiveTab(convertTypeToTabIndex(type));
	}

	@Override
	public RecommendationListFragment createTabListFragment(int tabIndex) {
		RecommendationListFragment fragment = RecommendationListFragment.getNewFragment(convertTabIndexToType(tabIndex));
		logger.getCurrentApplicationLogger().debug("createTabListFragment index {}", tabIndex);
		return fragment;
	}

	@Override
	public int getTabListTitleId(int tabIndex) {
		switch (convertTabIndexToType(tabIndex)) {
			case RECOMMENDATION:
				return R.string.list_title_recommendation;
			case TRY_THIS:
				return R.string.list_title_trythis;
		}
		throw new IllegalArgumentException("Unknown tab index: " + tabIndex);
	}

	@Override
	public Intent getNewRecommendationIntent() {
	 return EditRecommendationActivity.getStartIntent(activityContext, convertTabIndexToType(view.getActiveTab()), null);
	}

	@Override
	public void onDestroy() {
		// stop any subscriptions
		if (categorySubscription != null) {
			categorySubscription.unsubscribe();
			categorySubscription = null;
		}
		if (eventBusSubscription != null) {
			eventBusSubscription.unsubscribe();
			eventBusSubscription = null;
		}
		repository.close();
	}

	@Override
	public void deleteAllRecommendations() {
		repository.deleteAll();
	}

	private void addCategoriesToFilterList() {
		categorySubscription = repository.getCategories(new Action1<List<String>>() {
			@Override
			public void call(List<String> categories) {
				logger.getCurrentApplicationLogger().debug("getCategories updated");

				List<FilterSelectionItem> newFilter = null;
				int initialSize = 1;
				if (categories != null) {
					initialSize = categories.size() + 1;
				}
				newFilter = new ArrayList<>(initialSize);
				newFilter.add(new FilterSelectionItem(
						context.getString(R.string.action_filter_none),
						R.drawable.ic_action_collection,
						false,
						true
				));
				if (categories != null) {
					for (String category : categories) {
						logger.getCurrentApplicationLogger().debug("Category {}", category);
						newFilter.add(new FilterSelectionItem(
								category,
								R.drawable.ic_action_collection,
								currentFilter.isFilterSelected(category),
								false
						));
					}
				}
				currentFilter.setFilters(newFilter);
				view.showCategoryFilter(currentFilter);
				view.applyFilterToAllTabs(currentFilter);
			};
		});
	}

	@Override
	public void clearFilter() {
		currentFilter.setAllSelected(false);
		view.setCategoryFilterUiState(currentFilter);
		view.applyFilterToAllTabs(currentFilter);
	}

	@Override
	public void toggleFilterSelectionByName(String name) {
		currentFilter.toggleFilterSelectedByName(name);
		view.setCategoryFilterUiState(currentFilter);
		view.applyFilterToAllTabs(currentFilter);
	}

	@Override
	public void updateSearchFilter(String s) {
		logger.getCurrentApplicationLogger().debug("updateSearchFilter {}", s);
		currentFilter.setSearchFilter(s);
		view.applyFilterToAllTabs(currentFilter);
	}
}
