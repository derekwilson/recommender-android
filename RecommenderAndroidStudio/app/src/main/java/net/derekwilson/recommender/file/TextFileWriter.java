package net.derekwilson.recommender.file;

import android.content.Context;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.logging.ILoggerFactory;

import java.io.File;
import java.io.FileWriter;

import javax.inject.Inject;

public class TextFileWriter implements ITextFileWriter {
	private ILoggerFactory logger;
	private ICrashReporter crashReporter;
	Context applicationContext;

	@Inject
	public TextFileWriter(ILoggerFactory logger, ICrashReporter crashReporter, Context applicationContext) {
		this.logger = logger;
		this.crashReporter = crashReporter;
		this.applicationContext = applicationContext;
	}

	private String getDestinationFolder() {
		// we are limited where we can write unless we ask for extra permissions in android 11+
		File directory = applicationContext.getExternalFilesDir(null);
		// throw if we cannot get the path
		return directory.getAbsolutePath() + "/";
	}
	public String getDestinationPathname(String filename) {
		return getDestinationFolder() + filename;
	}


	private boolean prepareSdCard() {
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			File sDir = new File(getDestinationFolder());
			if (!sDir.exists()) {
				sDir.mkdirs();
			}
		} else {
			logger.getCurrentApplicationLogger().error("There is no memory card available. This application requires a memory card on which to store the files.");
			return false;
		}
		return true;
	}

	private void writeTextFile(String fileName, String txt){
		try {
			FileWriter writer = new FileWriter(new File(fileName),true);
			writer.append(txt);
			writer.append("\n");
			writer.flush();
			writer.close();
		} catch (Exception e) {
			logger.getCurrentApplicationLogger().error("Unable to write to {}", fileName,e);
			crashReporter.logNonFatalException(e);
		}
	}

	@Override
	public void writeStringToFile(String filename, String text) {
		synchronized (this) {
			if (!prepareSdCard()) {
				logger.getCurrentApplicationLogger().debug("failed to prepare sdcard");
				crashReporter.logNonFatalException(new ExternalStorageException(filename, null));
				return;
			}
			logger.getCurrentApplicationLogger().debug("writing to {}", getDestinationPathname(filename));
			writeTextFile(getDestinationPathname(filename), text);
			return;
		}
	}
}
