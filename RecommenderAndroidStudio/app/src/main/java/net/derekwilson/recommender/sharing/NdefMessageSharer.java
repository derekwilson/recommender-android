package net.derekwilson.recommender.sharing;

import android.content.Context;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;

import net.derekwilson.recommender.json.IExporter;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class NdefMessageSharer extends BaseSharer implements INdefMessageSharer {

	private final ILoggerFactory logger;
	private final IExporter exporter;

	private final Context applicationContext;

	@Inject
	public NdefMessageSharer(ILoggerFactory logger, IExporter exporter, Context applicationContext) {
		this.logger = logger;
		this.exporter = exporter;
		this.applicationContext = applicationContext;
	}

	private NdefMessage createNdefMessage(String text) {
		// our package names are different for debug builds - we can only share dbg <=> dbg and prod <=> prod
		String packageName = applicationContext.getPackageName();
		NdefMessage msg = new NdefMessage(
				new NdefRecord[]{
						NdefRecord.createMime("application/json", text.getBytes()),
						NdefRecord.createApplicationRecord(packageName)
				}
		);
		return msg;
	}

	@Override
	public NdefMessageShareResult getMessage(Recommendation recommendation) {
		List<Recommendation> recommendations = new ArrayList<Recommendation>();
		recommendations.add(recommendation);
		return getMessage(recommendations);
	}

	@Override
	public NdefMessageShareResult getMessage(List<Recommendation> recommendations) {
		String shareText = exporter.exportRecommendationsToString(recommendations);

		NdefMessageShareResult result = new NdefMessageShareResult();
		result.NumberOfRecommendations = recommendations.size();
		result.Message = createNdefMessage(shareText);
		return result;
	}
}
