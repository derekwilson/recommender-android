package net.derekwilson.recommender.fragment.recommendation;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.fragment.IRecommendationClickListener;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.listrecommendations.IListRecommendationsPresenter;
import net.derekwilson.recommender.view.HighlightTextView;
import net.derekwilson.recommender.wrapper.CalendarUtils;
import net.derekwilson.recommender.wrapper.ICalendarUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecommendationRecyclerAdapter extends RecyclerView.Adapter<RecommendationRecyclerAdapter.RecyclerViewHolder> {
	private IRecommendationClickListener clickListener;

	private boolean isSelectable = false;
	private SparseBooleanArray selectedPositions = new SparseBooleanArray();
	private List<Recommendation> items = Collections.emptyList();

	private IListRecommendationsPresenter presenter;

	public RecommendationRecyclerAdapter(IListRecommendationsPresenter presenter, List<Recommendation> items) {
		this.presenter = presenter;
		this.items = items;
	}

	public void setClickListener(IRecommendationClickListener listener) {
		this.clickListener = listener;
	}

	@Override
	public int getItemCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}

	@NonNull
	@Override
	public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.list_item_recommendation, parent, false);
		return new RecyclerViewHolder(view);
	}

	@Override
	public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
		Recommendation thisRecommendation = items.get(position);

		holder.bind(
				presenter.getRecommendationTitle(thisRecommendation),
				presenter.getRecommendationSubtitle(thisRecommendation),
				presenter.getCurrentSearchString(),
				isSelectable()
		);
		holder.view.setSelected(isItemSelected(position));
		holder.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isSelectable()) {
					// Selection is active; toggle activation
					toggleSelection(position);
				} else {
					if (clickListener != null) {
						clickListener.onRecommendationClicked(items.get(position));
					}
				}
			}
		});
		holder.view.setLongClickable(true);
		holder.view.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (clickListener != null) {
					clickListener.onRecommendationLongClicked(items.get(position));
					toggleSelection(position);
					return true;
				}
				return false;
			}
		});
	}

	public void call(List<Recommendation> recommendations) {
		this.items = recommendations;
		selectedPositions = new SparseBooleanArray();
		notifyDataSetChanged();
	}

	private boolean isItemSelected(int position) {
		return selectedPositions.get(position);
	}

	public void setSelectable(boolean selectable) {
		isSelectable = selectable;
	}

	private boolean isSelectable() {
		return isSelectable;
	}

	public void clearSelections() {
		selectedPositions.clear();
		notifyDataSetChanged();
		if (clickListener != null) {
			clickListener.onRecommendationSelectionChanged(null);
		}
	}

	public void toggleSelection(int pos) {
		if (selectedPositions.get(pos, false)) {
			selectedPositions.delete(pos);
		}
		else {
			selectedPositions.put(pos, true);
		}
		notifyItemChanged(pos);
		if (clickListener != null) {
			clickListener.onRecommendationSelectionChanged(items.get(pos));
		}
	}

	public int getSelectedItemCount() {
		return selectedPositions.size();
	}

	public List<Integer> getSelectedItemPositions() {
		List<Integer> items = new ArrayList<Integer>(selectedPositions.size());
		for (int i = 0; i < selectedPositions.size(); i++) {
			items.add(selectedPositions.keyAt(i));
		}
		return items;
	}

	/***
	 * this will return a list of recommendations that are selected
	 * these are copies of the ones in the list so we can change them if we want
	 */
	public List<Recommendation> getSelectedRecommendations() {
		List<Integer> selectedItems = getSelectedItemPositions();
		List<Recommendation> selectedRecommendations = new ArrayList<Recommendation>(selectedItems.size());
		for (int itemIndex : selectedItems) {
			selectedRecommendations.add(new Recommendation(items.get(itemIndex)));
		}
		return selectedRecommendations;
	}

	public class RecyclerViewHolder extends RecyclerView.ViewHolder {
		View view;

		HighlightTextView labelView;
		TextView subLabelView;

		public RecyclerViewHolder(View view)
		{
			super(view);
			this.view = view;
			labelView = view.findViewById(R.id.recommendation_row_label);
			subLabelView = view.findViewById(R.id.recommendation_row_sub_label);
		}

		public void bind(String label, String sublabel, String search, boolean isSelectable) {
			view.setClickable(true);
			setSelectable(isSelectable);
			if (isSelectable) {
				// we need to do this programmatically as using ?attr/selectableItemBackground in a drawable causes non nexus device to crash
				view.setBackgroundResource(R.drawable.selectable_list_item);
			}
			labelView.setText(label);
			labelView.setHighlightedText(search);
			subLabelView.setText(sublabel);
		}
	}
}
