package net.derekwilson.recommender.tests.html.documentconverer;

import net.derekwilson.recommender.model.Recommendation;
import org.junit.Test;
import java.io.IOException;
import java.net.URL;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class Amazon5ConverterTests extends BaseHtmlConverterTests {
	private static String AMAZON_URL = "http://www.amazon.co.uk/Sweet-Bells-Kate-Rusby/dp/B002S53LZG/ref=sr_1_4?ie=UTF8&qid=1442388218&sr=8-4&keywords=kate+rusby";
	private static String AMAZON_5_FILENAME = "test_amazon_5.html";

	private URL testUrl;

	@Test
	public void gets_amazon_name() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_5_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getName(), is("The Night Manager"));
	}

	@Test
	public void gets_amazon_by() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_5_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getBy(), is("John Le Carré"));
	}

	@Test
	public void gets_amazon_category() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_5_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void gets_amazon_notes() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_5_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getNotes(), is("The Night Manager: Amazon.co.uk: John Le Carré: 9780340937686: Books. I first read this spy novel in 1993 shortly after its publication, and have re-read it many times since then - most recently last week. It's probably my favourite le Carré book (and I've read all of them), blending a gripping, original plot with deftly-drawn characters and dialogue which resonates in the mind so precisely you can practically hear the voices in your head. Take, for example, this initial exchange [p24] between the two protagonists, which quickly tells you a great deal about who they are, and the differences between them: \"I'm Dicky Roper,\" a lazy voice announced as the hand closed round Jonathan's, and briefly owned it. \"My chaps booked some rooms here. Rather a lot of 'em. How d'you do?\" Belgravia slur, the proletarian accent of the vastly rich. They had entered each other's private space. \"How very good to see you, Mr Roper,\" Jonathan murmured, English voice to English voice. \"Welcome back, sir, and poor you, what a perfectly ghastly journey you must have had. Wasn't it rather heroic to venture aloft at all? No one else has, I can tell you. My name's Pine, I'm the night manager.\" The story is about Pine's penetration of Roper's luxurious, nefarious world as the latter engineers an enormous deal involving drugs, arms, investment bankers and corrupt UK and US officials. Themes that run through the story include sacrifice, honour, courage, love and (as is de rigeur with this author) betrayal. But the delight for the reader (especially the re-reader) comes in picking out eye-catching examples of the writing: a landscape is \"dour and blowy like Scotland with the lights on\", \"shoeless\" pelicans sit \"like feathery old bombing planes that might never bomb again\", the noise made by a well-fed government minister is \"a kind of slurrying grunt\", and Pine's spymaster describes himself as \"the other kind of Yorkshireman\". Just perfect."));
	}

	@Test
	public void gets_amazon_url() throws IOException {
		// arrange
		testUrl = new URL(AMAZON_URL);
		testDocument = loadHtml(AMAZON_5_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getUri(), is(AMAZON_URL));
	}
}

