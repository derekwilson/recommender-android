package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.fragment.recommendation.RecommendationSelectFragment;
import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.IBaseActivityComponent;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.presenter.selectrecommendation.ISelectRecommendationPresenter;

import dagger.Component;

@ActivityScope
@Component(dependencies = IApplicationComponent.class, modules = {BaseActivityModule.class, SelectRecommendationModule.class})
public interface ISelectRecommendationComponent extends IBaseActivityComponent {
	// list any activities or fragments that use this component
	void inject(RecommendationSelectFragment fragment);

	// used by the tests
	ISelectRecommendationPresenter getPresenter();
}


