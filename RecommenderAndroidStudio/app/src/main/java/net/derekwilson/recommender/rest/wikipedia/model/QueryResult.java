package net.derekwilson.recommender.rest.wikipedia.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
		"batchcomplete",
		"query"
})
public class QueryResult {

	@JsonProperty("batchcomplete")
	private String batchcomplete;
	@JsonProperty("query")
	private Query query;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 *
	 * @return
	 * The batchcomplete
	 */
	@JsonProperty("batchcomplete")
	public String getBatchcomplete() {
		return batchcomplete;
	}

	/**
	 *
	 * @param batchcomplete
	 * The batchcomplete
	 */
	@JsonProperty("batchcomplete")
	public void setBatchcomplete(String batchcomplete) {
		this.batchcomplete = batchcomplete;
	}

	/**
	 *
	 * @return
	 * The query
	 */
	@JsonProperty("query")
	public Query getQuery() {
		return query;
	}

	/**
	 *
	 * @param query
	 * The query
	 */
	@JsonProperty("query")
	public void setQuery(Query query) {
		this.query = query;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

