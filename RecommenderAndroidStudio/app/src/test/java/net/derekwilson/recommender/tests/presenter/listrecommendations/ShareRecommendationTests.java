package net.derekwilson.recommender.tests.presenter.listrecommendations;

import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.IntentShareResult;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.fail;
import static net.derekwilson.recommender.tests.testobjects.Matchers.orNull;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class ShareRecommendationTests extends PresenterTestSetup {
	@Captor
	private ArgumentCaptor<List<Recommendation>> emailSharerCaptor;

	private static final String NICKNAME = "TEST NICKNAME";

	private List<Recommendation> recommendations = new ArrayList<>(5);
	private IPreferencesProvider preferences;

	@Before
	public void setUp() {
		super.setUp();
		setupPresenter(true, false);
		when(mockView.copySelectedRecommendations()).thenReturn(recommendations);
		when(mockPreferences.getPreferenceString(orNull(anyString()))).thenReturn(NICKNAME);
		attachCaptor();
	}

	private void attachCaptor() {
		// so we can use generic captors
		MockitoAnnotations.initMocks(this);
		try {
			when(mockEmailSharer.getIntent(emailSharerCaptor.capture())).thenReturn(new IntentShareResult());
		} catch (ExternalStorageException e) {
			fail();
		}
	}

	@Test
	public void source_is_set_when_sharing() {
		// arrange
		Recommendation recommendation = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendations.add(recommendation);

		// act
		presenter.shareRecommendations();

		// assert
		List<Recommendation> recommendationToShare = getAllCapturedValuesAndAssertExpectedCount(emailSharerCaptor,1).get(0);
		assertThat("source has not been set", recommendationToShare.get(0).getSource(), is(NICKNAME));
	}
}
