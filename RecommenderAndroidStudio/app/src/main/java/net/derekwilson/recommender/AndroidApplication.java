package net.derekwilson.recommender;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import androidx.multidex.MultiDexApplication;

import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.RecommenderDatabaseHelper;
import net.derekwilson.recommender.ioc.ApplicationModule;
import net.derekwilson.recommender.ioc.DaggerIApplicationComponent;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.logging.SlfLoggerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

public class AndroidApplication extends MultiDexApplication {

	private IApplicationComponent applicationComponent;

	private Thread.UncaughtExceptionHandler defaultExceptionHandler;

	// Deliberately not using IoC for this - as this class is where IoC is setup
	final private Logger logger = LoggerFactory.getLogger(AndroidApplication.class);

	@Inject
	protected IEventLogger eventLogger;
	@Inject
	protected ICrashReporter crashReporter;

	@Override
	public void onCreate() {
		logger.warn("Application started v{}, debug {}, production build {}", getVersionName(getApplicationContext()), BuildConfig.DEBUG, RecommenderBuildConfig.PRODUCTION);
		// Setup handler for uncaught exceptions.
		defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable e) {
				handleUncaughtException(thread, e);
			}
		});
		// uncover to log system information
		//logger.debug(getSystemInformation(this));
		super.onCreate();
		this.applicationComponent = initialiseInjector();
		this.applicationComponent.inject(this);
		logger.debug("Application IoC bound");
		initialiseAnalytics();
		initialiseDatabase();
		logger.debug("Database initialised");
	}

	@Override
	public void onTerminate() {
		logger.debug("Application onTerminate");
		super.onTerminate();
	}

	private void handleUncaughtException(Thread thread, Throwable e) {
		// record it in the log file
		logger.error("Unhandled exception: ", e);
		defaultExceptionHandler.uncaughtException(thread, e);
	}

	protected IApplicationComponent initialiseInjector() {
		return DaggerIApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.build();
	}

	protected void initialiseDatabase() {
		// this will force the DB to be created / upgraded
		RecommenderDatabaseHelper _dbHelper = new RecommenderDatabaseHelper(this, new SlfLoggerFactory(), crashReporter);
		_dbHelper.openAndCloseDatabase();
	}

	protected void initialiseAnalytics() {
		String scaling = Float.toString(getResources().getConfiguration().fontScale);
		eventLogger.logStartup(scaling, getString(R.string.ui_mode));
	}

	public IApplicationComponent getApplicationComponent() {
		return this.applicationComponent;
	}

	public static String getVersionName(Context applicationContext) {
		String versionName;
		final PackageManager packageManager = applicationContext.getPackageManager();
		if (packageManager != null) {
			try {
				PackageInfo packageInfo = packageManager.getPackageInfo(applicationContext.getPackageName(), 0);
				versionName = packageInfo.versionName + " - " + BuildConfig.GIT_HASH;
			} catch (PackageManager.NameNotFoundException e) {
				versionName = "UNKNOWN";
			}
			return versionName;
		}
		return "UNKNOWN";
	}

	public static String getSystemInformation(Context applicationContext) {
		StringBuilder sysInfo = new StringBuilder();
		String[] ids = TimeZone.getAvailableIDs();
		sysInfo.append("\n\n")
				.append("App Version: ")
				.append(getVersionName(applicationContext)).append('\n')
				.append("System Information").append('\n')
				.append("Device model: ").append(android.os.Build.MODEL).append(" (").append(android.os.Build.DISPLAY).append(')').append('\n')
				.append("OS version: ").append(android.os.Build.VERSION.RELEASE).append(" (").append(android.os.Build.VERSION.INCREMENTAL).append(')').append('\n')
				.append("UI language: ").append(Locale.getDefault().getLanguage()).append('\n')
				.append("TZ IDs: ").append(String.join(",", ids))
				;
		return sysInfo.toString();
	}
}
