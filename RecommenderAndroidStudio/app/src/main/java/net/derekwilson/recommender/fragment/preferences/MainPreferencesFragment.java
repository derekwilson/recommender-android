package net.derekwilson.recommender.fragment.preferences;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.widget.Toast;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.BuildConfig;
import net.derekwilson.recommender.R;
import net.derekwilson.recommender.RecommenderBuildConfig;
import net.derekwilson.recommender.activity.preferences.OpenSourceLicensesActivity;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.data.utility.IDatabaseCopier;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.NewSortOrderApplied;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.json.IExporter;
import net.derekwilson.recommender.json.IImporter;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.receiving.IInserter;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.IntentShareResult;
import net.derekwilson.recommender.view.SnackbarHelper;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.functions.Action1;

public class MainPreferencesFragment extends BasePreferencesFragment {

	private final int REQUEST_SELECT_FILE = 3000;
	private static final String URL_PRIVACY = "https://bitbucket.org/derekwilson/recommender-android/src/master/PIRVACY.md";

	@Inject
	protected IExporter exporter;

	@Inject
	protected IImporter importer;

	@Inject
	protected IInserter inserter;

	@Inject
	protected IDatabaseCopier databaseCopier;

	@Inject
	protected IEmailIntentSharer emailSender;

	@Inject
	protected IRecommendationRepository recommendationRepository;

	@Inject
	protected IPreferencesProvider preferences;

	@Inject
	protected ITextUtils txtUtils;

	@Inject
	protected ICrashReporter crashReporter;

	@Inject
	protected IEventBus eventBus;

	private Subscription subscription;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// there isn't enough business logic to require a presenter
		this.getBaseActivityComponent(getActivity()).inject(this);        // will inject into the base as well as this class

		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.fragment_main_preferences);

		String nickname = preferences.getPreferenceString(getString(R.string.key_nickname));
		if (!txtUtils.isEmpty(nickname)) {
			findPreference(getString(R.string.key_nickname)).setSummary(
					nickname
			);
		}
		findPreference(getString(R.string.key_nickname)).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				preferences.setPreferenceString(getString(R.string.key_nickname), newValue.toString());
				if (txtUtils.isEmpty(newValue.toString())) {
					preference.setSummary(R.string.description_nickname);
				} else {
					preference.setSummary(newValue.toString());
				}
				return true;
			}
		});

		IRecommendationRepository.SortBy sortBy = preferences.getPreferencesSortOrder(getString(R.string.key_sortby));
		findPreference(getString(R.string.key_sortby)).setSummary(
					getString(R.string.description_sortby) + " - " + sortBy.toString()
			);
		findPreference(getString(R.string.key_sortby)).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				preferences.setPreferenceString(getString(R.string.key_sortby), newValue.toString());
				preference.setSummary(getString(R.string.description_sortby) + " - " + newValue.toString());
				// refresh the display
				eventBus.send(new NewSortOrderApplied(newValue.toString()));
				return true;
			}
		});

		String scaling = " Font scale: " + Float.toString(getResources().getConfiguration().fontScale);
		String versionSuffix = (RecommenderBuildConfig.PRODUCTION ? " (prod)" : " (dev)");
		findPreference(getString(R.string.key_version)).setSummary(
				"Version " + AndroidApplication.getVersionName(getActivity().getApplicationContext()) +
						versionSuffix +
						scaling
		);
		if (!RecommenderBuildConfig.PRODUCTION) {
			findPreference(getString(R.string.key_version)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference preference) {
					crashReporter.testReporting();
					return true;
				}
			});
		}

		String exportSummary = getString(R.string.description_export_error);
		try {
			exportSummary = getString(R.string.description_export) + "\n" + exporter.getExportUri();
		} catch (Exception ex) {
			crashReporter.logNonFatalException(ex);
		}
		findPreference(getString(R.string.key_export)).setSummary(exportSummary);

		findPreference(getString(R.string.key_export)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				exportAllRecommendations(false);
				return true;
			}
		});

		findPreference(getString(R.string.key_import)).setSummary(
				getString(R.string.description_import)
		);

		findPreference(getString(R.string.key_import)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				selectFile();
				return true;
			}
		});

		String shareSummary = getString(R.string.description_share_error);
		try {
			shareSummary = getString(R.string.description_share1) + "\n" + importer.getImportUri() + "\n" + getString(R.string.description_share2);
		} catch (Exception ex) {
			crashReporter.logNonFatalException(ex);
		}
		findPreference(getString(R.string.key_share)).setSummary(shareSummary);

		findPreference(getString(R.string.key_share)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				shareAllRecommendation();
				return true;
			}
		});

		PreferenceCategory category = (PreferenceCategory) findPreference(getString(R.string.key_category_rec));
		String copySummary = getString(R.string.description_copydb_error);
		try {
			copySummary = getString(R.string.description_copydb) + "\n" + databaseCopier.getDestinationPathname();
		} catch (Exception ex) {
			crashReporter.logNonFatalException(ex);
		}
		Preference copydbPreference = findPreference(getString(R.string.key_copydb));
		if (copydbPreference != null && !BuildConfig.DEBUG) {
			// dont offer this for release builds
			category.removePreference(copydbPreference);
		} else {
			findPreference(getString(R.string.key_copydb)).setSummary(copySummary);

			findPreference(getString(R.string.key_copydb)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference preference) {
					copyDb();
					displayMessage("Database copied");
					return true;
				}
			});
		}

		findPreference(getString(R.string.key_license)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				// we cannot do this in the XML file as our package name changes between flavours
				Intent intent = new Intent(getActivity(), OpenSourceLicensesActivity.class);
				startActivity(intent);
				return true;
			}
		});

		findPreference(getString(R.string.key_privacy)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIVACY));
				startActivity(browserIntent);
				return true;
			}
		});
	}

	private void copyDb() {
		if (databaseCopier.copyDatabaseFile(databaseCopier.getDestinationPathname())) {
			displayMessage("Database copied OK");
		} else {
			displayMessage("Error, check log for details");
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isInjected()) {
			this.getBaseActivityComponent(getActivity()).inject(this);        // will inject into the base as well as this class
		}
	}

	@Override
	public void onDestroy() {
		if (subscription != null && subscription.isUnsubscribed()) {
			subscription.unsubscribe();     // in case its in progress
		}
		recommendationRepository.close();   // cancel any outstanding subscriptions
		super.onDestroy();
	}

	private void exportAllRecommendations(boolean thenShare) {
		subscription = recommendationRepository.getAll(new Action1<List<Recommendation>>() {
			@Override
			public void call(List<Recommendation> recommendations) {
				subscription.unsubscribe();
				if (recommendations != null) {
					if (recommendations.size() < 1) {
						// do not overwrite whatever backup they did have with an empty one
						displayMessage(getString(R.string.nothing_to_backup));
					} else {
						try {
							exporter.exportRecommendations(recommendations, null);
							displayMessage(Integer.toString(recommendations.size()) + " Recommendations exported");
							if (thenShare) {
								IntentShareResult result = emailSender.getIntent(exporter.getExportUri());
								startActivity(Intent.createChooser(result.SendIntent, getString(R.string.email_share_all_text)));
							}
						} catch (ExternalStorageException e) {
							crashReporter.logNonFatalException(e);
							displayMessage(getString(R.string.storage_error));
						}
					}
				}
			}
		});
	}

	public void shareAllRecommendation() {
		exportAllRecommendations(true);
	}

	private void selectFile() {
		// ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file browser.
		Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

		// Filter to only show results that can be "opened", such as a
		// file (as opposed to a list of contacts or timezones)
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

		// Filter using the MIME type.
		// If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
		// To search for all documents available via installed storage providers, it would be "*/*".
		// as we know that other apps do not always report GPX MIME type correctly lets try for everything
		intent.setType("*/*");

		startActivityForResult(intent, REQUEST_SELECT_FILE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		logger.getCurrentApplicationLogger().debug("Activity Result code {} Request {}", resultCode, requestCode);
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
				case REQUEST_SELECT_FILE:
					InputStream stream = null;
					try {
						ContentResolver resolver = getActivity().getContentResolver();
						logger.getCurrentApplicationLogger().debug("opening stream from {}", data.getData());
						stream = resolver.openInputStream(data.getData());
						importAllRecommendations(stream);
					} catch (IOException e) {
						logger.getCurrentApplicationLogger().error("Error reading {}", data.getData(), e);
						crashReporter.logNonFatalException(new ExternalStorageException(data.getData().toString(), e));
					} finally {
						try {
							if (stream != null) {
								stream.close();
							}
						} catch (IOException e) {
							logger.getCurrentApplicationLogger().error("Error closing {}", data.getData(), e);
							crashReporter.logNonFatalException(new ExternalStorageException(data.getData().toString(), e));
						}
					}
			}
		}
	}

	private void importAllRecommendations(InputStream stream) {
		// TODO move this to a background thread?
		List<Recommendation> recommendations = importer.importRecommendations(stream);
		if (recommendations == null || recommendations.size() < 1) {
			displayMessage(getString(R.string.nothing_to_restore));
			return;
		}
		int numberInserted = inserter.insertRecommendations(recommendations);
		displayMessage(Integer.toString(numberInserted) + getString(R.string.restored_suffix));
	}

	private void displayMessage(String message) {
		if (!SnackbarHelper.displayMessageAsSnackbar(getActivity(), message)) {
			displayMessageAsToast(message);
		}
	}

	private void displayMessageAsToast(String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}
}

