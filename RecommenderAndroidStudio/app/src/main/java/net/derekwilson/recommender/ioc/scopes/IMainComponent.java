package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.activity.main.MainActivity;
import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.presenter.mainactivity.IMainActivityPresenter;

import dagger.Component;

@ActivityScope
@Component(dependencies = IApplicationComponent.class, modules = {BaseActivityModule.class, MainModule.class})
public interface IMainComponent {
	// list any activities or fragments that use this component
	void inject(MainActivity activity);

	// used by the tests
	IMainActivityPresenter getPresenter();
	IFilterCollection getFilter();
}
