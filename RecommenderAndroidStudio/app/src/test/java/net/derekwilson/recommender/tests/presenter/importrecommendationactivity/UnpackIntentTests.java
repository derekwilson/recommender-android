package net.derekwilson.recommender.tests.presenter.importrecommendationactivity;

import android.content.Intent;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.receiving.IIntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromContentResolver;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromNfcBeam;
import net.derekwilson.recommender.receiving.IntentContentType;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UnpackIntentTests extends PresenterTestSetup {
	List<Recommendation> recommendations = new ArrayList<Recommendation>(1);
	List<Recommendation> empty_recommendations = new ArrayList<Recommendation>(1);

	public UnpackIntentTests() {
		recommendations.add(new Recommendation());
	}

	@Test
	public void exit_if_unknown_type() {
		// arrange
		setupPresenter(true);
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.Unknown);

		// act
		presenter.unpackIntent(intent, null);

		// assert
		verify(mockView, times(1)).showMessage(R.string.cannot_import);
		verify(mockView, times(1)).end();
	}

	@Test
	public void exit_if_empty_recommendations() {
		// arrange
		setupPresenter(true);
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.RecommenderNfcBeam);
		when(mockUnpackerFromBeam.getRecommendationsFromPayload(intent)).thenReturn(empty_recommendations);

		// act
		presenter.unpackIntent(intent, null);

		// assert
		verify(mockView, times(1)).showMessage(R.string.cannot_import);
		verify(mockView, times(1)).end();
	}

	@Test
	public void exit_if_null_recommendations() {
		// arrange
		setupPresenter(true);
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.RecommenderNfcBeam);
		when(mockUnpackerFromBeam.getRecommendationsFromPayload(intent)).thenReturn(null);

		// act
		presenter.unpackIntent(intent, null);

		// assert
		verify(mockView, times(1)).showMessage(R.string.cannot_import);
		verify(mockView, times(1)).end();
	}

	@Test
	public void unpack_if_nfc() {
		// arrange
		setupPresenter(true);
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.RecommenderNfcBeam);
		when(mockUnpackerFromBeam.getRecommendationsFromPayload(intent)).thenReturn(recommendations);

		// act
		presenter.unpackIntent(intent, null);

		// assert
		verify(mockView, never()).end();
		verify(mockUnpackerFromBeam, times(1)).getRecommendationsFromPayload(intent);
	}

	@Test
	public void unpack_if_content() {
		// arrange
		setupPresenter(true);
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.ContentUri);
		when(mockUnpackerFromContent.getRecommendationsFromContent(intent, null)).thenReturn(recommendations);

		// act
		presenter.unpackIntent(intent, null);

		// assert
		verify(mockView, never()).end();
		verify(mockUnpackerFromContent, times(1)).getRecommendationsFromContent(intent, null);
	}

	@Test
	public void unpack_if_file() {
		// arrange
		setupPresenter(true);
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.FileUri);
		when(mockUnpackerFromContent.getRecommendationsFromContent(intent,null)).thenReturn(recommendations);

		// act
		presenter.unpackIntent(intent, null);

		// assert
		verify(mockView, never()).end();
		verify(mockUnpackerFromContent, times(1)).getRecommendationsFromContent(intent,null);
	}
}
