package net.derekwilson.recommender.presenter.selectrecommendation;

import net.derekwilson.recommender.presenter.IPresenter;

public interface ISelectRecommendationPresenter extends IPresenter<ISelectRecommendationView> {
}
