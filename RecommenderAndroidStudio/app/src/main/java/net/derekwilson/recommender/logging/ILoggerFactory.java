package net.derekwilson.recommender.logging;

import org.slf4j.Logger;

public interface ILoggerFactory {
	Logger getCurrentApplicationLogger();
}
