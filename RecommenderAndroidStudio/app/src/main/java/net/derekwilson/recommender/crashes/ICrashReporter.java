package net.derekwilson.recommender.crashes;

public interface ICrashReporter {
	void testReporting();
	void logNonFatalException(Throwable ex);
}
