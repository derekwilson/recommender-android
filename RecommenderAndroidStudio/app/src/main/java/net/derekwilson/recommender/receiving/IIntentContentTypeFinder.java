package net.derekwilson.recommender.receiving;

import android.content.Intent;

public interface IIntentContentTypeFinder {
	IntentContentType getTypeOfContent(Intent intent);
}
