# Recommender #

This is the Recommender Android app (and related things).

## What is Recommender?

Recommender is an application to store, organize and share personal recommendations: music, films, books, apps, web sites, restaurants – pretty much anything.

Its designed to be used in person where you want to pass on a recommendation to a friend. There is no web service, there is no need to register you details anywhere. This is not an anonymous crowd sourced opinion. Its intended for low volume but high value recommendations.

All data is stored on your device, it can be shared or backed up into a common textual interchange format (JSON), or can be backed up by Google Drive with your other applications.

You can share recommendations with anyone via email. Though it is a more straightforward process if the other person also has Recommender installed.

Also: Tap to Share
Any two NFC enabled devices can now "beam" recommendations directly from device to device using NFC. No email or server is involved.

## Benifits

1. No server or registration, all your data is local and private
1. Add recommendations from other apps by sharing into the app
1. Share from the app to send recommendations to others
1. Easy backup and restore via a text JSON file or Google Drive
1. There are no adverts
1. The source is open and available
1. There is no cost to using Recommender for any purpose.

## AndroidStudio

This project uses AndroidStudio and Gradle, the project is in a folder called RecommenderAndroidStudio. 

You will need to download AndroidStudio from here

http://developer.android.com/sdk/index.html

You will also need the Java SE JDK

http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html

## Building for debug

```gradlew assembleDebug```

Or use android studio

### Building for release

```build```

or

```build_set_jdk```

To sign the release APK you will need the `LocalOnly` folder which has the keys and passwords in it. This folder is not checked into the repo.

### Building from the command line

Note that if you use `gradlew assembleDebug` or `build.cmd` then it will use the environment variable `JAVA_HOME` to find the JDK. If that is set incorrectly you will see an error like this

```
FAILURE: Build failed with an exception.

* Where:
Build file 'D:\Data\Code\andrew-and-derek\trailblazer\TrailblazerAndroid\app\build.gradle' line: 17

* What went wrong:
A problem occurred evaluating project ':app'.
> Failed to apply plugin 'com.android.internal.application'.
   > Android Gradle plugin requires Java 17 to run. You are currently using Java 11.
      Your current JDK is located in C:\Program Files\Microsoft\jdk-11.0.14.9-hotspot
      You can try some of the following options:
       - changing the IDE settings.
       - changing the JAVA_HOME environment variable.
       - changing `org.gradle.java.home` in `gradle.properties`.
```

As you can see this error means that the configured JDK is incompatible with AGP.

You may want or need to override that variable, if you do then you can set it via the command line `gradlew assembleRelease "-Dorg.gradle.java.home=C:\Program Files\Android\Android Studio\jbr"`. The `build_set_jdk.cmd` script is an example of how to set the JDK used to be the one embedded in Android Studio JDK. This will only work if you have installed Android Studio in the default location and it contains a JDK that is compatibly with AGP.


### Installing the release

Run the script `CopyBuild` and then CD to the folder `AndroidSupport\deploy` and then install the APK like this `adb install -r recommender-1.6.0-34-b208c00-release.apk`

### Running the unit tests

```gradlew testDebugUnitTest```

The test results HTML is here
app\build\reports\tests\debug\index.html

The test XML is here
app\build\test-results\debug\*.xml
