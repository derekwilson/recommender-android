package net.derekwilson.recommender.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.squareup.sqlbrite.SqlBrite;

import net.derekwilson.recommender.data.utility.DatabaseFieldConverter;
import net.derekwilson.recommender.data.schema.RecommendationTable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rx.functions.Func1;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Recommendation implements Parcelable {
	private long id;
	private String name;
	private String by;
	private String category;
	private String notes;
	private String uri;
	private Calendar updated;
	private RecommendationType type;
	private String source;

	@JsonIgnore
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBy() {
		return by;
	}

	public void setBy(String by) {
		this.by = by;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Calendar getUpdated() {
		return updated;
	}

	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

	public RecommendationType getType() {
		return type;
	}

	public void setType(RecommendationType type) {
		this.type = type;
	}

	public void setTypeFromInt(int type) {
		this.type = RecommendationType.values()[type];
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@JsonIgnore
	public String getTitle() {
		if (getBy() != null && !getBy().isEmpty()) {
			return getName() + " - " + getBy();
		}
		return getName();
	}

	@JsonIgnore
	public String getSubTitle() {
		if (source != null && !source.isEmpty()) {
			return category + " from " + source;
		}
		return category;
	}

	public Recommendation(Recommendation source) {
		this.id = source.id;
		this.name = source.name;
		this.by = source.by;
		this.category = source.category;
		this.notes = source.notes;
		this.uri = source.uri;
		this.updated = source.updated;
		this.type = source.type;
		this.source = source.source;
	}

	public static final Func1<SqlBrite.Query, List<Recommendation>> MAP = new Func1<SqlBrite.Query, List<Recommendation>>() {
		@Override
		public List<Recommendation> call(SqlBrite.Query query) {
			Cursor cursor = query.run();
			try {
				List<Recommendation> values = new ArrayList<>(cursor.getCount());
				while (cursor.moveToNext()) {
					Recommendation newRecommendation = new Recommendation();
					newRecommendation.setId(DatabaseFieldConverter.getLong(cursor, RecommendationTable.COLUMN_ID));
					newRecommendation.setName(DatabaseFieldConverter.getString(cursor, RecommendationTable.COLUMN_NAME));
					newRecommendation.setBy(DatabaseFieldConverter.getString(cursor, RecommendationTable.COLUMN_BY));
					newRecommendation.setCategory(DatabaseFieldConverter.getString(cursor, RecommendationTable.COLUMN_CATEGORY));
					newRecommendation.setNotes(DatabaseFieldConverter.getString(cursor, RecommendationTable.COLUMN_NOTES));
					newRecommendation.setUri(DatabaseFieldConverter.getString(cursor, RecommendationTable.COLUMN_URI));
					newRecommendation.setUpdated(DatabaseFieldConverter.getCalendar(cursor, RecommendationTable.COLUMN_UPDATED));
					newRecommendation.setTypeFromInt(DatabaseFieldConverter.getInt(cursor, RecommendationTable.COLUMN_TYPE));
					newRecommendation.setSource(DatabaseFieldConverter.getString(cursor, RecommendationTable.COLUMN_SOURCE));
					values.add(newRecommendation);
				}
				return values;
			} finally {
				cursor.close();
			}
		}
	};

	public static final Func1<SqlBrite.Query, List<String>> MAPCATEGORIES = new Func1<SqlBrite.Query, List<String>>() {
		@Override
		public List<String> call(SqlBrite.Query query) {
			Cursor cursor = query.run();
			try {
				List<String> values = new ArrayList<>(cursor.getCount());
				while (cursor.moveToNext()) {
					values.add(DatabaseFieldConverter.getString(cursor, RecommendationTable.COLUMN_CATEGORY));
				}
				return values;
			} finally {
				cursor.close();
			}
		}
	};

	public static final Func1<SqlBrite.Query, List<String>> MAPSOURCES = new Func1<SqlBrite.Query, List<String>>() {
		@Override
		public List<String> call(SqlBrite.Query query) {
			Cursor cursor = query.run();
			try {
				List<String> values = new ArrayList<>(cursor.getCount());
				while (cursor.moveToNext()) {
					values.add(DatabaseFieldConverter.getString(cursor, RecommendationTable.COLUMN_SOURCE));
				}
				return values;
			} finally {
				cursor.close();
			}
		}
	};

	public static final class Builder {
		private final ContentValues values = new ContentValues();

		private String forceNullToEmpty(String s) {
			return  (s == null) ? "" : s;
		}

		// all in one step
		public Builder fromRecommendation(Recommendation recommendation) {
			return type(recommendation.getType())
					.name(recommendation.getName())
					.by(recommendation.getBy())
					.category(recommendation.getCategory())
					.notes(recommendation.getNotes())
					.uri(recommendation.getUri())
					.source(recommendation.getSource())
					.updated(recommendation.getUpdated())			// copy from source
					;
		}

		public Builder id(long id) {
			values.put(RecommendationTable.COLUMN_ID, id);
			return this;
		}

		public Builder name(String name) {
			values.put(RecommendationTable.COLUMN_NAME, name);
			return this;
		}

		public Builder recommendation() {
			values.put(RecommendationTable.COLUMN_TYPE, RecommendationType.RECOMMENDATION.getValue());
			return this;
		}

		public Builder tryThis() {
			values.put(RecommendationTable.COLUMN_TYPE, RecommendationType.TRY_THIS.getValue());
			return this;
		}

		public Builder type(RecommendationType type) {
			values.put(RecommendationTable.COLUMN_TYPE, type.getValue());
			return this;
		}

		public Builder by(String by) {
			values.put(RecommendationTable.COLUMN_BY, forceNullToEmpty(by));
			return this;
		}

		public Builder category(String category) {
			values.put(RecommendationTable.COLUMN_CATEGORY, forceNullToEmpty(category));
			return this;
		}

		public Builder notes(String notes) {
			values.put(RecommendationTable.COLUMN_NOTES, forceNullToEmpty(notes));
			return this;
		}

		public Builder uri(String uri) {
			values.put(RecommendationTable.COLUMN_URI, forceNullToEmpty(uri));
			return this;
		}

		public Builder source(String source) {
			values.put(RecommendationTable.COLUMN_SOURCE, forceNullToEmpty(source));
			return this;
		}

		public Builder updated() {
			values.put(RecommendationTable.COLUMN_UPDATED, DatabaseFieldConverter.convertCalendarTimeToDbString(Calendar.getInstance()));
			return this;
		}

		public Builder updated(Calendar updated) {
			values.put(RecommendationTable.COLUMN_UPDATED, DatabaseFieldConverter.convertCalendarTimeToDbString(updated));
			return this;
		}

		public ContentValues build() {
			return values;
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeParcelable(type,flags);
		dest.writeString(name);
		dest.writeString(by);
		dest.writeString(category);
		dest.writeString(notes);
		dest.writeString(uri);
		dest.writeValue(updated);
		dest.writeString(source);
	}

	protected Recommendation(Parcel in) {
		id = in.readLong();
		type = (RecommendationType) in.readParcelable(RecommendationType.class.getClassLoader());
		name = in.readString();
		by = in.readString();
		category = in.readString();
		notes = in.readString();
		uri = in.readString();
		updated = (Calendar) in.readValue(null);
		source = in.readString();
	}

	public Recommendation() {
		id = -1;
	}

	public static final Parcelable.Creator<Recommendation> CREATOR = new Parcelable.Creator<Recommendation>() {
		public Recommendation createFromParcel(Parcel in) {
			return new Recommendation(in);
		}

		public Recommendation[] newArray(int size) {
			return new Recommendation[size];
		}
	};
}