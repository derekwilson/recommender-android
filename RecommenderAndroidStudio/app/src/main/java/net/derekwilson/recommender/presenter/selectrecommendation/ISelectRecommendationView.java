package net.derekwilson.recommender.presenter.selectrecommendation;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.IView;

import java.util.List;

public interface ISelectRecommendationView extends IView {
	List<Recommendation> getSelected();
}
