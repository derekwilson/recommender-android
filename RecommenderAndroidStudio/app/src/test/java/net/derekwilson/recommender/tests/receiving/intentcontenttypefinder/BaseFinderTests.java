package net.derekwilson.recommender.tests.receiving.intentcontenttypefinder;

import net.derekwilson.recommender.file.ITextFileWriter;
import net.derekwilson.recommender.receiving.IntentContentTypeFinder;
import net.derekwilson.recommender.tests.BaseJunitTests;

import org.junit.Before;

import static org.mockito.Mockito.mock;

public class BaseFinderTests extends BaseJunitTests {
	// mocks
	protected ITextFileWriter mockWriter = mock(ITextFileWriter.class);

	// object under test
	protected IntentContentTypeFinder finder;

	@Before
	public void setup() {
		finder = new IntentContentTypeFinder(mockLoggerFactory);
	}
}
