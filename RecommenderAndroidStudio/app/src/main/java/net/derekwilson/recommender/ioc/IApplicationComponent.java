package net.derekwilson.recommender.ioc;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.activity.help.HelpActivity;
import net.derekwilson.recommender.activity.preferences.OpenSourceLicensesActivity;
import net.derekwilson.recommender.activity.preferences.PreferencesActivity;
import net.derekwilson.recommender.activity.shareReceiveRecommendation.ShareReceiveRecommendationActivity;
import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.utility.IDatabaseCopier;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.file.IAssetFile;
import net.derekwilson.recommender.json.IExporter;
import net.derekwilson.recommender.json.IImporter;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.receiving.IInserter;
import net.derekwilson.recommender.receiving.IIntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromContentResolver;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromNfcBeam;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromText;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromUrl;
import net.derekwilson.recommender.sharing.IBeamAvailabilityProvider;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.INdefMessageSharer;
import net.derekwilson.recommender.wrapper.ICalendarUtils;
import net.derekwilson.recommender.wrapper.ITextUtils;

import javax.inject.Singleton;

import dagger.Component;

@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = ApplicationModule.class)
public interface IApplicationComponent {
	// do not forget to list ALL classes that can ask to be injected
	// remove any that do NOT use this component
	void inject(AndroidApplication app);
	void inject(PreferencesActivity activity);
	void inject(OpenSourceLicensesActivity activity);
	void inject(HelpActivity activity);

	// fragments without a scoped component just use the application component

	// we only need to put things in here that are exposed to subgraphs
	// for example objects that are supplied by the application component but used by scoped components such as presenters
	Context context();
	IPreferencesProvider providePreferences();
	ILoggerFactory provideLogger();
	IDatabaseCopier provideCopier();
	SqlBrite provideSqlBrite();
	BriteDatabase provideDatabase();
	IEventLogger provideEventLogger();
	ICrashReporter provideCrashReporter();
	ITextUtils provideTextUtils();
	ICalendarUtils provideCalendarUtils();
	IBeamAvailabilityProvider provideBeamAvailabilityProvider();
	IEmailIntentSharer provideEmailSharer();
	INdefMessageSharer provideNfcSharer();
	IExporter provideExporter();
	IImporter provideImporter();
	IInserter provideInserter();
	IEventBus provideEventBus();
	IIntentContentTypeFinder provideIntentContentTypeFinder();
	IIntentUnpackerFromContentResolver provideIntentUnpackerFromContentResolver();
	IIntentUnpackerFromNfcBeam provideIntentUnpackerFromNfcBeam();
	IIntentUnpackerFromUrl provideIntentUnpackerUrl();
	IIntentUnpackerFromText provideIntentUnpackerFromText();
	IAssetFile provideAssetFile();
	Html.ImageGetter provideImageGetter();
}