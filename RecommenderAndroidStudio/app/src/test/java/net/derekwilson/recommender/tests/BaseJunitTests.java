package net.derekwilson.recommender.tests;

import android.content.Intent;
import android.net.Uri;

import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import org.mockito.ArgumentCaptor;
import org.slf4j.Logger;

import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BaseJunitTests {
	protected ILoggerFactory mockLoggerFactory = mock(ILoggerFactory.class);
	private Logger logger = mock(Logger.class);
	protected IEventBus mockEventBus = mock(IEventBus.class);

	public BaseJunitTests() {
		when(mockLoggerFactory.getCurrentApplicationLogger()).thenReturn(logger);
	}

	protected Recommendation setupRecommendation(String name, String by, String category, String notes, String uri, String source) {
		Recommendation recommendation = new Recommendation();
		recommendation.setType(RecommendationType.RECOMMENDATION);
		recommendation.setName(name);
		recommendation.setBy(by);
		recommendation.setCategory(category);
		recommendation.setNotes(notes);
		recommendation.setUri(uri);
		recommendation.setSource(source);
		return recommendation;
	}

	protected Intent setupIntent(String action, String extraText, String urlScheme, String urlPath) {
		Intent intent = mock(Intent.class);
		when(intent.getAction()).thenReturn(action);
		when(intent.getStringExtra(Intent.EXTRA_TEXT)).thenReturn(extraText);

		if (urlScheme != null) {
			Uri uri = mock(Uri.class);
			when(uri.getScheme()).thenReturn(urlScheme);
			when(uri.getPath()).thenReturn(urlPath);
			when(uri.getSchemeSpecificPart()).thenReturn(urlPath);
			when(intent.getData()).thenReturn(uri);
		}

		return intent;
	}

	/**
	 * Returns all captured values by the given captor. Fails with error if null or doesn't meet expected size
	 */
	protected <T> List<T> getAllCapturedValuesAndAssertExpectedCount(ArgumentCaptor<T> captor, int expectedSize) {
		List<T> capturedCallbacks = captor.getAllValues();
		assertNotNull("callback has not been captured by the code under test but we are attempting to call it", capturedCallbacks);
		assertThat("incorrect number of arguments have been captured by the code under test", capturedCallbacks.size(), is(expectedSize));
		return capturedCallbacks;
	}
}
