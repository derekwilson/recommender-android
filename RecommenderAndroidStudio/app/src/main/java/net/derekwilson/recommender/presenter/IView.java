package net.derekwilson.recommender.presenter;

public interface IView {
	void end();
	void showError(Throwable throwable);
	void showMessage(int messageId);
}
