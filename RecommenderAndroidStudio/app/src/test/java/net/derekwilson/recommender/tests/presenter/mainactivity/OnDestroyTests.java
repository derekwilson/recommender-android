package net.derekwilson.recommender.tests.presenter.mainactivity;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class OnDestroyTests extends PresenterTestsSetup {

	@Before
	public void setUp() {
		setupPresenter(true, false);
	}

	@Test
	public void the_repository_is_closed() {
		// arrange
		presenter.onCreate();

		// act
		presenter.onDestroy();

		// assert
		verify(mockRepository, times(1)).close();
	}
}
