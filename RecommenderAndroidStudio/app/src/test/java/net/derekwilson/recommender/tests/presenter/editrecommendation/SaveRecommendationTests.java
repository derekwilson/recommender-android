package net.derekwilson.recommender.tests.presenter.editrecommendation;

import android.content.ContentValues;

import net.derekwilson.recommender.data.repository.IdAndValues;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.RecommendationAddedEvent;
import net.derekwilson.recommender.model.RecommendationType;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SaveRecommendationTests extends PresenterTestsSetup {

	@Before
	public void setUp() {
		super.setUp();
		setupPresenter(true, false);
	}

	@Test
	public void calls_create_for_new_recommendation() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri","source");

		// act
		presenter.saveRecommendation(recommendationPassedToActivity);

		// assert
		verify(mockRepository, times(1)).create((ContentValues) any());
	}

	@Test
	public void fires_added_event_for_created_recommendation() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri","source");
		when(mockView.getRecommendationType()).thenReturn(RecommendationType.TRY_THIS);

		// act
		presenter.saveRecommendation(recommendationPassedToActivity);

		// assert
		ArgumentCaptor<RecommendationAddedEvent> eventCaptor = ArgumentCaptor.forClass(RecommendationAddedEvent.class);
		verify(mockEventBus, times(1)).send(eventCaptor.capture());

		List<RecommendationAddedEvent> capturedEvents = eventCaptor.getAllValues();
		assertThat("there should only be one event", capturedEvents.size(), is(1));
		assertThat("it should be a try_this", capturedEvents.get(0).getDestination(), is(RecommendationType.TRY_THIS));
	}

	@Test
	public void calls_create_for_new_recommendation2() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri","source");

		// act
		presenter.saveRecommendation(recommendationPassedToActivity);

		// assert
		verify(mockRepository, times(1)).create((ContentValues) any());
	}

	@Test
	public void calls_update_for_existing_recommendation() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationPassedToActivity.setId(99);
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri","source");

		// act
		presenter.saveRecommendation(recommendationPassedToActivity);

		// assert
		verify(mockRepository, times(1)).update((IdAndValues) any());
	}

	@Test
	public void does_not_fire_any_event_for_updated_recommendation() {
		// arrange
		recommendationPassedToActivity = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendationPassedToActivity.setId(99);
		recommendationFromUi = setupRecommendation("name", "by", "category", "notes", "uri","source");

		// act
		presenter.saveRecommendation(recommendationPassedToActivity);

		// assert
		verify(mockEventBus, never()).send(any());
	}
}
