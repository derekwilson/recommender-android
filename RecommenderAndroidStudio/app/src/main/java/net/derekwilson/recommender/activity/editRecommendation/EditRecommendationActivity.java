package net.derekwilson.recommender.activity.editRecommendation;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.DialogFragment;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.BaseActivity;
import net.derekwilson.recommender.fragment.recommendation.CategoryBottomSheetFragment;
import net.derekwilson.recommender.fragment.recommendation.RecommendationEditFragment;
import net.derekwilson.recommender.fragment.recommendation.SourceBottomSheetFragment;
import net.derekwilson.recommender.fragment.utility.OkCancelDialogFragment;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.scopes.DaggerIEditRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.EditRecommendationModule;
import net.derekwilson.recommender.ioc.scopes.IEditRecommendationComponent;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.IBottomSheetView;
import net.derekwilson.recommender.presenter.editrecommendationactivity.IEditRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.editrecommendationactivity.IEditRecommendationActivityView;

import javax.inject.Inject;


public class EditRecommendationActivity extends BaseActivity
		implements
			IEditRecommendationActivityView,
			IBottomSheetView,
			OkCancelDialogFragment.Listener,
			View.OnClickListener
{
	public static final String ACTIVITY_PARAM_RECOMMENDATION_TYPE = "recommendation_type";
	public static final String ACTIVITY_PARAM_RECOMMENDATION = "recommendation";
	private static final String TAG_EDIT_FRAGMENT = "edit_fragment_tag";
	private static final String TAG_SAVE_DISCARD = "save_discard_tag";
	private static final String TAG_DELETE = "delete_tag";

	private static final String KEY_EDIT_FRAGMENT_TAG = "edit_fragment_key";

	public static Intent getStartIntent(Activity activity,  RecommendationType recommendationType, Recommendation recommendation) {
		Intent intent = new Intent(activity, EditRecommendationActivity.class);
		intent.putExtra(EditRecommendationActivity.ACTIVITY_PARAM_RECOMMENDATION_TYPE, (Parcelable) recommendationType);
		intent.putExtra(EditRecommendationActivity.ACTIVITY_PARAM_RECOMMENDATION, recommendation);
		return intent;
	}

	private IEditRecommendationComponent component;

	@Inject
	protected IEditRecommendationActivityPresenter presenter;

	private RecommendationType recommendationType;

	private RecommendationEditFragment editFragment;
	private CategoryBottomSheetFragment categoryFragment;
	private SourceBottomSheetFragment sourceFragment;

	protected FloatingActionButton fab;
	protected CollapsingToolbarLayout collapsingToolbar;
	protected ImageView backdropImageView;
	@Override
	protected void bindControls() {
		super.bindControls();
		fab = findViewById(R.id.fab);
		collapsingToolbar = findViewById(R.id.collapsing_toolbar);
		backdropImageView = findViewById(R.id.backdrop);
	}

	protected IEditRecommendationComponent getActivityComponent(Activity activity) {
		if (component == null) {
			component = DaggerIEditRecommendationComponent.builder()
					.iApplicationComponent(getApplicationComponent())
					.baseActivityModule(new BaseActivityModule(activity))
					.editRecommendationModule(new EditRecommendationModule())
					.build();
		}
		return component;
	}

	@Override
	protected void injectDependencies(IApplicationComponent applicationComponent) {
		getActivityComponent(this).inject(this);    // will inject into the base as well as this class
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_edit_recommendation;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		if (editFragment != null) {
			String fragmentTag = editFragment.getTag();
			if (fragmentTag != null) {
				outState.putString(KEY_EDIT_FRAGMENT_TAG, fragmentTag);
			}
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		toolbarBackButtonNeeded = true;
		super.onCreate(savedInstanceState);
		logger.getCurrentApplicationLogger().debug("EditRecommendation started");
		presenter.bindView(this);

		if (savedInstanceState == null) {
			recommendationType = (RecommendationType) getIntent().getParcelableExtra(ACTIVITY_PARAM_RECOMMENDATION_TYPE);
			Recommendation recommendation = (Recommendation) getIntent().getParcelableExtra(ACTIVITY_PARAM_RECOMMENDATION);
			editFragment = new RecommendationEditFragment();
			editFragment.setArguments(recommendationType, recommendation, false);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.fragment_container, editFragment, TAG_EDIT_FRAGMENT)
					.commit();
		} else {
			// we are being restored - go find the fragment
			String fragmentTag = savedInstanceState.getString(KEY_EDIT_FRAGMENT_TAG);
			if (fragmentTag != null) {
				editFragment = (RecommendationEditFragment)getSupportFragmentManager().findFragmentByTag(fragmentTag);
			}
		}
		categoryFragment = new CategoryBottomSheetFragment();
		sourceFragment = new SourceBottomSheetFragment();
		fab.setOnClickListener(this);
		presenter.onCreate();
	}

	@Override
	public void onDestroy() {
		logger.getCurrentApplicationLogger().debug("EditRecommendation.onDestroy()");
		presenter.unbindView();
		presenter.onDestroy();
		super.onDestroy();
	}

	private void enableMenuItem(Menu menu, int id, boolean enable) {
		menu.findItem(id).setVisible(enable).setEnabled(enable);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.action_edit_recommendation, menu);

		// is this safe ? what happens if this is called after we have edited the recommendation
		Recommendation recommendation = (Recommendation) getIntent().getParcelableExtra(ACTIVITY_PARAM_RECOMMENDATION);

		enableMenuItem(menu, R.id.action_shareRecommendation, presenter.canShare(recommendation));
		enableMenuItem(menu, R.id.action_beamRecommendation, presenter.canShare(recommendation) && nfcAdapter != null);
		enableMenuItem(menu, R.id.action_deleteRecommendation, presenter.canDelete(recommendation));
		enableMenuItem(menu, R.id.action_moveRecommendation, presenter.canMove(recommendation));

		menu.findItem(R.id.action_moveRecommendation).setTitle(presenter.getMoveMenuTextId(recommendationType));

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_deleteRecommendation:
				deleteRecommendationsPrompt();
				break;
			case R.id.action_shareRecommendation:
				presenter.shareRecommendation();
				break;
			case R.id.action_beamRecommendation:
				presenter.beamRecommendation();
				break;
			case R.id.action_moveRecommendation:
				moveRecommendation();
				finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.fab:
				saveRecommendation();
				finish();
				break;
		}
	}

	private RecommendationEditFragment getEditFragment() {
		return (RecommendationEditFragment) getSupportFragmentManager().findFragmentByTag(TAG_EDIT_FRAGMENT);
	}

	@Override
	public void onBackPressed() {
		final RecommendationEditFragment fragment = getEditFragment();

		if (fragment != null) {
			if (fragment.isDirty()) {
				showSaveDiscardDialog();
				return;
			}
		}
		super.onBackPressed();
	}

	protected void showSaveDiscardDialog() {
		DialogFragment newFragment = OkCancelDialogFragment.newInstance(
				getString(R.string.save_discard_message),
				getString(R.string.ok_cancel_save),
				getString(R.string.ok_cancel_discard)
		);
		newFragment.show(getSupportFragmentManager(), TAG_SAVE_DISCARD);
	}

	private void saveRecommendation() {
		final RecommendationEditFragment fragment = getEditFragment();
		if (fragment != null) {
			fragment.saveRecommendation();
		}
	}

	private void moveRecommendation() {
		final RecommendationEditFragment fragment = getEditFragment();
		if (fragment != null) {
			fragment.moveRecommendation();
		}
	}

	private void deleteRecommendationsPrompt() {
		DialogFragment newFragment = OkCancelDialogFragment.newInstance(
				getString(R.string.delete_single_message),
				getString(R.string.ok_cancel_yes),
				getString(R.string.ok_cancel_no)
		);
		newFragment.show(getSupportFragmentManager(), TAG_DELETE);
	}

	private void deleteRecommendation() {
		final RecommendationEditFragment fragment = getEditFragment();
		if (fragment != null) {
			fragment.deleteRecommendation();
		}
	}

	@Override
	public void ok(String tag) {
		logger.getCurrentApplicationLogger().debug("OK ({})", tag);
		if (tag.equals(TAG_SAVE_DISCARD)) {
			saveRecommendation();
			finish();
		}
		else if (tag.equals(TAG_DELETE)) {
			deleteRecommendation();
			finish();
		}
	}

	@Override
	public void cancel(String tag) {
		logger.getCurrentApplicationLogger().debug("Cancel ({})", tag);
		if (tag.equals(TAG_SAVE_DISCARD)) {
			finish();
		}
		else if (tag.equals(TAG_DELETE)) {
			// nothing
		}
	}

	@Override
	public void end() {
		finish();
	}

	@Override
	public void showError(Throwable throwable) {
		displayMessage(throwable.getMessage());
	}

	@Override
	public void showMessage(int message) {
		displayMessage(getString(message));
	}

	@Override
	public RecommendationType getRecommendationType() {
		return recommendationType;
	}

	@Override
	public void setMainTitleTextId(int id) {
		collapsingToolbar.setTitle(getString(id));
	}

	@Override
	public Recommendation getRecommendationFromPassedIntent() {
		Recommendation recommendation = (Recommendation) getIntent().getParcelableExtra(ACTIVITY_PARAM_RECOMMENDATION);
		return recommendation;
	}

	@Override
	public String getNicknameKey() {
		return getString(R.string.key_nickname);
	}

	@Override
	public void sendNfcMessage(NdefMessage message) {
		nfcAdapter.setNdefPushMessage(message, this);
	}

	@Override
	public void startIntent(Intent intent, int promptId) {
		startActivity(Intent.createChooser(intent, getString(promptId)));
	}

	@Override
	public void showCategoryBottomSheet() {
		categoryFragment.showBottomSheet(getSupportFragmentManager(), editFragment);
	}

	@Override
	public void showSourceBottomSheet() {
		sourceFragment.showBottomSheet(getSupportFragmentManager(), editFragment);
	}
}
