package net.derekwilson.recommender.presenter.editrecommendation;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.data.repository.IdAndValues;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.RecommendationAddedEvent;
import net.derekwilson.recommender.event.RecommendationMovedEvent;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.BasePresenter;
import net.derekwilson.recommender.wrapper.ICalendarUtils;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import javax.inject.Inject;

public class EditRecommendationPresenter extends BasePresenter<IEditRecommendationView> implements IEditRecommendationPresenter {

	private ILoggerFactory logger;
	private IRecommendationRepository repository;
	private Context context;
	private IEventBus eventBus;
	private ICrashReporter crashReporter;
	private ICalendarUtils calendarUtils;
	private boolean isFromShareReceiver;

	@Inject
	public EditRecommendationPresenter(ILoggerFactory logger, IRecommendationRepository repository, Context context, IEventBus eventBus, ICrashReporter crashReporter, ICalendarUtils calendarUtils) {
		this.logger = logger;
		this.repository = repository;
		this.context = context;
		this.eventBus = eventBus;
		this.crashReporter = crashReporter;
		this.calendarUtils = calendarUtils;
	}

	@Override
	public void setIsFromShareReceiver(boolean fromShareReceiver) {
		isFromShareReceiver = fromShareReceiver;
	}

	@Override
	public String getDateForDisplay(Calendar date) {
		if (date == null) {
			return "";
		}
		return calendarUtils.convertCalendarToDateTimeString(date);
	}

	@Override
	public void onCreate() {
		if (isFromShareReceiver) {
			view.hideBrowseButton();
		}
		Recommendation recommendation = view.getRecommendation();
		view.copyToUi(recommendation);
	}

	@Override
	public void onDestroy() {
		// stop any subscriptions
		repository.close();
	}

	private boolean isFieldDirty(String ui, String db) {
		if (ui.equals(db)) {
			return false;
		}
		if (ui.isEmpty() && db == null) {
			return false;
		}
		return true;
	}

	@Override
	public boolean hasBeenEdited() {
		Recommendation recommendationPassedToView = view.getRecommendation();
		Recommendation recommendationFromUi = view.getRecommendationFromUi();
		if (recommendationPassedToView == null) {
			// create
			if (recommendationFromUi.getName().length() > 0) {
				return true;
			}
			if (recommendationFromUi.getBy().length() > 0) {
				return true;
			}
			if (recommendationFromUi.getCategory().length() > 0) {
				return true;
			}
			if (recommendationFromUi.getNotes().length() > 0) {
				return true;
			}
			if (recommendationFromUi.getUri().length() > 0) {
				return true;
			}
			if (recommendationFromUi.getSource().length() > 0) {
				return true;
			}
		} else {
			// edit
			if (isFieldDirty(recommendationFromUi.getName(), recommendationPassedToView.getName())) {
				return true;
			}
			if (isFieldDirty(recommendationFromUi.getBy(), recommendationPassedToView.getBy())) {
				return true;
			}
			if (isFieldDirty(recommendationFromUi.getCategory(), recommendationPassedToView.getCategory())) {
				return true;
			}
			if (isFieldDirty(recommendationFromUi.getNotes(), recommendationPassedToView.getNotes())) {
				return true;
			}
			if (isFieldDirty(recommendationFromUi.getUri(), recommendationPassedToView.getUri())) {
				return true;
			}
			if (isFieldDirty(recommendationFromUi.getSource(), recommendationPassedToView.getSource())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void deleteRecommendation(Recommendation recommendation) {
		if (recommendation != null) {
			repository.delete(String.valueOf(recommendation.getId()));
		}
	}

	@Override
	public void saveRecommendation(Recommendation recommendation) {
		Recommendation ui = view.getRecommendationFromUi();
		if (recommendation == null || recommendation.getId() == -1) {
			// create
			ContentValues contentValues = new Recommendation.Builder()
					// UI fields
					.name(ui.getName())
					.by(ui.getBy())
					.category(ui.getCategory())
					.notes(ui.getNotes())
					.uri(ui.getUri())
					// fields not in the UI
					.updated()
					// set the type of the new recommendation from the view we are on
					.type(view.getRecommendationType())
					.source(ui.getSource())
					.build();
			repository.create(contentValues);
			// really we only need this event if we are adding via a share
			eventBus.send(new RecommendationAddedEvent(view.getRecommendationType()));
		} else {
			// update
			IdAndValues idAndValues = new IdAndValues();
			// we will only have an id if we are editing an existing recommendation
			// dont forget that the id will not be in the ui object
			idAndValues.id = recommendation.getId();
			idAndValues.contentValues = new Recommendation.Builder()
					// UI fields
					.name(ui.getName())
					.by(ui.getBy())
					.category(ui.getCategory())
					.notes(ui.getNotes())
					.uri(ui.getUri())
					// fields not in the UI
					.updated()
					// when we edit we do not want to move things between the lists - just keep it in the same list as it was
					.type(recommendation.getType())
					.source(ui.getSource())
					.build();
			repository.update(idAndValues);
		}
	}

	@Override
	public void moveRecommendation(Recommendation recommendation) {
		Recommendation ui = view.getRecommendationFromUi();
		// remember to get the non ui elements from the recommendation param
		ui.setType(recommendation.getType().toggleType());
		ui.setUpdated(recommendation.getUpdated());

		IdAndValues idAndValues = new IdAndValues();
		idAndValues.id = recommendation.getId();
		idAndValues.contentValues = new Recommendation.Builder()
				.fromRecommendation(ui)
				.build();
		repository.update(idAndValues);
		eventBus.send(new RecommendationMovedEvent(recommendation.getType(), ui.getType()));
	}

	@Override
	public void browseToUri() {
		Recommendation recommendationFromUi = view.getRecommendationFromUi();
		try {
			URL url = new URL(recommendationFromUi.getUri());
			logger.getCurrentApplicationLogger().debug("browseToUri uri {}", url.toString());

			Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(url.toString()));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		}
		catch (MalformedURLException e) {
			// we cannot understand it assume it is text
			logger.getCurrentApplicationLogger().warn("cannot browse to url {}", recommendationFromUi.getUri(),  e);
			crashReporter.logNonFatalException(e);
			view.showMessage(R.string.error_browse_url);
		}
	}
}
