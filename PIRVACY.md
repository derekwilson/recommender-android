# Recommender Privacy Policy #

Recommender is an application written by Derek Wilson.

This policy describes the data that is collected by Derek Wilson.

## What information is collected? ##

None. No personally identifiable information is collected by the app. 

## How is this information processed? ##

No information is collected.

## How do I know that this is true? ##

Recommender is open source and [this repository](https://bitbucket.org/derekwilson/recommender-android/src/master/) contains the source code for the application. You can check the code.



