package net.derekwilson.recommender.json;

import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.model.Recommendation;

import java.io.InputStream;
import java.util.List;

public interface IImporter {
	String getImportUri();
	List<Recommendation> importRecommendations() throws ExternalStorageException;
	List<Recommendation> importRecommendations(String importFilename) throws ExternalStorageException;
	List<Recommendation> importRecommendations(InputStream stream);
	List<Recommendation> importRecommendationsFromString(String content);
}
