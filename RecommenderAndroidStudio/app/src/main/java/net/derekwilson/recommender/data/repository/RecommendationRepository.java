package net.derekwilson.recommender.data.repository;

import static net.derekwilson.recommender.data.schema.RecommendationTable.COLUMN_NAME;
import static net.derekwilson.recommender.data.schema.RecommendationTable.COLUMN_UPDATED;

import android.content.ContentValues;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.schema.RecommendationTable;
import net.derekwilson.recommender.event.DbSubscriptionRestartRequired;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

public class RecommendationRepository implements IRecommendationRepository {

	protected ILoggerFactory logger;
	protected BriteDatabase db;
	protected IEventBus eventBus;
	protected ICrashReporter crashReporter;

	private final PublishSubject<ContentValues> createNeeded = PublishSubject.create();
	private Subscription createSubscription;
	private final PublishSubject<IdAndValues> updatedNeeded = PublishSubject.create();
	private Subscription updateSubscription;
	private final PublishSubject<String> deleteNeeded = PublishSubject.create();
	private Subscription deleteSubscription;

	private Action1<Throwable> queryErrorHandler;

	@Inject
	public RecommendationRepository(final ILoggerFactory logger, BriteDatabase db, IEventBus eventBus, ICrashReporter crashReporter) {
		this.logger = logger;
		this.db = db;
		this.eventBus = eventBus;
		this.crashReporter = crashReporter;

		this.logger.getCurrentApplicationLogger().debug("RecommendationRepository - creating subscriptions");

		createSubscription = createNeeded
				.subscribeOn(AndroidSchedulers.mainThread())
				.observeOn(Schedulers.io())
				.subscribe(new Action1<ContentValues>() {
					           @Override
					           public void call(ContentValues contentValues) {
						           _createRecommendation(contentValues);
					           }
				           }, new Action1<Throwable>() {
					           @Override
					           public void call(Throwable throwable) {
						           logger.getCurrentApplicationLogger().error("Error creating a recommendation", throwable);
								   crashReporter.logNonFatalException(throwable);
					           }
				           }, new Action0() {
					           @Override
					           public void call() {
						           logger.getCurrentApplicationLogger().debug("Create complete");
					           }
				           }
				);

		updateSubscription = updatedNeeded
				.subscribeOn(AndroidSchedulers.mainThread())
				.observeOn(Schedulers.io())
				.subscribe(new Action1<IdAndValues>() {
							   @Override
						 	   public void call(IdAndValues idAndValues) {
							       _updateRecommendation(idAndValues);
							   }
				           }, new Action1<Throwable>() {
					           @Override
					           public void call(Throwable throwable) {
						           logger.getCurrentApplicationLogger().error("Error updating a recommendation", throwable);
								   crashReporter.logNonFatalException(throwable);
					           }
				           }, new Action0() {
					           @Override
					           public void call() {
						           logger.getCurrentApplicationLogger().debug("Update complete");
					           }
				           }
				);

		deleteSubscription = deleteNeeded
				.subscribeOn(AndroidSchedulers.mainThread())
				.observeOn(Schedulers.io())
				.subscribe(new Action1<String>() {
					           @Override
					           public void call(String id) {
						           _deleteRecommendation(id);
					           }
				           }, new Action1<Throwable>() {
					           @Override
					           public void call(Throwable throwable) {
						           logger.getCurrentApplicationLogger().error("Error deleting a recommendation", throwable);
								   crashReporter.logNonFatalException(throwable);
					           }
				           }, new Action0() {
					           @Override
					           public void call() {
						           logger.getCurrentApplicationLogger().debug("Delete complete");
					           }
				           }
				);

		queryErrorHandler = new Action1<Throwable>() {
			@Override
			public void call(Throwable throwable) {
				logger.getCurrentApplicationLogger().warn("Error on query", throwable);
				crashReporter.logNonFatalException(throwable);
			}
		};
	}

	@Override
	public void close() {
		this.logger.getCurrentApplicationLogger().debug("RecommendationRepository - unsubscribe");
		closeSubscription(createSubscription);
		closeSubscription(updateSubscription);
		closeSubscription(deleteSubscription);
	}

	private void closeSubscription(Subscription subscription) {
		if (subscription != null) {
			subscription.unsubscribe();
		}
	}

	/**
	 * Run on the background thread no UI allowed in here
	 */
	private void _createRecommendation(ContentValues contentValues) {
		logger.getCurrentApplicationLogger().debug("RecommendationRepository Create");
		db.insert(RecommendationTable.TABLE_RECOMMENDATION, contentValues);
	}

	/**
	 * Run on the background thread no UI allowed in here
	 */
	private void _updateRecommendation(IdAndValues idAndValues) {
		logger.getCurrentApplicationLogger().debug("RecommendationRepository Update: {}", idAndValues.id);
		db.update(RecommendationTable.TABLE_RECOMMENDATION,
				idAndValues.contentValues,
				RecommendationTable.COLUMN_ID + " = ?",
				String.valueOf(idAndValues.id)
		);
	}

	/**
	 * Run on the background thread no UI allowed in here
	 */
	private void _deleteRecommendation(String id) {
		logger.getCurrentApplicationLogger().debug("RecommendationRepository Delete: {}", id);
		if (id == null) {
			// delete everything
			db.delete(RecommendationTable.TABLE_RECOMMENDATION, null);
		}
		else {
			// delete single
			db.delete(RecommendationTable.TABLE_RECOMMENDATION,
					RecommendationTable.COLUMN_ID + " = ?",
					id
			);
		}
	}

	@Override
	public void create(ContentValues newItem) {
		createNeeded.onNext(newItem);
	}

	@Override
	public void update(IdAndValues item) {
		updatedNeeded.onNext(item);
	}

	@Override
	public void delete(String id) {
		deleteNeeded.onNext(id);
	}

	@Override
	public void deleteAll() {
		deleteNeeded.onNext(null);
	}

	private <T extends Object> Subscription subscribeToQuery(
			final String name,
			Observable<SqlBrite.Query> query,
			Func1<? super SqlBrite.Query, ?> mapper,
			Action1<T> responseHandler
	) {
		return query
			.doOnError(queryErrorHandler)
			.map(mapper)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())      // TODO move to a handler thread
			.subscribe((Action1<Object>) responseHandler, new Action1<Throwable>() {
				@Override
				public void call(Throwable throwable) {
					subscriptionErrorHandler(name, throwable);
				}
			});
	}

	private void subscriptionErrorHandler(String name, Throwable throwable) {
		if (throwable instanceof MissingBackpressureException) {
			// the subscribers are overwhelmed
			// we really need to stop updating so quickly
			logger.getCurrentApplicationLogger().debug("STOP DOING SO MANY DB UPDATES QUICKLY - consider wrapping multiple updates in a transaction");
			crashReporter.logNonFatalException(throwable);
			eventBus.send(new DbSubscriptionRestartRequired(name));
			return;
		}
		logger.getCurrentApplicationLogger().warn("Error on subscription {} ", name, throwable);
		crashReporter.logNonFatalException(throwable);
	}

	@Override
	public Subscription getById(String id, Action1<List<Recommendation>> responseHandler) {
		Observable<SqlBrite.Query> query = db.createQuery(
				RecommendationTable.TABLE_RECOMMENDATION,
				RecommendationTable.LIST_RECOMMENDATIONS_BY_ID_QUERY,
				id);
		return subscribeToQuery("getById", query, Recommendation.MAP, responseHandler);
	}

	@Override
	public Subscription getAll(Action1<List<Recommendation>> responseHandler) {
		Observable<SqlBrite.Query> query = db.createQuery(
				RecommendationTable.TABLE_RECOMMENDATION,
				RecommendationTable.LIST_RECOMMENDATIONS
		);
		return subscribeToQuery("getAll", query, Recommendation.MAP, responseHandler);
	}

	private static final String SORT_BY_NAME_PARAMS = COLUMN_NAME + " ASC";
	private static final String SORT_BY_UPDATED_PARAMS = COLUMN_UPDATED + " DESC, " + COLUMN_NAME + " ASC";

	private String getSortByParameters(SortBy sorby) {
		switch (sorby) {
			case UPDATED:
				return SORT_BY_UPDATED_PARAMS;
			case NAME:
			default:
				return SORT_BY_NAME_PARAMS;
		}
	}

	@Override
	public Subscription getByType(RecommendationType type, SortBy sortby, Action1<List<Recommendation>> responseHandler) {
		String querySql = RecommendationTable.LIST_RECOMMENDATIONS_BY_TYPE_QUERY
				.replace(RecommendationTable.SORT_INSERT_POINT, getSortByParameters(sortby))
		;
		Observable<SqlBrite.Query> query = db.createQuery(
				RecommendationTable.TABLE_RECOMMENDATION,
				querySql,
				type.getValueAsString()
		);
		return subscribeToQuery("getByType", query, Recommendation.MAP, responseHandler);
	}

	@Override
	public Subscription getByTypeWithFilter(RecommendationType type, SortBy sortby, IFilterCollection filters, Action1<List<Recommendation>> responseHandler) {
		String querySql = null;
		Observable<SqlBrite.Query> query = null;
		switch (filters.getFilterType()) {
			case None:
				return getByType(type, sortby, responseHandler);
			case Category:
				querySql = RecommendationTable.LIST_RECOMMENDATIONS_BY_TYPE_WITH_CATEGORY_FILTER_QUERY
						.replace(RecommendationTable.INSERT_POINT, filters.getSelectedAsParameterPlaceholder())
						.replace(RecommendationTable.SORT_INSERT_POINT, getSortByParameters(sortby))
				;
				query = db.createQuery(
						RecommendationTable.TABLE_RECOMMENDATION,
						querySql,
						filters.getSelectedAsArray(type)
				);
				break;
			case Search:
				querySql = RecommendationTable.LIST_RECOMMENDATIONS_BY_TYPE_WITH_SEARCH_QUERY
						.replace(RecommendationTable.SORT_INSERT_POINT, getSortByParameters(sortby))
				;
				query = db.createQuery(
						RecommendationTable.TABLE_RECOMMENDATION,
						querySql,
						type.getValueAsString(),
						filters.getSearchFilter()
				);
				break;
			case CategoryAndSearch:
				querySql = RecommendationTable.LIST_RECOMMENDATIONS_BY_TYPE_WITH_CATEGORY_FILTER_AND_SEARCH_QUERY
						.replace(RecommendationTable.INSERT_POINT, filters.getSelectedAsParameterPlaceholder())
						.replace(RecommendationTable.SORT_INSERT_POINT, getSortByParameters(sortby))
				;
				query = db.createQuery(
						RecommendationTable.TABLE_RECOMMENDATION,
						querySql,
						filters.getSelectedAsArray(type)
				);
				break;
		}
		if (query != null) {
			return subscribeToQuery("getByTypeWithFilter", query, Recommendation.MAP, responseHandler);
		}
		return null;
	}

	@Override
	public Subscription getCategories(Action1<List<String>> responseHandler) {
		Observable<SqlBrite.Query> query = db.createQuery(
				RecommendationTable.TABLE_RECOMMENDATION,
				RecommendationTable.LIST_RECOMMENDATION_CATEGORIES
		);
		return subscribeToQuery("getCategories", query, Recommendation.MAPCATEGORIES, responseHandler);
	}

	@Override
	public Subscription getSources(Action1<List<String>> responseHandler) {
		Observable<SqlBrite.Query> query = db.createQuery(
				RecommendationTable.TABLE_RECOMMENDATION,
				RecommendationTable.LIST_RECOMMENDATION_SOURCES
		);
		return subscribeToQuery("getSources", query, Recommendation.MAPSOURCES, responseHandler);
	}
}
