package net.derekwilson.recommender.fragment.recommendation;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.FragmentManager;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.R;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.scopes.DaggerIEditRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.EditRecommendationModule;
import net.derekwilson.recommender.ioc.scopes.IEditRecommendationComponent;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.functions.Action1;

public class SourceBottomSheetFragment extends BaseBottomSheetFragment {

	private IEditRecommendationComponent component;

	@Inject
	protected IRecommendationRepository repository;

	@Inject
	protected ILoggerFactory logger;

	@Inject
	protected ITextUtils textUtils;

	private SourceAdapter sourceAdapter;

	private Subscription sourceSubscription = null;

	private RecommendationEditFragment editFragment;

	private IEditRecommendationComponent getComponent() {
		if (component == null) {
			component = DaggerIEditRecommendationComponent.builder()
					.iApplicationComponent(((AndroidApplication) getActivity().getApplication()).getApplicationComponent())
					.baseActivityModule(new BaseActivityModule(getActivity()))
					.editRecommendationModule(new EditRecommendationModule())
					.build();
		}
		return component;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getComponent().inject(this);
	}

	@Override
	protected int getLayout() {
		return R.layout.fragment_source_bottomsheet;
	}

	protected ListView sourceList;
	@Override
	public void setupDialog(Dialog dialog, int style) {
		super.setupDialog(dialog, style);
		sourceList = dialog.findViewById(R.id.source_list);

		sourceAdapter = new SourceAdapter(this.getActivity(), textUtils);
		sourceList.setAdapter(sourceAdapter);
		sourceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				if (editFragment != null) {
					editFragment.setSource(sourceAdapter.getItem(position));
				} else {
					logger.getCurrentApplicationLogger().warn("SourceBottomSheetFragment: cannot fire click event as the edit fragment is null");
				}
				dismiss();
			}
		});
	}

	@Override public void onResume() {
		super.onResume();
		sourceSubscription = repository.getSources(new Action1<List<String>>() {
			@Override
			public void call(List<String> categories) {
				logger.getCurrentApplicationLogger().debug("SourceBottomSheetFragment: bottomsheet getCategories updated - {}", categories.size());
				sourceAdapter.call(categories);
			}
		});
	}

	@Override public void onPause() {
		super.onPause();
		// stop any subscriptions
		if (sourceSubscription != null) {
			sourceSubscription.unsubscribe();
			sourceSubscription = null;
		}
	}

	public void showBottomSheet(FragmentManager supportFragmentManager, RecommendationEditFragment editFragment) {
		this.editFragment =  editFragment;
		show(supportFragmentManager, getTag());
	}
}
