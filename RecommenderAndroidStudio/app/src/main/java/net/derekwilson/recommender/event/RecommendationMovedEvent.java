package net.derekwilson.recommender.event;

import net.derekwilson.recommender.model.RecommendationType;

public class RecommendationMovedEvent {
	private RecommendationType source;
	private RecommendationType destination;

	public RecommendationMovedEvent(RecommendationType source, RecommendationType destination) {
		this.source = source;
		this.destination = destination;
	}

	public RecommendationType getDestination() {
		return destination;
	}

	public RecommendationType getSource() {
		return source;
	}
}
