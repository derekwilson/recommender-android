package net.derekwilson.recommender.analytics;

import net.derekwilson.recommender.model.Recommendation;

import java.util.List;

public interface IEventLogger {
	void logShareRecommendation(Recommendation recommendation, String origin);

	void logShareRecommendations(List<Recommendation> recommendations, String origin);

	void logReceivedRecommendation(Recommendation recommendation, String origin);

	void logReceivedRecommendations(List<Recommendation> recommendations, String origin);

	void logErrorEvent();

	void logStartup(String fontScaling, String uiMode);
}
