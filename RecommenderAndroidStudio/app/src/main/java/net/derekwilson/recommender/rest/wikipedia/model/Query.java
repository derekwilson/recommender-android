package net.derekwilson.recommender.rest.wikipedia.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
		"normalized",
		"pages"
})
public class Query {

	@JsonProperty("normalized")
	private List<Normalized> normalized = new ArrayList<Normalized>();
	@JsonProperty("pages")
	private Pages pages;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 *
	 * @return
	 * The normalized
	 */
	@JsonProperty("normalized")
	public List<Normalized> getNormalized() {
		return normalized;
	}

	/**
	 *
	 * @param normalized
	 * The normalized
	 */
	@JsonProperty("normalized")
	public void setNormalized(List<Normalized> normalized) {
		this.normalized = normalized;
	}

	/**
	 *
	 * @return
	 * The pages
	 */
	@JsonProperty("pages")
	public Pages getPages() {
		return pages;
	}

	/**
	 *
	 * @param pages
	 * The pages
	 */
	@JsonProperty("pages")
	public void setPages(Pages pages) {
		this.pages = pages;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
