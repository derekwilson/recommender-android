package net.derekwilson.recommender.fragment.utility;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class OkCancelDialogFragment extends DialogFragment {
	private static String MESSAGE = "message_key";
	private static String OK = "ok_key";
	private static String CANCEL = "cancel_key";

	public interface Listener {
		public void ok(String tag);
		public void cancel(String tag);
	}

	private Listener callbackActivity;

	public static OkCancelDialogFragment newInstance(String message, String ok, String cancel) {
		OkCancelDialogFragment fragment = new OkCancelDialogFragment();
		Bundle args = new Bundle();
		args.putString(MESSAGE, message);
		args.putString(OK, ok);
		args.putString(CANCEL, cancel);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			callbackActivity = (Listener) getActivity();
		} catch (ClassCastException e) {
			throw new ClassCastException(getActivity().toString() + " must implement Listener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Bundle args = getArguments();
		final String tag = getTag();
		String message = args.getString(MESSAGE);
		String ok = args.getString(OK);
		String cancel = args.getString(CANCEL);

		MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getActivity());
		builder.setMessage(message);
		builder
				.setPositiveButton(ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								callbackActivity.ok(tag);
							}
						}
				)
				.setNegativeButton(cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								callbackActivity.cancel(tag);
							}
						}
				);
		return builder.create();
	}
}

