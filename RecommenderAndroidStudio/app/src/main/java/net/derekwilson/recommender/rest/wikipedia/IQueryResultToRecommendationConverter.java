package net.derekwilson.recommender.rest.wikipedia;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.rest.wikipedia.model.QueryResult;

import java.net.URL;

public interface IQueryResultToRecommendationConverter {
	Recommendation convert(URL url, QueryResult wikipediaResult);
}
