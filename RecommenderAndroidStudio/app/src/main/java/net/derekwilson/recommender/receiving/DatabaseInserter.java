package net.derekwilson.recommender.receiving;

import android.database.sqlite.SQLiteDatabase;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.data.schema.RecommendationTable;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class DatabaseInserter implements IInserter {

	private ILoggerFactory logger;
	private BriteDatabase db;

	@Inject
	public DatabaseInserter(ILoggerFactory logger, BriteDatabase db) {
		this.logger = logger;
		this.db = db;
	}

	@Override
	public int insertRecommendations(List<Recommendation> recommendations) {
		int numberInserted = 0;
		if (recommendations == null || recommendations.size() < 1) {
			return numberInserted;
		}
		BriteDatabase.Transaction transaction = db.newTransaction();
		try {
			for (Recommendation thisRecommendation : recommendations) {
				// TODO - process errors
				// TODO - consider running async
				db.insert(RecommendationTable.TABLE_RECOMMENDATION,
						new Recommendation.Builder()
								.fromRecommendation(thisRecommendation)
								.build(),
						SQLiteDatabase.CONFLICT_IGNORE
				);
				numberInserted++;
			}
			transaction.markSuccessful();
		} finally {
			transaction.end();
		}
		return numberInserted;
	}

	@Override
	public int insertRecommendationsIntoType(List<Recommendation> recommendations, RecommendationType type) {
		for (Recommendation thisRecommendation : recommendations) {
			// careful this will alter the listed passed to us
			thisRecommendation.setType(type);
		};
		return insertRecommendations(recommendations);
	}
}
