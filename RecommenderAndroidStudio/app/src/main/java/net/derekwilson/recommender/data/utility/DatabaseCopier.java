package net.derekwilson.recommender.data.utility;

import android.content.ComponentName;
import android.content.Context;
import android.os.Environment;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.RecommenderDatabaseHelper;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.logging.ILoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import javax.inject.Inject;

public class DatabaseCopier implements IDatabaseCopier {

	ILoggerFactory logger;
	Context applicationContext;
	ICrashReporter crashReporter;

	@Inject
	public DatabaseCopier(ILoggerFactory logger, Context applicationContext, ICrashReporter crashReporter) {
		this.logger = logger;
		this.applicationContext = applicationContext;
		this.crashReporter = crashReporter;
	}

	private String getDestinationFolder() {
		// we are limited where we can write unless we ask for extra permissions in android 11+
		File directory = applicationContext.getExternalFilesDir(null);
		// throw if we cannot get the path
		return directory.getAbsolutePath() + "/db/";
	}
	@Override
	public String getDestinationPathname() {
		return getDestinationFolder() + RecommenderDatabaseHelper.DATABASE_FILE_NAME;
	}

	@Override
	public boolean copyDatabaseFile(String fullPathToDestination) {
		try {
			File sDir = new File(getDestinationFolder());
			if (!sDir.exists()) {
				sDir.mkdirs();
			}

			String currentDBPath = "/data/data/" + applicationContext.getPackageName() + "/databases/" + RecommenderDatabaseHelper.DATABASE_FILE_NAME;
			String backupDBPath = fullPathToDestination;
			File currentDB = new File(currentDBPath);
			File backupDB = new File(backupDBPath);

			if (currentDB.exists()) {
				FileChannel src = new FileInputStream(currentDB).getChannel();
				FileChannel dst = new FileOutputStream(backupDB).getChannel();
				long size = src.size();
				dst.transferFrom(src, 0, size);
				src.close();
				dst.close();
				logger.getCurrentApplicationLogger().debug("copyDatabaseFile: copier {} bytes to {}", size, fullPathToDestination);
			} else {
				logger.getCurrentApplicationLogger().debug("copyDatabaseFile: Cannot find database in path {}", currentDBPath);
				return false;
			}
		} catch (Exception e) {
			logger.getCurrentApplicationLogger().error("Error copying database: ", e);
			crashReporter.logNonFatalException(e);
			return false;
		}
		return true;
	}
}
