package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.presenter.importrecommendationactivity.IImportRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.importrecommendationactivity.ImportRecommendationActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ImportRecommendationModule {
	/**
	 * this is the presenter used by the import activity
	 */
	@Provides
	@ActivityScope
	IImportRecommendationActivityPresenter provideActivityPresenter(ImportRecommendationActivityPresenter presenter) {
		return presenter;
	}
}
