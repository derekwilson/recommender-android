package net.derekwilson.recommender.fragment.recommendation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.functions.Action1;

final public class SourceAdapter extends BaseAdapter implements Action1<List<String>> {
	private final LayoutInflater inflater;

	private ITextUtils textUtils;
	private List<String> items = Collections.emptyList();

	public SourceAdapter(Context context, ITextUtils textUtils) {
		this.textUtils = textUtils;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public void call(List<String> strings) {
		if (strings == null || strings.size() < 1) {
			return;
		}
		this.items = new ArrayList<String>(strings.size());
		for (String thisString : strings) {
			if (!textUtils.isEmpty(thisString)) {
				items.add(thisString);
			}
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() { return items == null ? 0 : items.size(); }

	@Override
	public String getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).hashCode();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getView(int position, View rowView, ViewGroup parent) {
		if (rowView == null) {
			rowView = inflater.inflate(R.layout.list_item_source, parent, false);
		}
		ViewHolder holder = new ViewHolder(rowView);
		String thisSource = items.get(position);

		holder.label.setText(thisSource);

		return rowView;
	}

	static class ViewHolder {
		TextView label;

		public ViewHolder(View view) {
			label = view.findViewById(R.id.source_row_label);
		}
	}
}

