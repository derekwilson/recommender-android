package net.derekwilson.recommender.tests.presenter.listrecommendations;

import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import rx.functions.Action1;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SetFilterTests extends PresenterTestSetup {

	private IFilterCollection collection;

	@Before
	public void setUp() {
		super.setUp();
		setupPresenter(true, false);
		collection = mock(IFilterCollection.class);
	}

	@Test
	public void there_is_no_filter_set() {
		// arrange
		when(collection.getFilterType()).thenReturn(FilterCollection.FilterType.None);

		// act
		presenter.setFilter(collection);

		// assert
		// the empty text is set correctly
		verify(mockView, times(1)).setNonFilteredEmptyText();
		verify(mockView, never()).setFilteredEmptyText();
		// we call the repository correctly
		verify(mockRepository, times(1)).getByType((RecommendationType) any(), any(), (Action1<List<Recommendation>>) any());
	}

	@Test
	public void there_is_a_category_filter_set() {
		// arrange
		when(collection.getFilterType()).thenReturn(FilterCollection.FilterType.Category);

		// act
		presenter.setFilter(collection);

		// assert
		// the empty text is set correctly
		verify(mockView, times(1)).setFilteredEmptyText();
		verify(mockView, never()).setNonFilteredEmptyText();
		// we call the repository correctly
		verify(mockRepository, times(1)).getByTypeWithFilter((RecommendationType) any(), any(), (IFilterCollection) any(), (Action1<List<Recommendation>>) any());
	}

	@Test
	public void there_is_a_search_filter_set() {
		// arrange
		when(collection.getFilterType()).thenReturn(FilterCollection.FilterType.Search);

		// act
		presenter.setFilter(collection);

		// assert
		// the empty text is set correctly
		verify(mockView, times(1)).setFilteredEmptyText();
		verify(mockView, never()).setNonFilteredEmptyText();
		// we call the repository correctly
		verify(mockRepository, times(1)).getByTypeWithFilter((RecommendationType) any(), any(), (IFilterCollection) any(), (Action1<List<Recommendation>>) any());
	}

	@Test
	public void there_is_a_category_and_search_filter_set() {
		// arrange
		when(collection.getFilterType()).thenReturn(FilterCollection.FilterType.CategoryAndSearch);

		// act
		presenter.setFilter(collection);

		// assert
		// the empty text is set correctly
		verify(mockView, times(1)).setFilteredEmptyText();
		verify(mockView, never()).setNonFilteredEmptyText();
		// we call the repository correctly
		verify(mockRepository, times(1)).getByTypeWithFilter((RecommendationType) any(), any(), (IFilterCollection) any(), (Action1<List<Recommendation>>) any());
	}
}
