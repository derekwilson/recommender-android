package net.derekwilson.recommender.data.repository;

import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import java.util.List;

import rx.Subscription;
import rx.functions.Action1;

public interface IRecommendationRepository extends IRepository<Recommendation> {
	public enum SortBy {
		NAME,
		UPDATED
	}

	Subscription getByType(RecommendationType type, SortBy sortby, Action1<List<Recommendation>> responseHandler);
	Subscription getByTypeWithFilter(RecommendationType type, SortBy sortby, IFilterCollection filters, Action1<List<Recommendation>> responseHandler);
	Subscription getCategories(Action1<List<String>> responseHandler);
	Subscription getSources(Action1<List<String>> responseHandler);
}
