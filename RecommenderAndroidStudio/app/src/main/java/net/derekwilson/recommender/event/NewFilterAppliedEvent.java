package net.derekwilson.recommender.event;

import net.derekwilson.recommender.model.RecommendationType;

public class NewFilterAppliedEvent {
	private RecommendationType recommendationType;

	public NewFilterAppliedEvent(RecommendationType recommendationType) {
		this.recommendationType = recommendationType;
	}

	public RecommendationType getRecommendationType() {
		return recommendationType;
	}
}
