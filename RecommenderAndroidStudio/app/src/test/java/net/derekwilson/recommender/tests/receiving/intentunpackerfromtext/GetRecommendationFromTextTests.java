package net.derekwilson.recommender.tests.receiving.intentunpackerfromtext;

import android.content.Intent;

import net.derekwilson.recommender.model.Recommendation;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GetRecommendationFromTextTests extends BaseUnpackerTests {
	@Test
	public void unpacks_general_text() {
		// arrange
		Intent intent = setupIntent(null, "hello tests", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("hello tests"));
	}

	@Test
	public void unpacks_kindle_progress_text_1() {
		// arrange
		Intent intent = setupIntent(null, "45% through The Sisters Brothers by Patrick deWitt on Kindle for Android! http://www.amazon.co.uk/kindleforandroid/", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("The Sisters Brothers"));
		assertThat(recommendation.getBy(), is("Patrick deWitt"));
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void unpacks_kindle_progress_text_2() {
		// arrange
		Intent intent = setupIntent(null, "I'm 37% through this book and think you might like it - \"The Stone Man - A Science Fiction Thriller\" by Luke Smitherd.\n\nStart reading it for free: http://amzn.to/1RAipL5", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("The Stone Man - A Science Fiction Thriller"));
		assertThat(recommendation.getBy(), is("Luke Smitherd."));
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void unpacks_kindle_progress_text_3() {
		// arrange
		Intent intent = setupIntent(null, "Hi - I'm 81% through  \"Agency: Sequel to The Peripheral, now a major new TV series with Amazon Prime\" by William Gibson and I think you might find it interesting. Start reading it for free: https://amzn.eu/7sH0vpL", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("Agency: Sequel to The Peripheral, now a major new TV series with Amazon Prime"));
		assertThat(recommendation.getBy(), is("William Gibson"));
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void unpacks_kindle_completed_text() {
		// arrange
		Intent intent = setupIntent(null, "I just finished The Sisters Brothers by Patrick deWitt on Kindle for Android! http://www.amazon.co.uk/kindleforandroid/", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("The Sisters Brothers"));
		assertThat(recommendation.getBy(), is("Patrick deWitt"));
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void unpacks_kindle_completed_text_2() {
		// arrange
		Intent intent = setupIntent(null, "Hi - I think you should check out this book, \"Agency: Sequel to The Peripheral, now a major new TV series with Amazon Prime\" by William Gibson.  Start reading it for free: https://amzn.eu/fL2IAC5", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("Agency: Sequel to The Peripheral, now a major new TV series with Amazon Prime"));
		assertThat(recommendation.getBy(), is("William Gibson."));
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void unpacks_kindle_text_bad_format1() {
		// arrange
		Intent intent = setupIntent(null, "45% into The Sisters Brothers by Patrick deWitt goto http://www.amazon.co.uk/kindleforandroid/", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("45% into The Sisters Brothers"));
		assertThat(recommendation.getBy(), is("Patrick deWitt goto http://www.amazon.co.uk/kindleforandroid/"));
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void unpacks_kindle_text_bad_format2() {
		// check if the minimum text we are looking for is there we do not crash
		// arrange
		Intent intent = setupIntent(null, " byhttp://www.amazon.co.uk/kindleforandroid/", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is(""));
		assertThat(recommendation.getCategory(), is("Book"));
	}

	@Test
	public void unpacks_beyondpod_text() {
		// check if the minimum text we are looking for is there we do not crash
		// arrange
		Intent intent = setupIntent(null, "Mark Kermode and Simon Mayo's Film Reviews - http://downloads.bbc.co.uk/podcasts/fivelive/kermode/rss.xml", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("Mark Kermode and Simon Mayo's Film Reviews"));
		assertThat(recommendation.getUri(), is("http://downloads.bbc.co.uk/podcasts/fivelive/kermode/rss.xml"));
		assertThat(recommendation.getCategory(), is("Podcast"));
	}

	@Test
	public void unpacks_tunin_text() {
		// check if the minimum text we are looking for is there we do not crash
		// arrange
		Intent intent = setupIntent(null, "I'm listening to BBC Live Event 1 with TuneIn. #NowPlaying http://tun.in/seO9f", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("I'm listening to BBC Live Event 1 with TuneIn. #NowPlaying"));
		assertThat(recommendation.getUri(), is("http://tun.in/seO9f"));
		assertThat(recommendation.getCategory(), is("WebSite"));
	}

	@Test
	public void unpacks_iplayer_text() {
		// check if the minimum text we are looking for is there we do not crash
		// arrange
		Intent intent = setupIntent(null, "The Archers Omnibus - 13/09/2015 - @bbcradio4 http://www.bbc.co.uk/programmes/b069gvl3", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getName(), is("The Archers Omnibus 13/09/2015 @bbcradio4"));
		assertThat(recommendation.getUri(), is("http://www.bbc.co.uk/programmes/b069gvl3"));
		assertThat(recommendation.getCategory(), is("WebSite"));
	}

	@Test
	public void unpacks_play_text() {
		// check if the minimum text we are looking for is there we do not crash
		// arrange
		Intent intent = setupIntent(null, "https://play.google.com/store/apps/details?id=com.microsoft.office.word", null, null);

		// act
		Recommendation recommendation = unpacker.getRecommendationFromText(intent);

		// assert
		assertThat(recommendation.getUri(), is("https://play.google.com/store/apps/details?id=com.microsoft.office.word"));
		assertThat(recommendation.getCategory(), is("App"));
	}

}
