package net.derekwilson.recommender.sharing;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.core.content.FileProvider;

import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.json.IExporter;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class EmailSharer extends BaseSharer implements IEmailIntentSharer {
	private static final String FILE_PREFIX = "recommendations_share";
	private static final String FILE_SUFFIX = ".recommendation";

	private final ILoggerFactory logger;
	private final IExporter exporter;
	private final Context context;

	@Inject
	public EmailSharer(ILoggerFactory logger, IExporter exporter, Context context) {
		this.logger = logger;
		this.exporter = exporter;
		this.context = context;
	}

	private Intent getSharingIntent(String shareText, ArrayList<Uri> attachmentUris) {
		//Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		Intent sharingIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
		sharingIntent.setType("vnd.android.cursor.dir/email");
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "I Recommend");
		sharingIntent.putExtra(Intent.EXTRA_TEXT, shareText);
		sharingIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, attachmentUris);
		return sharingIntent;
	}

	private ArrayList<Uri> getRecommendationsAsJsonFile(List<Recommendation> recommendations) throws ExternalStorageException {
		ArrayList<Uri> attachmentUris = new ArrayList<Uri>();
		String filename = exporter.getBaseUri() + FILE_PREFIX + FILE_SUFFIX;
		logger.getCurrentApplicationLogger().debug("EmailSharer: attaching file {}", filename);
		Uri fileURI = exporter.exportRecommendations(recommendations,filename);
		attachmentUris.add(fileURI);
		return attachmentUris;
	}

	@Override
	public IntentShareResult getIntent(Recommendation recommendation) throws ExternalStorageException {
		List<Recommendation> recommendations = new ArrayList<Recommendation>();
		recommendations.add(recommendation);
		return getIntent(recommendations);
	}

	@Override
	public IntentShareResult getIntent(List<Recommendation> recommendations) throws ExternalStorageException {
		String shareText = getShareText(context, recommendations);
		ArrayList<Uri> attachmentUris = getRecommendationsAsJsonFile(recommendations);

		IntentShareResult result = new IntentShareResult();
		result.NumberOfRecommendations = recommendations.size();
		result.FileUris = new Uri[attachmentUris.size()];
		attachmentUris.toArray(result.FileUris);
		result.SendIntent = getSharingIntent(shareText, attachmentUris);
		result.SendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		return result;
	}

	@Override
	public IntentShareResult getIntent(String filename) {
		Uri uri = getAttachmentUri(filename);

		ArrayList<Uri> attachmentUris = new ArrayList<Uri>();
		attachmentUris.add(uri);

		IntentShareResult result = new IntentShareResult();
		result.NumberOfRecommendations = -1;
		result.FileUris = new Uri[attachmentUris.size()];
		attachmentUris.toArray(result.FileUris);
		result.SendIntent = getSharingIntent("Backup of all recommendations", attachmentUris);
		result.SendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		return result;
	}

	protected Uri getAttachmentUri(String filename) {
		logger.getCurrentApplicationLogger().debug("EmailSharer: getAttachmentUri file {}", filename);
		File shareFile = new File(filename);
		Uri shareableUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", shareFile);
		logger.getCurrentApplicationLogger().debug("EmailSharer: getAttachmentUri uri {}", shareableUri);
		return shareableUri;
	}

}
