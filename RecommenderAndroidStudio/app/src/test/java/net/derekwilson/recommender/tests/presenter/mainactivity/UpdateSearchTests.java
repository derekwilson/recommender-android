package net.derekwilson.recommender.tests.presenter.mainactivity;

import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.IFilterCollection;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class UpdateSearchTests extends PresenterTestsSetup {
	String testSearchString = "TEST";

	@Before
	public void setUp() {
		setupPresenter(true, true);
	}

	@Test
	public void we_can_set_a_search_filter() {
		// arrange
		presenter.onCreate();

		// act
		presenter.updateSearchFilter(testSearchString);

		// assert
		ArgumentCaptor<IFilterCollection> argument = ArgumentCaptor.forClass(IFilterCollection.class);
		verify(mockView, times(1)).applyFilterToAllTabs(argument.capture());
		assertThat("incorrect search filter", argument.getValue().getSearchFilter(), is(testSearchString));
		assertThat("incorrect search type", argument.getValue().getFilterType(), is(FilterCollection.FilterType.Search));
	}

	@Test
	public void we_can_reset_a_search_filter() {
		// arrange
		presenter.onCreate();

		// act
		presenter.updateSearchFilter(null);

		// assert
		ArgumentCaptor<IFilterCollection> argument = ArgumentCaptor.forClass(IFilterCollection.class);
		verify(mockView, times(1)).applyFilterToAllTabs(argument.capture());
		assertNull("incorrect search filter", argument.getValue().getSearchFilter());
		assertThat("incorrect search type", argument.getValue().getFilterType(), is(FilterCollection.FilterType.None));
	}
}
