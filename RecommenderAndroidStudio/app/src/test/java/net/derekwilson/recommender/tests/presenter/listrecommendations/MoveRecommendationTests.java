package net.derekwilson.recommender.tests.presenter.listrecommendations;

import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.RecommendationMovedEvent;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MoveRecommendationTests extends PresenterTestSetup {
	private List<Recommendation> recommendations = new  ArrayList<>(5);

	@Before
	public void setUp() {
		super.setUp();
		setupPresenter(true, false);
		when(mockView.copySelectedRecommendations()).thenReturn(recommendations);
	}

	@Test
	public void event_is_fired_when_recommendation_is_moved() {
		// arrange
		Recommendation recommendation = setupRecommendation("name", "by", "category", "notes", "uri","source");
		recommendation.setUpdated(Calendar.getInstance());
		recommendations.add(recommendation);

		// act
		presenter.moveRecommendations();

		// assert
		ArgumentCaptor<RecommendationMovedEvent> eventCaptor = ArgumentCaptor.forClass(RecommendationMovedEvent.class);
		verify(mockEventBus, times(1)).send(eventCaptor.capture());

		List<RecommendationMovedEvent> capturedEvents = eventCaptor.getAllValues();
		assertThat("there should only be one event", capturedEvents.size(), is(1));
		assertThat("it should be a try_this", capturedEvents.get(0).getDestination(), is(RecommendationType.TRY_THIS));
	}
}
