package net.derekwilson.recommender.json;

import android.net.Uri;

import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.model.Recommendation;

import java.util.List;

public interface IExporter {
	String getBaseUri();
	String getExportUri();
	Uri exportRecommendations(List<Recommendation> recommendations, String filename) throws ExternalStorageException;
	String exportRecommendationsToString(List<Recommendation> recommendations);
}
