package net.derekwilson.recommender.model.recommendation;

import android.os.Bundle;

import net.derekwilson.recommender.BaseTests;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.MediumTest;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class ParcelableTests extends BaseTests {
	private static final String RECOMMENDATION = "recommendation_edit";

	// object under test
	private Recommendation recommendation;

	@Before
	public void setUp() {
		recommendation = setupRecommendation("test_name", "test_by", "test_category", "test_notes", "test_uri", "test_source");
		recommendation.setType(RecommendationType.TRY_THIS);
	}

	@Test
	public void can_parcel_into_a_bundle_name() {
		// arrange
		Bundle args = new Bundle();

		// act
		args.putParcelable(RECOMMENDATION, recommendation);
		Recommendation recommendationAfterParcelling = args.getParcelable(RECOMMENDATION);

		// assert
		assertThat(recommendationAfterParcelling.getName(), is(recommendation.getName()));
	}

	@Test
	public void can_parcel_into_a_bundle_by() {
		// arrange
		Bundle args = new Bundle();

		// act
		args.putParcelable(RECOMMENDATION, recommendation);
		Recommendation recommendationAfterParcelling = args.getParcelable(RECOMMENDATION);

		// assert
		assertThat(recommendationAfterParcelling.getBy(), is(recommendation.getBy()));
	}

	@Test
	public void can_parcel_into_a_bundle_category() {
		// arrange
		Bundle args = new Bundle();

		// act
		args.putParcelable(RECOMMENDATION, recommendation);
		Recommendation recommendationAfterParcelling = args.getParcelable(RECOMMENDATION);

		// assert
		assertThat(recommendationAfterParcelling.getCategory(), is(recommendation.getCategory()));
	}

	@Test
	public void can_parcel_into_a_bundle_notes() {
		// arrange
		Bundle args = new Bundle();

		// act
		args.putParcelable(RECOMMENDATION, recommendation);
		Recommendation recommendationAfterParcelling = args.getParcelable(RECOMMENDATION);

		// assert
		assertThat(recommendationAfterParcelling.getNotes(), is(recommendation.getNotes()));
	}

	@Test
	public void can_parcel_into_a_bundle_uri() {
		// arrange
		Bundle args = new Bundle();

		// act
		args.putParcelable(RECOMMENDATION, recommendation);
		Recommendation recommendationAfterParcelling = args.getParcelable(RECOMMENDATION);

		// assert
		assertThat(recommendationAfterParcelling.getUri(), is(recommendation.getUri()));
	}

	@Test
	public void can_parcel_into_a_bundle_type() {
		// arrange
		Bundle args = new Bundle();

		// act
		args.putParcelable(RECOMMENDATION, recommendation);
		Recommendation recommendationAfterParcelling = args.getParcelable(RECOMMENDATION);

		// assert
		assertThat(recommendationAfterParcelling.getType(), is(recommendation.getType()));
	}

	@Test
	public void can_parcel_into_a_bundle_source() {
		// arrange
		Bundle args = new Bundle();

		// act
		args.putParcelable(RECOMMENDATION, recommendation);
		Recommendation recommendationAfterParcelling = args.getParcelable(RECOMMENDATION);

		// assert
		assertThat(recommendationAfterParcelling.getSource(), is(recommendation.getSource()));
	}

}
