# Recommender Release Archive
This is the release archive for Recommender. If you cannot get an APK from a play store you can side-load it from here. It is also available from the [Amazon App Store](https://www.amazon.com/gp/product/B0BVPH5YMY) 

## APKs

| Version | Date        | MinSDK              | TargetSDK
| ------- | ----------- | ------------------- | ---------------
| 2.1.0   | 13 Jun 2024 | 21 (Android 5)      | 33 (Android 13)
| 2.0.0   | 11 Oct 2023 | 21 (Android 5)      | 33 (Android 13)
| 1.8.0   | 29 Apr 2023 | 21 (Android 5)      | 31 (Android 12)
| 1.7.0   | 12 Apr 2023 | 21 (Android 5)      | 31 (Android 12)
| 1.6.0   | 13 Feb 2023 | 21 (Android 5)      | 31 (Android 12)
| 1.5.3   | 26 Sep 2018 | 15 (Android 4.0.3)  | 23 (Android 6)
| 1.5.2   | 26 Sep 2018 | 15 (Android 4.0.3)  | 23 (Android 6)
| 1.5.0   | 10 Jan 2017 | 15 (Android 4.0.3)  | 23 (Android 6)
| 1.4.3   | 21 Dec 2016 | 15 (Android 4.0.3)  | 23 (Android 6)
| 1.4.2   | 19 Dec 2016 | 15 (Android 4.0.3)  | 23 (Android 6)
| 1.4.1   |  1 Sep 2016 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.4.0   | 28 Jul 2016 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.3.1   | 18 Jul 2016 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.3.0   | 23 Jun 2016 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.2.1   | 30 Apr 2016 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.2.0   | 15 Apr 2016 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.1.1   | 27 Mar 2016 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.1.0   | 27 Feb 2016 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.0.6   | 18 Nov 2015 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.0.5   | 13 Nov 2015 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.0.4   |  8 Nov 2015 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.0.3   |  6 Nov 2015 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.0.2   | 22 Oct 2015 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.0.1   | 10 Oct 2015 | 15 (Android 4.0.3)  | 21 (Android 5)
| 1.0.0   |  7 Oct 2015 | 15 (Android 4.0.3)  | 21 (Android 5)

## Installation

To install use ADB like this, or email/copy the APK into the device and install on the device

```
adb install -r recommender-2.0.0-38-70d3c10-release.apk
```

## Notes

Major changes for each version

### v2.1.0 (39) - 13 Jun 2024
- Fixed bug when moving a recommendation from the edit screen
- Removed dependency on AppCenter as it is being retired
- Updated help text to include link to release archive

### v2.0.0 (38) - 11 Oct 2023
- Display the date/time a recommendation was last updated, on the list and the recommendation 
- Added ability to order by last updated time
- Added highlighting for the search text
- Tidy up UI for recommendation lists
- Display number of categories selected on nav menu
- Store datetime in the DB in UTC, fix issue with importing dates from backup
- Removed Butterknife

### v1.8.0 (37) - 29 Apr 2023
- Updated help text
- Added amazon app store url when sharing

### v1.7.0 (36) - 12 Apr 2023
- Accessibility updates
- Scaleable text support
- Dark mode support
- Added the destination tab name to the import screen
- Fixed crash on settings screen when there is no sd card present
- Replaced toast with snackbar

### v1.6.0 (35) - 13 Feb 2023
- First version available through the Amazon App Store
- Updated app to for Android 10/11/12 permissions and file system changes
- Fixed bug on backup and share when there are no recommendations
- Switched from Firebase to AppCenter for analytics and crashes
- Added support for debug and release builds on the same device
- Added hash to build version
- Switch from the support libraries to AndroidX
- Fixed share receiver from the Kindle app
- Improved share receiver from amazon web pages
- Added share receiver from goodreads web pages
- Fixed issue with Android Beam

### v1.5.3 (34) - 26 Sep 2018
- Fixed bug in privacy menu item

### v1.5.2 (33) - 26 Sep 2018
- Added privacy statement required by google play store
- Removed Robolectric from all tests

### v1.5.0 (32) - 10 Jan 2017
- Added ability to be able to identify where a recommendation came from
- Also, optionally, you can add a nickname so that when you share a recommendation its added as the source of the recommendation

### v1.4.3 (31) - 21 Dec 2016
- Support Android 6 permission request for external storage

### v1.4.2 (30) - 19 Dec 2016
- Bug fix to enable sharing between different versions of the app.
- Enabled Android automatic app backup to backup recommendations
- Added source picker to recommendation

### v1.4.1 (29) - 1 Sep 2016
- Made the active tab indicated more obvious
- Fixed crash when restoring before a backup has been done

### v1.4.0 (28) - 28 Jul 2016
- Added category picker to recommendation

### v1.3.1 (27) - 18 Jul 2016
- Added firebase analytics

### v1.3.0 (26) - 23 Jun 2016
- Fixed handling of bluetooth share from windows 10 SendRecommendation

### v1.2.1 (25) - 30 Apr 2016
- Fixed rotation issue on main activity

### v1.2.0 (24) - 16 Apr 2016
- Added support for NFC/Android Beam

### v1.1.1 (22) - 27 Mar 2016
- Improved share receiver from amazon web pages
- Improved share receiver from kindle app
- Added import recommendations from email attachments/downloaders

### v1.1.0 (22) - 27 Feb 2016
- Added search to the recommendations list
- Added item count to the tabs on the recommendation list
- Fixed render issue with the text when changing the active tab

### v1.0.6 (21) - 18 Nov 2015
- Attempt to fix the redraw issue on the nav drawer

### v1.0.5 (20) - 13 Nov 2015
- Removed patch code to stop the crash when importing
- Implemented error handling in the repository subscribers
- Implemented subscriber re-initialisation via the event bus when overwhelmed
- Implemented bulk insert

### v1.0.4 (19) - 8 Nov 2015
- Added error reporting to the repo and put a delay into the inserter until we can write a bulk inserter

### v1.0.3 (18) - 6 Nov 2015

- List switches to correct tab after adding or moving a recommendation
- Added save/discard prompt when importing or receiving

### v1.0.2 (17) - 22 Oct 2015

- Added collapsing toolbar
- Added move action to edit recommendation

### v1.0.1 (16) - 10 Oct 2015

- RC2
- add recommendation to the correct list
- upped Wikipedia abstract text from 200 to 500 chars

### v1.0.0 (15) - 7 Oct 2015

- RC1

