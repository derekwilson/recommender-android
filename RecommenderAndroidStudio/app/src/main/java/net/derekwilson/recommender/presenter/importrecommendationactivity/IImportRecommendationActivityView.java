package net.derekwilson.recommender.presenter.importrecommendationactivity;

import net.derekwilson.recommender.presenter.IView;

public interface IImportRecommendationActivityView  extends IView {
	void showImportSuccessMessage(int numberInserted);

    void setTitle(int resId);
}
