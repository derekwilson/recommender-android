package net.derekwilson.recommender.tests.model.filtercollection;

import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.FilterSelectionItem;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.tests.BaseJunitTests;

import org.junit.Before;

import java.util.ArrayList;
import java.util.List;

public class TestSetup extends BaseJunitTests {

	protected FilterSelectionItem noFilterItem;
	protected FilterSelectionItem testItem1;
	protected FilterSelectionItem testItem2;
	protected FilterSelectionItem testItem3;

	// object under test
	protected FilterCollection collection;

	@Before
	public void setUp() {
		noFilterItem = setupItem("NOFILTER", true);
		testItem1 = setupItem("FILTER 1", false);
		testItem2 = setupItem("FILTER 2", false);
		testItem3 = setupItem("FILTER 3", false);

		collection = new FilterCollection();
	}

	private FilterSelectionItem setupItem(String name, boolean isNoFilter) {
		return new FilterSelectionItem(name, 11, false, isNoFilter);
	}

	protected List<FilterSelectionItem> getFilters(boolean getNoFilter, boolean getTestFilters) {
		List<FilterSelectionItem> items = new ArrayList<>(10);
		if (getNoFilter) {
			items.add(noFilterItem);
		}
		if (getTestFilters) {
			items.add(testItem1);
			items.add(testItem2);
			items.add(testItem3);
		}
		return items;
	}
}
