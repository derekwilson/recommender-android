package net.derekwilson.recommender.activity.main;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ActionMode;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.BaseActivity;
import net.derekwilson.recommender.activity.help.HelpActivity;
import net.derekwilson.recommender.activity.preferences.PreferencesActivity;
import net.derekwilson.recommender.fragment.recommendation.RecommendationListFragment;
import net.derekwilson.recommender.fragment.utility.OkCancelDialogFragment;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.scopes.DaggerIMainComponent;
import net.derekwilson.recommender.ioc.scopes.IMainComponent;
import net.derekwilson.recommender.ioc.scopes.MainModule;
import net.derekwilson.recommender.model.FilterSelectionItem;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.presenter.mainactivity.IMainActivityPresenter;
import net.derekwilson.recommender.presenter.mainactivity.IMainActivityView;
import net.derekwilson.recommender.view.LockableTabLayout;
import net.derekwilson.recommender.view.LockableViewPager;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements
		View.OnClickListener,
		NavigationView.OnNavigationItemSelectedListener,
		OkCancelDialogFragment.Listener,
		IMainActivityView {

	private final int REQUEST_CODE_ASK_PERMISSION = 123;

	private static final String KEY_TAB_0_FRAGMENT_TAG = "tab_0_fragment_key";
	private static final String KEY_TAB_1_FRAGMENT_TAG = "tab_1_fragment_key";
	private static final String TAG_DELETE_ALL_PROMPT = "delete_all_tag";

	private IMainComponent component;

	@Inject
	protected IMainActivityPresenter presenter;

	private FragmentDescriptionListAdapter adapter;
	private ActionMode actionMode = null;

	protected DrawerLayout drawer;
	protected NavigationView navigationView;
	protected FloatingActionButton fab;
	protected LockableTabLayout tabs;
	protected LockableViewPager viewPager;
	protected LinearLayout searchContainer;
	protected EditText searchEditText;
	protected ImageView searchClearIcon;
	@Override
	protected void bindControls() {
		super.bindControls();
		drawer = findViewById(R.id.drawer);
		navigationView = findViewById(R.id.nav_view);
		fab = findViewById(R.id.fab);
		tabs = findViewById(R.id.tabs);
		viewPager = findViewById(R.id.viewpager);
		searchContainer = findViewById(R.id.search_container);
		searchEditText = findViewById(R.id.search_view);
		searchClearIcon = findViewById(R.id.search_clear);
		searchClearIcon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				searchEditText.setText("");
			}
		});
	}

	protected IMainComponent getMainActivityComponent(Activity activity) {
		if (component == null) {
			component = DaggerIMainComponent.builder()
					.iApplicationComponent(getApplicationComponent())
					.baseActivityModule(new BaseActivityModule(activity))
					.mainModule(new MainModule())
					.build();
		}
		return component;
	}

	@Override
	protected void injectDependencies(IApplicationComponent component) {
		getMainActivityComponent(this).inject(this);    // will inject into the base as well as this class
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_main;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		String fragmentTag = getFragmentTag(0);
		if (fragmentTag != null) {
			outState.putString(KEY_TAB_0_FRAGMENT_TAG, fragmentTag);
		}
		fragmentTag = getFragmentTag(1);
		if (fragmentTag != null) {
			outState.putString(KEY_TAB_1_FRAGMENT_TAG, fragmentTag);
		}

		super.onSaveInstanceState(outState);
	}

	private void checkFragment(RecommendationListFragment fragment, String message) {
		if (fragment == null) {
			logger.getCurrentApplicationLogger().error("unable to create or find main tab fragments: " + message);
			throw new UnsupportedOperationException("cannot find fragment: " + message);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		toolbarMenuButtonNeeded = true;
		super.onCreate(savedInstanceState);

		logger.getCurrentApplicationLogger().debug("MainActivity started");
		presenter.bindView(this);

		if (navigationView != null) {
			navigationView.setNavigationItemSelectedListener(this);
			//setDrawerSize();
		}

		logger.getCurrentApplicationLogger().debug("MainActivity onCreate saveInstanceState NULL == {}", savedInstanceState == null);
		adapter = new FragmentDescriptionListAdapter(getSupportFragmentManager());
		RecommendationListFragment fragment0 = null;
		RecommendationListFragment fragment1 = null;
		if (savedInstanceState == null) {
			// we are being created new - create the fragments
			fragment0 = presenter.createTabListFragment(0);
			fragment1 = presenter.createTabListFragment(1);
		}
		else {
			// we are being restored - go find the fragments
			String fragmentTag = savedInstanceState.getString(KEY_TAB_0_FRAGMENT_TAG);
			if (fragmentTag != null) {
				fragment0 = (RecommendationListFragment)getSupportFragmentManager().findFragmentByTag(fragmentTag);
			}
			fragmentTag = savedInstanceState.getString(KEY_TAB_1_FRAGMENT_TAG);
			if (fragmentTag != null) {
				fragment1 = (RecommendationListFragment)getSupportFragmentManager().findFragmentByTag(fragmentTag);
			}
		}
		// the fragments should not be null
		// but if they are then lets record them in the developer console and fix the issue rather than hiding it
		checkFragment(fragment0,"fragment0");
		checkFragment(fragment1,"fragment1");
		// the index of these tabs needs to match up with what the presenter thinks is going on
		adapter.addFragment(fragment0, getString(presenter.getTabListTitleId(0)));
		adapter.addFragment(fragment1, getString(presenter.getTabListTitleId(1)));
		viewPager.setAdapter(adapter);
		setupTabs();
		tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				viewPager.setCurrentItem(tab.getPosition());
				selectTab(tab, true);
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {
				selectTab(tab, false);
			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {
			}
		});

		if (fab != null) {
			fab.setOnClickListener(this);
		}

		searchEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				presenter.updateSearchFilter(s.toString());
			}

			@Override
			public void afterTextChanged(Editable editable) {
			}
		});

		presenter.onCreate();
		// we currently dont need any explicit permissions - if we do then reinstate this call
		//setupPermissions();
	}

	private void setupTabs() {
		tabs.setupWithViewPager(viewPager);
		// Iterate over all tabs and set the custom view
		for (int index = 0; index < tabs.getTabCount(); index++) {
			TabLayout.Tab tab = tabs.getTabAt(index);
			if (tab != null) {
				tab.setCustomView(adapter.getTabView(this,index));
			}
		}
		selectTab(tabs.getTabAt(viewPager.getCurrentItem()), true);
	}

	private void selectTab(TabLayout.Tab tab, boolean selected) {
		if (tab == null) {
			return;
		}
		View customTab = (View) tab.getCustomView();
		if (customTab != null) {
			TextView tv = (TextView) customTab.findViewById(R.id.custom_text);
			if (tv != null) {
				tv.setTypeface(null, selected ? Typeface.BOLD : Typeface.NORMAL);
			}
		}
	}

	private void setupPermissions() {
		int hasStoragePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
		if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
			if (!ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
				showMessageOKCancel(getString(R.string.permission_prompt),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								ActivityCompat.requestPermissions(MainActivity.this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSION);
							}
						});
				return;
			}
			ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSION);
		}
	}

	private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
		new AlertDialog.Builder(MainActivity.this)
				.setMessage(message)
				.setPositiveButton(getString(R.string.ok_cancel_ok), okListener)
				.setNegativeButton(getString(R.string.ok_cancel_cancel), null)
				.create()
				.show();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case REQUEST_CODE_ASK_PERMISSION:
			{
				Map<String, Integer> perms = new HashMap<String, Integer>();
				// Initial
				perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
				perms.put(android.Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
				// Fill with results
				for (int i = 0; i < permissions.length; i++)
					perms.put(permissions[i], grantResults[i]);
				// Check for ACCESS_FINE_LOCATION
				if (perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
						&& perms.get(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
					// All Permissions Granted
					logger.getCurrentApplicationLogger().debug("MainActivity permissions granted");
				} else {
					// Permission Denied
					logger.getCurrentApplicationLogger().debug("MainActivity permissions denied");
					showMessage(R.string.permission_denied);
				}
			}
			break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	@Override
	public void onStart() {
		logger.getCurrentApplicationLogger().debug("mainActivity.onStart()");
		super.onStart();
	}

	@Override
	public void onStop() {
		logger.getCurrentApplicationLogger().debug("mainActivity.onStop()");
		super.onStop();
	}

	@Override
	public void onDestroy() {
		logger.getCurrentApplicationLogger().debug("mainActivity.onDestroy()");
		presenter.unbindView();
		presenter.onDestroy();
		super.onDestroy();
	}

	private void setDrawerSize() {
		int width = getResources().getDisplayMetrics().widthPixels/2;
		DrawerLayout.LayoutParams params = (androidx.drawerlayout.widget.DrawerLayout.LayoutParams) navigationView.getLayoutParams();
		params.width = width;
		navigationView.setLayoutParams(params);
	}

	private boolean isSearchVisible() {
		return searchContainer.getVisibility() == View.VISIBLE;
	}

	private void enterSearchMode() {
		drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		searchContainer.setVisibility(View.VISIBLE);
		invalidateOptionsMenu();
		this.fab.setVisibility(View.GONE);

		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);

		// Shift focus to the updateSearchFilter EditText
		searchContainer.requestFocus();

		// Pop up the soft keyboard
		new Handler().postDelayed(new Runnable() {
			public void run() {
				searchEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
				searchEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
			}
		}, 200);
	}

	private void exitSearchMode() {
		presenter.updateSearchFilter(null);
		drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		searchContainer.setVisibility(View.GONE);
		invalidateOptionsMenu();

		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);

		// Hide the keyboard because the updateSearchFilter box has been hidden
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);

		// show the fab - but wait for the keyboard to go
		new Handler().postDelayed(new Runnable() {
			public void run() {
				searchEditText.setText("");
				fab.setVisibility(View.VISIBLE);
			}
		}, 200);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.action_list_recommendations, menu);
		return true;
	}

	private void setMenuItemVisibility(Menu menu, int id, boolean isVisible) {
		MenuItem item = menu.findItem(id);
		if (item != null) {
			item.setVisible(isVisible);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		setMenuItemVisibility(menu, R.id.action_deleteAllRecommendation, !isSearchVisible());
		setMenuItemVisibility(menu, R.id.action_search, !isSearchVisible());
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				if (isSearchVisible()) {
					exitSearchMode();
				} else {
					drawer.openDrawer(GravityCompat.START);
				}
				return true;
			case R.id.action_deleteAllRecommendation:
				deleteAllRecommendations();
				return true;
			case R.id.action_search:
				enterSearchMode();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void deleteAllRecommendations() {
		DialogFragment newFragment = OkCancelDialogFragment.newInstance(
				getString(R.string.delete_all_message),
				getString(R.string.ok_cancel_yes),
				getString(R.string.ok_cancel_no)
		);
		newFragment.show(getSupportFragmentManager(), TAG_DELETE_ALL_PROMPT);
	}

	@Override
	public void ok(String tag) {
		logger.getCurrentApplicationLogger().debug("MainActivity OK ({})", tag);
		// it could be delete all prompt
		if (TAG_DELETE_ALL_PROMPT.equals(tag)) {
			presenter.deleteAllRecommendations();
			return;
		}
		// or it could be delete selected if we are in multi select mode
		RecommendationListFragment fragment = getActiveListFragment();
		if (fragment != null) {
			fragment.ok(tag);
		}
	}

	@Override
	public void cancel(String tag) {
		logger.getCurrentApplicationLogger().debug("MainActivity Cancel ({})", tag);
		RecommendationListFragment fragment = getActiveListFragment();
		if (fragment != null) {
			fragment.cancel(tag);
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.fab:
				Intent intent = presenter.getNewRecommendationIntent();
				startActivity(intent);
				break;
		}
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem menuItem) {
		logger.getCurrentApplicationLogger().debug("Navigation item selected: {} checked {}", menuItem.getTitle(), menuItem.isChecked());
		switch (menuItem.getItemId()) {
			case R.id.navigation_category_filter_entry:
				if (menuItem.getOrder() == 0) {
					presenter.clearFilter();
				}
				else {
					presenter.toggleFilterSelectionByName(menuItem.getTitle().toString());
				}
				return true;
			case R.id.navigation_settings:
				Intent settingsIntent = new Intent(this, PreferencesActivity.class);
				startActivity(settingsIntent);
				break;
			case R.id.navigation_help:
				Intent helpIntent = new Intent(this, HelpActivity.class);
				startActivity(helpIntent);
				break;
		}
		drawer.closeDrawers();
		return false;
	}

	@Override
	public void onBackPressed() {
		if (isSearchVisible()) {
			logger.getCurrentApplicationLogger().debug("Search mode, back key suppressed");
			exitSearchMode();
			return;
		}
		if (drawer.isDrawerOpen(navigationView)) {
			logger.getCurrentApplicationLogger().debug("Navigation drawer open, back key suppressed");
			drawer.closeDrawers();
			return;
		}
		if (actionMode != null) {
			logger.getCurrentApplicationLogger().debug("in action mode, back key suppressed");
			actionMode.finish();
			return;
		}
		super.onBackPressed();
	}

	@Override
	public void onSupportActionModeStarted (ActionMode mode) {
		logger.getCurrentApplicationLogger().debug("MainActivity: action mode started");
		this.actionMode = mode;
		this.fab.setVisibility(View.GONE);
		this.tabs.setTabSwitchEnabled(false);
		this.viewPager.setPagingEnabled(false);
		super.onSupportActionModeStarted(mode);
	}

	@Override
	public void onSupportActionModeFinished (ActionMode mode) {
		logger.getCurrentApplicationLogger().debug("MainActivity: action mode ended");
		this.actionMode = null;
		this.fab.setVisibility(View.VISIBLE);
		this.tabs.setTabSwitchEnabled(true);
		this.viewPager.setPagingEnabled(true);
		super.onSupportActionModeFinished(mode);
	}

	@Override
	public int getActiveTab() {
		return viewPager.getCurrentItem();
	}

	@Override
	public void setActiveTab(final int tabIndex) {
		logger.getCurrentApplicationLogger().debug("setting active tab from tab {} to tab index {}", getActiveTab(), tabIndex);
		tabs.getTabAt(tabIndex).select();
	}

	@Override
	public void updateTabTitles(int tabIndex) {
		// get the item count from the fragment in the adapter
		logger.getCurrentApplicationLogger().debug("update tab title {}", tabIndex);
		int count = getRecommendationListFragment(tabIndex).getItemCount();
		logger.getCurrentApplicationLogger().debug("update tab title {} count = {}, number of fragments {}", tabIndex, count, adapter.getCount());
		adapter.setItemCount(tabIndex, count);
		setupTabs();
	}

	private RecommendationListFragment getActiveListFragment() {
		int currentPageIndex = viewPager.getCurrentItem();
		return getRecommendationListFragment(currentPageIndex);
	}

	private RecommendationListFragment getRecommendationListFragment(int index) {
		Fragment fragment = adapter.getItem(index);
		if (fragment != null) {
			return ((RecommendationListFragment) fragment);
		}
		logger.getCurrentApplicationLogger().warn("cannot find list fragment for {}", index);
		return null;
	}

	private String getFragmentTag(int tabIndex) {
		RecommendationListFragment fragment = getRecommendationListFragment(tabIndex);
		if (fragment != null) {
			logger.getCurrentApplicationLogger().debug("fragment tag for {} is {}", tabIndex, fragment.getTag());
			return fragment.getTag();
		}
		logger.getCurrentApplicationLogger().warn("cannot find tag for fragment {}", tabIndex);
		return null;
	}


	@Override
	public void end() {
		finish();
	}

	@Override
	public void showError(Throwable throwable) {
		displayMessage(throwable.getMessage());
	}

	@Override
	public void showMessage(int message) {
		displayMessage(getString(message));
	}

	private MenuItem getCategoriesMenu() {
		Menu menu = navigationView.getMenu();
		MenuItem categories = menu.getItem(2);      // hmmm not ideal - can we do this by ID ?
		return categories;
	}

	private SubMenu getCategoriesSubMenu() {
		MenuItem categories = getCategoriesMenu();
		SubMenu categoriesSubMenu = null;
		if (categories != null) {
			categoriesSubMenu = categories.getSubMenu();
		}
		return categoriesSubMenu;
	}

	private void removeAllCategoryFilters() {
		SubMenu categoriesSubMenu = getCategoriesSubMenu();
		if (categoriesSubMenu != null) {
			categoriesSubMenu.clear();
		}
	}

	private void setCategoriesMenuTitle(@NonNull final IFilterCollection currentFilter) {
		MenuItem categories = getCategoriesMenu();
		if (categories == null) {
			logger.getCurrentApplicationLogger().warn("setCategoriesMenuTitle - cannot find menu item");
			return;
		}
		if (currentFilter.isNoFilter()) {
			categories.setTitle(getString(R.string.action_title_category_filter_none));
		} else {
			categories.setTitle(getString(R.string.action_title_category_filter_selected, currentFilter.getNumberOfSelectedFilters()));
		}
	}

	@Override
	public void showCategoryFilter(IFilterCollection currentFilter) {
		SubMenu categoriesSubMenu = getCategoriesSubMenu();
		if (categoriesSubMenu == null) {
			return;
		}
		removeAllCategoryFilters();
		int order = 0;
		if (currentFilter.getFilters() != null) {
			for (FilterSelectionItem item : currentFilter.getFilters()) {
				categoriesSubMenu.add(R.id.navigation_category_group, R.id.navigation_category_filter_entry, order, item.getText())
						.setIcon(item.getIconId())
						.setCheckable(true)
						.setChecked(item.isSelected());
				order++;
			}
		}
		setCategoriesMenuTitle(currentFilter);
	}

	@Override
	public void setCategoryFilterUiState(final IFilterCollection currentFilter) {
		final SubMenu categoriesSubMenu = getCategoriesSubMenu();
		if (categoriesSubMenu == null) {
			return;
		}
		// we may be called from another thread - we must do the action on the UI thread - so we use post
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				if (currentFilter.getFilters() != null) {
					for (int index = 0; index < categoriesSubMenu.size(); index++) {
						MenuItem menuItem = categoriesSubMenu.getItem(index);
						boolean selected = currentFilter.isFilterSelected(index);
						//logger.getCurrentApplicationLogger().debug("setCategoryFilterUiState {} {} ", menuItem.getTitle(), selected);
						menuItem.setChecked(selected);
					}
				}
				setCategoriesMenuTitle(currentFilter);
			}
		});
	}

	private void applyFilterToTab(int index, IFilterCollection currentFilter) {
		RecommendationListFragment fragment = getRecommendationListFragment(index);
		if (fragment != null) {
			logger.getCurrentApplicationLogger().debug("MainActivity - applyFilterToTab {}", index);
			fragment.setFilter(currentFilter);
		}
	}

	@Override
	public void applyFilterToAllTabs(IFilterCollection currentFilter) {
		applyFilterToTab(0, currentFilter);
		applyFilterToTab(1, currentFilter);
	}
}
