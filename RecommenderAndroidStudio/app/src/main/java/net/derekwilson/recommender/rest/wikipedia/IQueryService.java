package net.derekwilson.recommender.rest.wikipedia;

import net.derekwilson.recommender.rest.wikipedia.model.QueryResult;
import net.derekwilson.recommender.tasks.ITaskResultHandler;

import rx.Subscription;

public interface IQueryService {
	Subscription getQueryResult(final String title, final ITaskResultHandler<QueryResult> handler);
}
