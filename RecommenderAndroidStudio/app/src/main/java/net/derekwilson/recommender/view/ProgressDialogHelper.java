package net.derekwilson.recommender.view;

import android.app.ProgressDialog;
import android.content.Context;

import net.derekwilson.recommender.R;

public class ProgressDialogHelper {

	private ProgressDialog progress;

	public ProgressDialogHelper(Context context) {
		progress = new ProgressDialog(context);
		progress.setCancelable(false);
		progress.setIndeterminate(true);
	}

	public void setMessage(String message) {
		progress.setMessage(message);
	}

	public void showProgress(String message) {
		setMessage(message);
		progress.show();
	}

	public void hideProgress() {
		progress.dismiss();
	}

}
