package net.derekwilson.recommender.json;

import android.content.Context;
import android.net.Uri;

import androidx.core.content.FileProvider;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

public class ExportToJson extends BaseImportExport implements IExporter {

	protected ICrashReporter crashReporter;

	@Inject
	public ExportToJson(ObjectMapper mapper, ILoggerFactory logger, ITextUtils textUtils, Context context, ICrashReporter crashReporter) {
		super(mapper, logger, textUtils, context);
		this.crashReporter = crashReporter;
	}

	@Override
	public String getBaseUri() {
		return getExportBasePath();
	}

	public String getExportUri(){
		return getExportPath();
	}

	@Override
	public String exportRecommendationsToString(List<Recommendation> recommendations) {
		try {
			String jsonInString = mapper.writeValueAsString(recommendations);
			return jsonInString;
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error writing JSON", e);
			crashReporter.logNonFatalException(e);
		}
		return null;
	}

	@Override
	public Uri exportRecommendations(List<Recommendation> recommendations, String exportFilename) throws ExternalStorageException {
		Uri uri = null;
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			File sDir = new File(getExportBasePath());
			if (!sDir.exists()) {
				sDir.mkdirs();
			}
			String filename = (exportFilename != null ? exportFilename : getExportPath());
			logger.getCurrentApplicationLogger().debug("Writing {} recommendations to {}", recommendations.size(), filename);
			File file = new File(filename);
			//if (!file.exists()) {
				writeRecommendations(file, recommendations);
			//}
			// make this file readable by other apps
			file.setReadable(true, false);
			uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
		} else {
			logger.getCurrentApplicationLogger().error("There is no memory card available. This application requires a memory card to store the files on.");
		}
		return uri;
	}

	private void writeRecommendations(File file, List<Recommendation> recommendations) throws ExternalStorageException {
		try {
			mapper.writeValue(file, recommendations);
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error writing JSON", e);
			throw new ExternalStorageException(file.getAbsolutePath(), e);
		}
	}
}
