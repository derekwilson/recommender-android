package net.derekwilson.recommender.wrapper;

import java.util.Calendar;
public interface ICalendarUtils {
    String convertCalendarToDateTimeString(Calendar date);
    String convertCalendarToDateString(Calendar date);
    Calendar getNow();
}
