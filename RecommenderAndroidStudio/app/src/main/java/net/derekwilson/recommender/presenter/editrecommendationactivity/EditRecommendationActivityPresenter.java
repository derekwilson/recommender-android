package net.derekwilson.recommender.presenter.editrecommendationactivity;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.analytics.IEventLogger;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.preferences.IPreferencesProvider;
import net.derekwilson.recommender.presenter.BasePresenter;
import net.derekwilson.recommender.sharing.IEmailIntentSharer;
import net.derekwilson.recommender.sharing.INdefMessageSharer;
import net.derekwilson.recommender.sharing.IntentShareResult;
import net.derekwilson.recommender.sharing.NdefMessageShareResult;
import net.derekwilson.recommender.wrapper.ITextUtils;

import javax.inject.Inject;

public class EditRecommendationActivityPresenter extends BasePresenter<IEditRecommendationActivityView> implements IEditRecommendationActivityPresenter {
	private IPreferencesProvider preferences;
	private ITextUtils txtUtils;
	private IEmailIntentSharer emailSender;
	private INdefMessageSharer nfcSender;
	private IEventLogger eventLogger;
	private ICrashReporter crashReporter;

	@Inject
	public EditRecommendationActivityPresenter(IPreferencesProvider preferences, ITextUtils txtUtils, IEmailIntentSharer emailSender, INdefMessageSharer nfcSender, IEventLogger eventLogger, ICrashReporter crashReporter) {
		this.preferences = preferences;
		this.txtUtils = txtUtils;
		this.emailSender = emailSender;
		this.nfcSender = nfcSender;
		this.eventLogger = eventLogger;
		this.crashReporter = crashReporter;
	}

	@Override
	public void onCreate() {
		setMainTitleForRecommendationType();
	}

	private void setMainTitleForRecommendationType() {
		RecommendationType recommendationType = view.getRecommendationType();
		if (recommendationType == RecommendationType.RECOMMENDATION) {
			view.setMainTitleTextId(R.string.list_title_recommendation);
		} else {
			view.setMainTitleTextId(R.string.list_title_trythis);
		}
	}

	private boolean isNewRecommendation(Recommendation recommendation) {
		// check to see if we are editing a recommendation or if we are creating a new one
		if (recommendation != null && recommendation.getId() != -1) {
			// it has an id - so its not new
			return false;
		}
		return true;
	}

	@Override
	public boolean canShare(Recommendation recommendation) {
		return !isNewRecommendation(recommendation);
	}

	@Override
	public boolean canDelete(Recommendation recommendation) {
		return !isNewRecommendation(recommendation);
	}

	@Override
	public boolean canMove(Recommendation recommendation) {
		return !isNewRecommendation(recommendation);
	}

	@Override
	public int getMoveMenuTextId(RecommendationType recommendationType) {
		if (recommendationType == RecommendationType.RECOMMENDATION) {
			return R.string.action_move_to_trythis;
		} else {
			return R.string.action_move_to_recommendation;
		}
	}

	private Recommendation getRecommendationToShare() {
		// TODO: maybe allow sharing of a new recommendation by getting the current state from the fragment
		// we need a copy of the recommendation as we are going to change the source
		Recommendation shareRecommendation = new Recommendation(view.getRecommendationFromPassedIntent());
		// if we do get the current state then make sure we do not overwrite any edits
		String nickname = preferences.getPreferenceString(view.getNicknameKey());
		if (!txtUtils.isEmpty(nickname)) {
			shareRecommendation.setSource(nickname);
		}
		// when we make a recommendation we are suggesting that the receiver "Try This"
		shareRecommendation.setType(RecommendationType.TRY_THIS);
		return shareRecommendation;
	}

	@Override
	public void shareRecommendation() {
		Recommendation shareRecommendation = getRecommendationToShare();
		if (shareRecommendation != null) {
			eventLogger.logShareRecommendation(shareRecommendation, "edit");
			IntentShareResult result = null;
			try {
				result = emailSender.getIntent(shareRecommendation);
				view.startIntent(result.SendIntent, R.string.email_share_text);
			} catch (ExternalStorageException e) {
				crashReporter.logNonFatalException(e);
				view.showMessage(R.string.storage_error);
			}
		}
	}

	@Override
	public void beamRecommendation() {
		Recommendation shareRecommendation = getRecommendationToShare();
		if (shareRecommendation != null) {
			eventLogger.logShareRecommendation(shareRecommendation, "edit-nfc");
			NdefMessageShareResult result = nfcSender.getMessage(shareRecommendation);
			view.sendNfcMessage(result.Message);
			view.showMessage(R.string.nfc_beam);
		}
	}
}
