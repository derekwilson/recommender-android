package net.derekwilson.recommender.tests.presenter.mainactivity;

import android.app.Activity;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.event.IEventBus;
import net.derekwilson.recommender.event.RxEventBus;
import net.derekwilson.recommender.ioc.scopes.IMainComponent;
import net.derekwilson.recommender.model.FilterCollection;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.presenter.mainactivity.IMainActivityPresenter;
import net.derekwilson.recommender.presenter.mainactivity.IMainActivityView;
import net.derekwilson.recommender.presenter.mainactivity.MainActivityPresenter;
import net.derekwilson.recommender.tests.BaseJunitTests;
import net.derekwilson.recommender.tests.testobjects.TestRecommendationRepository;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

public class PresenterTestsSetup extends BaseJunitTests {

	// mocks
	protected Activity mockActivity = mock(Activity.class);
	protected IMainActivityView mockView = mock(IMainActivityView.class);
	protected IRecommendationRepository mockRepository = mock(IRecommendationRepository.class);
	protected IFilterCollection filter = new FilterCollection();

	// will use a concrete object until we have to use a mock
	protected TestRecommendationRepository realRepository = new TestRecommendationRepository();

	protected IMainComponent component = null;
	protected IEventBus eventBus = new RxEventBus();

	// object under test
	protected IMainActivityPresenter presenter;

	protected void setupPresenter(boolean bind, boolean useRealRepository) {
		presenter = new MainActivityPresenter(
				filter,
				(useRealRepository ? realRepository : mockRepository),
				mockLoggerFactory,
				mockActivity,
				eventBus,
				mockActivity
		);
		if (bind) {
			presenter.bindView(mockView);
		}
	}

	protected List<String> getTestCategories() {
		ArrayList<String> categories = new ArrayList<>(5);
		categories.add("1");
		categories.add("2");
		categories.add("3");
		categories.add("4");
		categories.add("5");
		return categories;
	}

}
