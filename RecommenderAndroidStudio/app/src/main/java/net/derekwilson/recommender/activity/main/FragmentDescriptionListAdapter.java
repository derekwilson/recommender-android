package net.derekwilson.recommender.activity.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import net.derekwilson.recommender.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentDescriptionListAdapter extends FragmentPagerAdapter {
	private List<FragmentDescription> fragmentDescriptions = new ArrayList<>(10);

	public FragmentDescriptionListAdapter(FragmentManager manager) {
		super(manager);
	}

	public void addFragment(Fragment fragment, String title) {
		fragmentDescriptions.add(new FragmentDescription(fragment,title,0));
	}

	@Override
	public Fragment getItem(int position) {
		return fragmentDescriptions.get(position).getFragment();
	}

	@Override
	public int getCount() {
		return fragmentDescriptions.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return fragmentDescriptions.get(position).getTitle();
	}

	public void setItemCount(int index, int itemCount) {
		fragmentDescriptions.get(index).setItemCount(itemCount);
	}

	public View getTabView(Context context, int position) {
		View tab = LayoutInflater.from(context).inflate(R.layout.tab_custom, null);
		TextView tv = (TextView) tab.findViewById(R.id.custom_text);
		tv.setText(getPageTitle(position));
		return tab;
	}
}