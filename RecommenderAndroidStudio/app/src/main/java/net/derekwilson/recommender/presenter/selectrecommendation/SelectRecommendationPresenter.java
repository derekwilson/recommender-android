package net.derekwilson.recommender.presenter.selectrecommendation;

import net.derekwilson.recommender.presenter.BasePresenter;

import javax.inject.Inject;

public class SelectRecommendationPresenter extends BasePresenter<ISelectRecommendationView> implements ISelectRecommendationPresenter {
	@Inject
	public SelectRecommendationPresenter() {
	}
}
