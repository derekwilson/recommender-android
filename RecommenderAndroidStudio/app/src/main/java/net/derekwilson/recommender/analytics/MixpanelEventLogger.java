package net.derekwilson.recommender.analytics;

import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import net.derekwilson.recommender.BuildConfig;
import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

public class MixpanelEventLogger implements IEventLogger {

	// to see the events go to
	// https://eu.mixpanel.com/project/3326975/view/3834999/app/home

	private final int MAX_VALUE_LENGTH = 36;
	private final String EVENT_SHARE_SINGLE = "Share_Single";
	private final String EVENT_SHARE_MULTIPLE = "Share_Multiple";
	private final String EVENT_RECEIVE_SINGLE = "Receive_Single";
	private final String EVENT_RECEIVE_MULTIPLE = "Receive_Multiple";
	private final String EVENT_ERROR = "Error";
	private final String EVENT_LIFECYCLE_START_APP = "Lifecycle_Start_App";

	private final String PROPERTY_ORIGIN = "Origin";
	private final String PROPERTY_TITLE = "Title";
	private final String PROPERTY_URI = "Uri";
	private final String PROPERTY_QUANTITY = "Quantity";
	private final String PROPERTY_UI_MODE = "UiMode";
	private final String PROPERTY_FONT_SCALING = "FontScaling";

	private ILoggerFactory logger;
	private ICrashReporter crashReporter;
	private Context applicationContext;

	private MixpanelAPI mixpanel;

	@Inject
	public MixpanelEventLogger(ILoggerFactory logger, ICrashReporter crashReporter, Context applicationContext) {
        this.logger = logger;
        this.crashReporter = crashReporter;
        this.applicationContext = applicationContext;

		logger.getCurrentApplicationLogger().debug("MixpanelEventLogger: ctor - start");
		try {
			mixpanel = MixpanelAPI.getInstance(applicationContext, BuildConfig.MIXPANEL_TOKEN, true);
			logger.getCurrentApplicationLogger().debug("MixpanelAnalyticsEngine: init - id  {}", mixpanel.getDistinctId());
		} catch (Exception ex) {
			// dont let the analytics crash the app
			logger.getCurrentApplicationLogger().error("MixpanelAnalyticsEngine: init - error", ex);
			crashReporter.logNonFatalException(ex);
		}
		logger.getCurrentApplicationLogger().debug("MixpanelEventLogger: ctor - end");
    }

	private void trackEvent(String event, JSONObject properties) {
		if (mixpanel == null) {
			return;
		}
		synchronized(mixpanel) {
			try {
				mixpanel.track(event, properties);
				logger.getCurrentApplicationLogger().debug("MixpanelAnalyticsEngine: trackEvent  {}", event);
			} catch (Exception ex) {
				// dont let the analytics crash the app
				logger.getCurrentApplicationLogger().error("MixpanelAnalyticsEngine: trackEvent - error", ex);
				crashReporter.logNonFatalException(ex);
			}
		}
	}

	private String getValue(String s) {
		int maxLength = (s.length() < MAX_VALUE_LENGTH) ? s.length() : MAX_VALUE_LENGTH;
		return s.substring(0, maxLength);
	}

	private void addRecomendations(JSONObject properties, List<Recommendation> recommendations) throws JSONException {
		if (properties == null || recommendations == null) {
			return;
		}
		int index = 0;
		for (Recommendation thisRecomendation : recommendations) {
			properties.put(PROPERTY_TITLE + "_" + Integer.toString(index), getValue(thisRecomendation.getTitle()));
			properties.put(PROPERTY_URI + "_" + Integer.toString(index), getValue(thisRecomendation.getUri()));
			index++;
			if (index > 9) {
				return;
			}
		}
	}

	@Override
	public void logShareRecommendation(Recommendation recommendation, String origin) {
		JSONObject properties = new JSONObject();
		try {
			properties.put(PROPERTY_ORIGIN, getValue(origin));
			properties.put(PROPERTY_TITLE, getValue(recommendation.getTitle()));
			properties.put(PROPERTY_URI, getValue(recommendation.getUri()));
		} catch (Exception ex) {
			// dont let the analytics crash the app
			logger.getCurrentApplicationLogger().error("MixpanelAnalyticsEngine: logShareRecommendation - error", ex);
			crashReporter.logNonFatalException(ex);
		}
		trackEvent(EVENT_SHARE_SINGLE, properties);
	}

	@Override
	public void logShareRecommendations(List<Recommendation> recommendations, String origin) {
		JSONObject properties = new JSONObject();
		try {
			properties.put(PROPERTY_ORIGIN, getValue(origin));
			properties.put(PROPERTY_QUANTITY, String.valueOf(recommendations.size()));
			addRecomendations(properties, recommendations);
		} catch (Exception ex) {
			// dont let the analytics crash the app
			logger.getCurrentApplicationLogger().error("MixpanelAnalyticsEngine: logShareRecommendations - error", ex);
			crashReporter.logNonFatalException(ex);
		}
		trackEvent(EVENT_SHARE_MULTIPLE, properties);
	}

	@Override
	public void logReceivedRecommendation(Recommendation recommendation, String origin) {
		JSONObject properties = new JSONObject();
		try {
			properties.put(PROPERTY_ORIGIN, getValue(origin));
			if (recommendation != null) {
				properties.put(PROPERTY_TITLE, getValue(recommendation.getTitle()));
				properties.put(PROPERTY_URI, getValue(recommendation.getUri()));
			}
		} catch (Exception ex) {
			// dont let the analytics crash the app
			logger.getCurrentApplicationLogger().error("MixpanelAnalyticsEngine: logReceivedRecommendation - error", ex);
			crashReporter.logNonFatalException(ex);
		}
		trackEvent(EVENT_RECEIVE_SINGLE, properties);
	}

	@Override
	public void logReceivedRecommendations(List<Recommendation> recommendations, String origin) {
		JSONObject properties = new JSONObject();
		try {
			properties.put(PROPERTY_ORIGIN, getValue(origin));
			properties.put(PROPERTY_QUANTITY, String.valueOf(recommendations.size()));
			addRecomendations(properties, recommendations);
		} catch (Exception ex) {
			// dont let the analytics crash the app
			logger.getCurrentApplicationLogger().error("MixpanelAnalyticsEngine: logReceivedRecommendations - error", ex);
			crashReporter.logNonFatalException(ex);
		}
		trackEvent(EVENT_RECEIVE_MULTIPLE, properties);
	}

	@Override
	public void logErrorEvent() {
		trackEvent(EVENT_ERROR, null);
	}

	@Override
	public void logStartup(String fontScaling, String uiMode) {
		JSONObject properties = new JSONObject();
		try {
			properties.put(PROPERTY_UI_MODE, getValue(uiMode));
			properties.put(PROPERTY_FONT_SCALING, String.valueOf(fontScaling));
		} catch (Exception ex) {
			// dont let the analytics crash the app
			logger.getCurrentApplicationLogger().error("MixpanelAnalyticsEngine: logStartup - error", ex);
			crashReporter.logNonFatalException(ex);
		}
		trackEvent(EVENT_LIFECYCLE_START_APP, properties);
	}
}

/*
 * - cannot leave this code here as we have removed the dependencies
 *
public class AppCenterEventLogger implements IEventLogger {

	// to see the events go to
	// https://appcenter.ms/users/derekwilson/apps/RecommenderRelease/analytics/overview

	private final int MAX_VALUE_LENGTH = 36;
	private final String EVENT_SHARE_SINGLE = "Share_Single";
	private final String EVENT_SHARE_MULTIPLE = "Share_Multiple";
	private final String EVENT_RECEIVE_SINGLE = "Receive_Single";
	private final String EVENT_RECEIVE_MULTIPLE = "Receive_Multiple";
	private final String EVENT_ERROR = "Error";
	private final String EVENT_LIFECYCLE_START_APP = "Lifecycle_Start_App";

	private final String PROPERTY_ORIGIN = "Origin";
	private final String PROPERTY_TITLE = "Title";
	private final String PROPERTY_URI = "Uri";
	private final String PROPERTY_QUANTITY = "Quantity";
	private final String PROPERTY_UI_MODE = "UiMode";
	private final String PROPERTY_FONT_SCALING = "FontScaling";

	@Inject
	public AppCenterEventLogger() {
	}

	private String getValue(String s) {
		int maxLength = (s.length() < MAX_VALUE_LENGTH) ? s.length() : MAX_VALUE_LENGTH;
		return s.substring(0, maxLength);
	}

	private void addRecomendations(Map<String, String> properties, List<Recommendation> recommendations) {
		if (properties == null || recommendations == null) {
			return;
		}
		int index = 0;
		for (Recommendation thisRecomendation : recommendations) {
			properties.put(PROPERTY_TITLE + "_" + Integer.toString(index), getValue(thisRecomendation.getTitle()));
			properties.put(PROPERTY_URI + "_" + Integer.toString(index), getValue(thisRecomendation.getUri()));
			index++;
			if (index > 9) {
				return;
			}
		}
	}

	@Override
	public void logShareRecommendation(Recommendation recommendation, String origin) {
		Map<String, String> properties = new HashMap<>();
		properties.put(PROPERTY_ORIGIN, getValue(origin));
		properties.put(PROPERTY_TITLE, getValue(recommendation.getTitle()));
		properties.put(PROPERTY_URI, getValue(recommendation.getUri()));
		Analytics.trackEvent(EVENT_SHARE_SINGLE, properties);
	}

	@Override
	public void logShareRecommendations(List<Recommendation> recommendations, String origin) {
		Map<String, String> properties = new HashMap<>();
		properties.put(PROPERTY_ORIGIN, getValue(origin));
		properties.put(PROPERTY_QUANTITY, String.valueOf(recommendations.size()));
		addRecomendations(properties, recommendations);
		Analytics.trackEvent(EVENT_SHARE_MULTIPLE, properties);
	}

	@Override
	public void logReceivedRecommendation(Recommendation recommendation, String origin) {
		Map<String, String> properties = new HashMap<>();
		properties.put(PROPERTY_ORIGIN, getValue(origin));
		if (recommendation != null) {
			properties.put(PROPERTY_TITLE, getValue(recommendation.getTitle()));
			properties.put(PROPERTY_URI, getValue(recommendation.getUri()));
		}
		Analytics.trackEvent(EVENT_RECEIVE_SINGLE, properties);
	}

	@Override
	public void logReceivedRecommendations(List<Recommendation> recommendations, String origin) {
		Map<String, String> properties = new HashMap<>();
		properties.put(PROPERTY_ORIGIN, getValue(origin));
		properties.put(PROPERTY_QUANTITY, String.valueOf(recommendations.size()));
		addRecomendations(properties, recommendations);
		Analytics.trackEvent(EVENT_RECEIVE_MULTIPLE, properties);
	}

	@Override
	public void logErrorEvent() {
		Analytics.trackEvent(EVENT_ERROR);
	}

	@Override
	public void logStartup(String fontScaling, String uiMode) {
		Map<String, String> properties = new HashMap<>();
		properties.put(PROPERTY_UI_MODE, getValue(uiMode));
		properties.put(PROPERTY_FONT_SCALING, String.valueOf(fontScaling));
		Analytics.trackEvent(EVENT_LIFECYCLE_START_APP, properties);
	}
}

 */