package net.derekwilson.recommender.file;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.logging.ILoggerFactory;

import javax.inject.Inject;

public class ImageGetter implements Html.ImageGetter {

	private Context context;
	private ILoggerFactory logger;

	@Inject
	public ImageGetter(Context context, ILoggerFactory logger) {
		this.context = context;
		this.logger = logger;
	}

	public Drawable getDrawable(String source) {
		int id;

		logger.getCurrentApplicationLogger().debug("ImageGetter {}", source);

		if (source.equals("step1")) {
			id = R.drawable.step1;
		}
		else if (source.equals("step2")) {
			id = R.drawable.step2;
		}
		else if (source.equals("step3")) {
			id = R.drawable.step3;
		}
		else if (source.equals("step4")) {
			id = R.drawable.step4;
		}
		else if (source.equals("step4a")) {
			id = R.drawable.step4a;
		}
		else {
			return null;
		}

		Drawable d = context.getResources().getDrawable(id);
		d.setBounds(0,0,d.getIntrinsicWidth(),d.getIntrinsicHeight());
		return d;
	}
};
