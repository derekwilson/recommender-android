package net.derekwilson.recommender.receiving;

public enum IntentContentType {
	Unknown,
	ContentUri,
	Url,
	Text,
	FileUri,
	RecommenderNfcBeam
}
