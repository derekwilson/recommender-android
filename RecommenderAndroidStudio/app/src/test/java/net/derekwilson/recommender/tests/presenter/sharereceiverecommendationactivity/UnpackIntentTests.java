package net.derekwilson.recommender.tests.presenter.sharereceiverecommendationactivity;

import android.content.Intent;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.receiving.IIntentContentTypeFinder;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromText;
import net.derekwilson.recommender.receiving.IIntentUnpackerFromUrl;
import net.derekwilson.recommender.receiving.IntentContentType;
import net.derekwilson.recommender.tasks.ITaskResultHandler;
import net.derekwilson.recommender.tasks.TaskErrorResult;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;

import java.util.List;

import rx.Subscription;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UnpackIntentTests extends PresenterTestSetup {
	private Recommendation recommendation;
	private Subscription mockSubscription;
	@Captor
	private ArgumentCaptor<ITaskResultHandler<Recommendation>> intentFromUrlCallbackCaptor;

	@Before
	public void setup() {
		// so we can use generic captors
		MockitoAnnotations.initMocks(this);

		setupPresenter(true);

		recommendation = new Recommendation();
		mockSubscription = mock(Subscription.class);
		// we want to capture the async callback setup call
		when(mockUrlUnpacker.getRecommendationFromUrl(any(Intent.class),intentFromUrlCallbackCaptor.capture())).thenReturn(mockSubscription);
	}

	private ITaskResultHandler<Recommendation> getIntentFromUrlCallback() {
		List<ITaskResultHandler<Recommendation>> capturedCallbacks = intentFromUrlCallbackCaptor.getAllValues();
		assertNotNull("callback handler has not been set by the code under test but we are attempting to call it", capturedCallbacks);
		assertThat("callback handler has not been set by the code under test but we are attempting to call it", capturedCallbacks.size(), is(greaterThan(0)));
		return capturedCallbacks.get(0);
	}

	@Test
	public void exit_if_unknown_type() {
		// arrange
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.Unknown);

		// act
		presenter.unpackIntent(intent);

		// assert
		verify(mockView, times(1)).showMessage(R.string.unknown_intent_type);
		verify(mockView, times(1)).end();
	}

	@Test
	public void unpack_if_text() {
		// arrange
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.Text);
		when(mockTextUnpacker.getRecommendationFromText(intent)).thenReturn(recommendation);

		// act
		presenter.unpackIntent(intent);

		// assert
		verify(mockView, never()).end();
		verify(mockView, times(1)).showRecommendation(recommendation);
	}

	@Test
	public void exit_if_null_text() {
		// arrange
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.Text);
		when(mockTextUnpacker.getRecommendationFromText(intent)).thenReturn(null);

		// act
		presenter.unpackIntent(intent);

		// assert
		verify(mockView, times(1)).showMessage(R.string.error_bad_share_receive);
		verify(mockView, times(1)).end();
	}

	@Test
	public void unpack_if_url() {
		// arrange
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.Url);

		// act
		presenter.unpackIntent(intent);

		// assert
		// this test only checks we start the async processing not that it completes
		verify(mockView, never()).end();
		verify(mockView, times(1)).showProgress(R.string.progress_unpacking_intent);
		verify(mockUrlUnpacker, times(1)).getRecommendationFromUrl(any(Intent.class), any(ITaskResultHandler.class));
	}

	@Test
	public void unpack_if_url_succeeds() {
		// arrange
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.Url);
		presenter.unpackIntent(intent);

		// act
		getIntentFromUrlCallback().onSuccess(recommendation);

		// assert
		verify(mockView, times(1)).showRecommendation(recommendation);
		verify(mockView, times(1)).hideProgress();
	}

	@Test
	public void exit_if_url_fails() {
		// arrange
		Intent intent = new Intent("UNKNOWN");
		when(mockFinder.getTypeOfContent(intent)).thenReturn(IntentContentType.Url);
		presenter.unpackIntent(intent);

		// act
		getIntentFromUrlCallback().onError(new TaskErrorResult(987));

		// assert
		verify(mockView, times(1)).showErrorMessage(any(TaskErrorResult.class));
		verify(mockView, times(1)).hideProgress();
		verify(mockView, times(1)).end();
	}
}
