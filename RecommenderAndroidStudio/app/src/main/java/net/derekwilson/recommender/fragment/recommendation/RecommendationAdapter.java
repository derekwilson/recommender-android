package net.derekwilson.recommender.fragment.recommendation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.model.Recommendation;

import java.util.Collections;
import java.util.List;

import rx.functions.Action1;

final class RecommendationAdapter extends BaseAdapter implements Action1<List<Recommendation>> {
	private final LayoutInflater inflater;

	private List<Recommendation> items = Collections.emptyList();

	private Context context;

	public RecommendationAdapter(Context context) {
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public void call(List<Recommendation> items) {
		this.items = items;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}

	@Override
	public Recommendation getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getView(int position, View rowView, ViewGroup parent) {
		if (rowView == null) {
			rowView = inflater.inflate(R.layout.list_item_recommendation, parent, false);
		}
		ViewHolder holder = new ViewHolder(rowView);
		Recommendation thisRecommendation = items.get(position);

		holder.label.setText(thisRecommendation.getTitle());
		String targetTabname = "";
		switch (thisRecommendation.getType()) {
			case RECOMMENDATION:
				targetTabname = context.getString(R.string.list_title_recommendation);
				break;
			case TRY_THIS:
				targetTabname = context.getString(R.string.list_title_trythis);
				break;
		}
		String label = context.getString(R.string.sub_label_fmt,thisRecommendation.getCategory(),targetTabname);
		holder.subLabel.setText(label);

		return rowView;
	}

	public List<Recommendation> getItems() {
		return this.items;
	}

	static class ViewHolder {
		TextView label;
		TextView subLabel;

		public ViewHolder(View view) {
			label = view.findViewById(R.id.recommendation_row_label);
			subLabel = view.findViewById(R.id.recommendation_row_sub_label);
		}
	}
}
