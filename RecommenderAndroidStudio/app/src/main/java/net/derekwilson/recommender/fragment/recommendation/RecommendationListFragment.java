package net.derekwilson.recommender.fragment.recommendation;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.activity.editRecommendation.EditRecommendationActivity;
import net.derekwilson.recommender.fragment.BaseFragment;
import net.derekwilson.recommender.fragment.IRecommendationClickListener;
import net.derekwilson.recommender.fragment.utility.OkCancelDialogFragment;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.scopes.DaggerIListRecommendationsComponent;
import net.derekwilson.recommender.ioc.scopes.IListRecommendationsComponent;
import net.derekwilson.recommender.ioc.scopes.ListRecommendationsModule;
import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.listrecommendations.IListRecommendationsPresenter;
import net.derekwilson.recommender.presenter.listrecommendations.IListRecommendationsView;
import net.derekwilson.recommender.sharing.IBeamAvailabilityProvider;
import net.derekwilson.recommender.view.EmptyRecyclerView;

import java.util.List;

import javax.inject.Inject;

public class RecommendationListFragment
		extends BaseFragment
		implements IListRecommendationsView, OkCancelDialogFragment.Listener {

	private static final String TAG_DELETE_SELECTED_PROMPT = "delete_selected_tag";
	private static final String RECOMMENDATION_LIST_TYPE = "recommendation_list_type";

	public static RecommendationListFragment getNewFragment(RecommendationType type) {
		RecommendationListFragment fragment = new RecommendationListFragment();
		Bundle args = new Bundle();
		args.putSerializable(RECOMMENDATION_LIST_TYPE, type);
		fragment.setArguments(args);
		return fragment;
	}

	private IListRecommendationsComponent component;

	@Inject
	protected IBeamAvailabilityProvider beamAvailable;

	@Inject
	protected IListRecommendationsPresenter presenter;

	protected ActionMode actionMode = null;
	private RecommendationRecyclerAdapter recommendationsAdapter;
	private RecommendationType recommendationType = RecommendationType.RECOMMENDATION;

	protected NfcAdapter nfcAdapter;

	private IListRecommendationsComponent getComponent() {
		if (component == null) {
			component = DaggerIListRecommendationsComponent.builder()
							.iApplicationComponent(getApplicationComponent(getActivity()))
							.baseActivityModule(new BaseActivityModule(getActivity()))
							.listRecommendationsModule(new ListRecommendationsModule())
							.build();
		}
		return component;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getComponent().inject(this);
		recommendationsAdapter = new RecommendationRecyclerAdapter(presenter, null);

		nfcAdapter = null;
		if (beamAvailable.isBeamAvailable()) {
			nfcAdapter = NfcAdapter.getDefaultAdapter(this.getActivity());
		}

		final Bundle args = getArguments();
		if (args != null) {
			if (args.containsKey(RECOMMENDATION_LIST_TYPE)) {
				recommendationType = (RecommendationType) args.getSerializable(RECOMMENDATION_LIST_TYPE);
			}
		}
		logger.getCurrentApplicationLogger().debug("RecommendationListFragment {} {} onCreate", getTag(), recommendationType);
		// its best to wait until the activity is fully initialised before signalling to the presenter
		presenter.bindView(this);
	}

	protected EmptyRecyclerView rvRecommendations;
	protected LinearLayout layoutNoData;
	protected TextView textNoData;

	private void bindControls(View view) {
		rvRecommendations = view.findViewById(R.id.rvRecommendations);
		layoutNoData = view.findViewById(R.id.layNoData);
		textNoData = view.findViewById(R.id.txtNoData);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		logger.getCurrentApplicationLogger().debug("RecommendationListFragment {} {} onCreateView", getTag(), recommendationType);
		View view = inflater.inflate(R.layout.fragment_recommendation_edit_list, container, false);
		bindControls(view);
		presenter.onCreate();
		return view;
	}

	private void setActionMode(ActionMode actionMode) {
		this.actionMode = actionMode;
	}

	private ActionMode.Callback getMultiSelectActionMode() {
		return new ActionMode.Callback() {
			@Override
			public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
				setActionMode(actionMode);
				menu.findItem(R.id.action_beamRecommendation).setVisible(nfcAdapter!=null).setEnabled(nfcAdapter!=null);
				recommendationsAdapter.setSelectable(true);
				return false;
			}

			@Override
			public void onDestroyActionMode(ActionMode actionMode) {
				setActionMode(null);
				recommendationsAdapter.setSelectable(false);
				recommendationsAdapter.clearSelections();
			}

			@Override
			public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
				actionMode.getMenuInflater().inflate(R.menu.action_list_recommendations_multi_select, menu);
				return true;
			}

			@Override
			public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
				switch (menuItem.getItemId()) {
					case R.id.action_deleteRecommendation:
						promptDeleteRecommendations();
						return true;
					case R.id.action_moveRecommendation:
						presenter.moveRecommendations();
						actionMode.finish();
						return true;
					case R.id.action_shareRecommendation:
						presenter.shareRecommendations();
						actionMode.finish();
						return true;
					case R.id.action_beamRecommendation:
						presenter.beamRecommendations();
						actionMode.finish();
						return true;
				}
				return false;
			}
		};
	}

	private void promptDeleteRecommendations() {
		DialogFragment newFragment = OkCancelDialogFragment.newInstance(
				getString(R.string.delete_message),
				getString(R.string.ok_cancel_yes),
				getString(R.string.ok_cancel_no)
		);
		newFragment.show(getActivity().getSupportFragmentManager(), TAG_DELETE_SELECTED_PROMPT);
	}

	@Override
	public void ok(String tag) {
		logger.getCurrentApplicationLogger().debug("RecommendationListFragment {} {} OK ({})", getTag(), recommendationType, tag);
		presenter.deleteRecommendations();
		if (actionMode != null) {
			actionMode.finish();
		}
	}

	@Override
	public void cancel(String tag) {
		logger.getCurrentApplicationLogger().debug("RecommendationListFragment {} {} cancel ({})", getTag(), recommendationType, tag);
		if (actionMode != null) {
			actionMode.finish();
		}
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		logger.getCurrentApplicationLogger().debug("RecommendationListFragment {} {} onViewCreated", getTag(), recommendationType);

		recommendationsAdapter.setClickListener(new IRecommendationClickListener() {
			@Override
			public void onRecommendationClicked(Recommendation selected) {
				logger.getCurrentApplicationLogger().debug("{} clicked on recommendation {}, {}", recommendationType, selected.getId(), selected.getName());
				Intent intent = EditRecommendationActivity.getStartIntent(getActivity(), selected.getType(), selected);
				startActivity(intent);
			}

			@Override
			public void onRecommendationLongClicked(Recommendation selected) {
				logger.getCurrentApplicationLogger().debug("{} long clicked on recommendation {}, {}", recommendationType, selected.getId(), selected.getName());
				AppCompatActivity activity = (AppCompatActivity)getActivity();
				activity.startSupportActionMode(getMultiSelectActionMode());
			}

			@Override
			public void onRecommendationSelectionChanged(Recommendation selected) {
				if (selected != null) {
					logger.getCurrentApplicationLogger().debug("{} selection changed on recommendation {}, {}", recommendationType, selected.getId(), selected.getName());
				} else {
					logger.getCurrentApplicationLogger().debug("{} selection changed on recommendation", recommendationType);
				}
				AppCompatActivity activity = (AppCompatActivity)getActivity();
				int numberSelected = recommendationsAdapter.getSelectedItemCount();
				setSelectedTitle(numberSelected);
			}
		});
		rvRecommendations.setEmptyView(layoutNoData);
		rvRecommendations.setLayoutManager(new LinearLayoutManager(rvRecommendations.getContext()));
		rvRecommendations.addItemDecoration(new DividerItemDecoration(rvRecommendations.getContext(), DividerItemDecoration.VERTICAL));
		rvRecommendations.setAdapter(recommendationsAdapter);
	}

	@Override
	public void onPause() {
		logger.getCurrentApplicationLogger().debug("RecommendationListFragment {} {} onPause", getTag(), recommendationType);
		super.onPause();
	}

	@Override
	public void onResume() {
		boolean neededInject = false;
		super.onResume();
		if (!isInjected()) {
			getComponent().inject(this);
			neededInject = true;
		}
		logger.getCurrentApplicationLogger().debug("RecommendationListFragment {} {} onResume, Inject {}", getTag(), recommendationType, neededInject);
	}

	@Override
	public void onDestroy() {
		logger.getCurrentApplicationLogger().debug("RecommendationListFragment {} {} onDestroy", getTag(), recommendationType);
		presenter.onDestroy();
		super.onDestroy();
	}

	private void setSelectedTitle(int numberSelected) {
		if (actionMode != null) {
			actionMode.setTitle(getString(R.string.multi_select_action_mode_title, numberSelected));
		}
	}

	@Override
	public void end() {
		getActivity().finish();
	}

	@Override
	public void showError(Throwable throwable) {
		displayMessage(throwable.getMessage());
	}

	@Override
	public void showMessage(int messageId) {
		displayMessage(getString(messageId));
	}

	@Override
	public void setRecommendations(List<Recommendation> recommendations) {
		recommendationsAdapter.call(recommendations);
	}

	@Override
	public RecommendationType getRecommendationType() {
		return recommendationType;
	}

	@Override
	public void setFilter(IFilterCollection currentItems) {
		if (presenter != null) {
			presenter.setFilter(currentItems);
		}
	}

	@Override
	public void setNonFilteredEmptyText() {
		textNoData.setText(R.string.no_recommendations_tabbed_text1);
	}

	@Override
	public void setFilteredEmptyText() {
		textNoData.setText(R.string.no_recommendations_tabbed_filtered_text1);
	}

	@Override
	public int getItemCount() {
		return recommendationsAdapter.getItemCount();
	}

	@Override
	public List<Recommendation> copySelectedRecommendations() {
		// this will copy the recommendations from the list
		List<Recommendation> recommendations = recommendationsAdapter.getSelectedRecommendations();
		return recommendations;
	}

	@Override
	public String getNicknameKey() {
		return getString(R.string.key_nickname);
	}

	@Override
	public String getSortByKey() {
		return getString(R.string.key_sortby);
	}
	@Override
	public void startIntent(Intent intent, int promptId) {
		startActivity(Intent.createChooser(intent, getString(promptId)));
	}

	@Override
	public void sendNfcMessage(NdefMessage message) {
		nfcAdapter.setNdefPushMessage(message, this.getActivity());
	}
}
