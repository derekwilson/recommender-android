package net.derekwilson.recommender.receiving;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.rest.wikipedia.IQueryResultToRecommendationConverter;
import net.derekwilson.recommender.rest.wikipedia.IQueryService;
import net.derekwilson.recommender.rest.wikipedia.model.QueryResult;
import net.derekwilson.recommender.tasks.ITaskErrorResult;
import net.derekwilson.recommender.tasks.ITaskResultHandler;

import java.net.URL;

import javax.inject.Inject;

import rx.Subscription;

public class AsyncConvertWikipediaToRecommendation implements IAsyncConvertUrlToRecommendation {

	private IQueryService queryWikipediaService;
	private IQueryResultToRecommendationConverter wikipediaConverter;

	@Inject
	public AsyncConvertWikipediaToRecommendation(IQueryService queryWikipediaService, IQueryResultToRecommendationConverter wikipediaConverter) {
		this.queryWikipediaService = queryWikipediaService;
		this.wikipediaConverter = wikipediaConverter;
	}

	@Override
	public Subscription getFromAsyncService(final URL url, final ITaskResultHandler<Recommendation> handler) {
		// if the url is from wikipedia then we can call the api to get json like so
		// https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exchars=200&explaintext=&titles=Brazil_(1985_film)
		// where Brazil_(1985_film) is the end of the url - after the final slash
		String title = url.getPath();
		int pos = title.lastIndexOf("/");
		if (pos >=0) {
			title = title.substring(pos + 1);
		}

		Subscription subscription = queryWikipediaService.getQueryResult(title, new ITaskResultHandler<QueryResult>() {
			@Override
			public void onSuccess(QueryResult result) {
				// maybe the conversion should be done in the rxandroid pipeline
				Recommendation recommendation = wikipediaConverter.convert(url, result);
				handler.onSuccess(recommendation);
			}

			@Override
			public void onError(ITaskErrorResult error) {
				handler.onError(error);
			}
		});
		return subscription;
	}
}
