package net.derekwilson.recommender.tests.html.documentconverer;

import net.derekwilson.recommender.model.Recommendation;

import org.junit.Test;

import java.io.IOException;
import java.net.URL;

import static junit.framework.Assert.assertNull;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Tests for the converter when it does not recognise the source of the URL
 */
public class DefaultConverterTests extends BaseHtmlConverterTests {
	private static String UNMATCHED_URL = "http://www.unmatched.com/somepath";
	private static String AMAZON_1_FILENAME = "test_amazon_1.html";

	private URL testUrl;

	@Test
	public void gets_default_name() throws IOException {
		// arrange
		testUrl = new URL(UNMATCHED_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getName(), is("Kate Rusby - Sweet Bells - Amazon.com Music"));
	}

	@Test
	public void gets_default_by() throws IOException {
		// arrange
		testUrl = new URL(UNMATCHED_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertNull(recommendation.getBy());
	}

	@Test
	public void gets_default_category() throws IOException {
		// arrange
		testUrl = new URL(UNMATCHED_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertNull(recommendation.getCategory());
	}

	@Test
	public void gets_default_notes() throws IOException {
		// arrange
		testUrl = new URL(UNMATCHED_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertNull(recommendation.getNotes());
	}

	@Test
	public void gets_default_url() throws IOException {
		// arrange
		testUrl = new URL(UNMATCHED_URL);
		testDocument = loadHtml(AMAZON_1_FILENAME);

		// act
		Recommendation recommendation = converter.convert(testUrl, testDocument);

		// assert
		assertThat(recommendation.getUri(), is(UNMATCHED_URL));
	}
}
