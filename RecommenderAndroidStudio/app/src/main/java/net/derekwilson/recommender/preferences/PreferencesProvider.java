package net.derekwilson.recommender.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.logging.ILoggerFactory;

import javax.inject.Inject;

public class PreferencesProvider implements IPreferencesProvider {
	private Context context;
	private SharedPreferences preferences;
	private ICrashReporter crashReporter;
	private ILoggerFactory loggerFactory;

	@Inject
	public PreferencesProvider(Context context, SharedPreferences preferences, ICrashReporter crashReported, ILoggerFactory loggerFactory) {
		this.context = context;
		this.preferences = preferences;
		this.crashReporter = crashReported;
		this.loggerFactory = loggerFactory;
	}

	@Override
	public String getPreferenceString(String key) {
		return preferences.getString(key, null);
	}

	@Override
	public void setPreferenceString(String key, String value) {
		preferences.edit()
				.putString(key, value)
				.commit();
	}

	@Override
	public IRecommendationRepository.SortBy getPreferencesSortOrder(String key) {
		String stringValue = getPreferenceString(key);
		if (stringValue == null || stringValue.isEmpty()) {
			return IRecommendationRepository.SortBy.NAME;
		}
		try {
			return Enum.valueOf(IRecommendationRepository.SortBy.class, stringValue);
		} catch (Exception ex) {
			loggerFactory.getCurrentApplicationLogger().warn("cannot convert sort by enum: " + stringValue, ex);
			crashReporter.logNonFatalException(ex);
			return IRecommendationRepository.SortBy.NAME;
		}
	}
}
