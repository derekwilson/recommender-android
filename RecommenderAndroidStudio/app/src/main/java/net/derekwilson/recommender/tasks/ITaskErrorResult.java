package net.derekwilson.recommender.tasks;

import android.content.Context;

public interface ITaskErrorResult {
	int getCode();
	String getMessage(Context context);
}
