package net.derekwilson.recommender.file;

import android.content.Context;

public interface IAssetFile {
	String getFileContents(Context context, String filename);
}
