package net.derekwilson.recommender;

import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;

import org.slf4j.Logger;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BaseTests {
	protected ILoggerFactory mockLoggerFactory = mock(ILoggerFactory.class);
	private Logger logger = mock(Logger.class);

	public BaseTests() {
		when(mockLoggerFactory.getCurrentApplicationLogger()).thenReturn(logger);
	}

	protected Recommendation setupRecommendation(String name, String by, String category, String notes, String uri, String source) {
		Recommendation recommendation = new Recommendation();
		recommendation.setType(RecommendationType.RECOMMENDATION);
		recommendation.setName(name);
		recommendation.setBy(by);
		recommendation.setCategory(category);
		recommendation.setNotes(notes);
		recommendation.setUri(uri);
		recommendation.setSource(source);
		return recommendation;
	}
}
