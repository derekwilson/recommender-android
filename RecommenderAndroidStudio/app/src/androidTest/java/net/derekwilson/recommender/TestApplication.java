package net.derekwilson.recommender;

import org.slf4j.LoggerFactory;

public class TestApplication extends AndroidApplication {
	@Override
	protected void initialiseDatabase() {
		// we dont want to create the default DB for the tests its just not useful
		LoggerFactory.getLogger(AndroidApplication.class).debug("database initialisation suppressed");
	}
}
