package net.derekwilson.recommender.sharing;

import android.content.Intent;
import android.net.Uri;

import net.derekwilson.recommender.exceptions.ExternalStorageException;
import net.derekwilson.recommender.model.Recommendation;

import java.util.List;

public interface IIntentSharer extends ISharer {
	IntentShareResult getIntent(List<Recommendation> recommendations) throws ExternalStorageException;
	IntentShareResult getIntent(Recommendation recommendation) throws ExternalStorageException;
	IntentShareResult getIntent(String filename);
}
