package net.derekwilson.recommender.tests.receiving.intentunpackerfromtext;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.file.ITextFileWriter;
import net.derekwilson.recommender.receiving.IntentUnpackerFromText;
import net.derekwilson.recommender.tests.BaseJunitTests;

import org.junit.Before;

import static org.mockito.Mockito.mock;

public class BaseUnpackerTests extends BaseJunitTests {
	// mocks
	protected ITextFileWriter mockWriter = mock(ITextFileWriter.class);
	protected ICrashReporter mockReporter = mock(ICrashReporter.class);

	// object under test
	protected IntentUnpackerFromText unpacker;

	@Before
	public void setup() {
		unpacker = new IntentUnpackerFromText(mockWriter, mockReporter);
	}
}
