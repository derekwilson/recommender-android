package net.derekwilson.recommender.sharing;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import net.derekwilson.recommender.logging.ILoggerFactory;

import javax.inject.Inject;

public class BeamAvailabilityProvider implements IBeamAvailabilityProvider {

	private ILoggerFactory logger;
	private Context context;

	@Inject
	public BeamAvailabilityProvider(ILoggerFactory logger, Context context) {
		this.logger = logger;
		this.context = context;
	}

	@Override
	public boolean isBeamAvailable() {
		if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
			// NFC isn't available on the device
			logger.getCurrentApplicationLogger().debug("NFC not supported by device");
			return false;
		} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
			// the NFC API we need is not available
			logger.getCurrentApplicationLogger().debug("NFC not supported by OS");
			return false;
		}
		logger.getCurrentApplicationLogger().debug("NFC available");
		return true;
	}
}
