package net.derekwilson.recommender.tests.model.filtercollection;

import net.derekwilson.recommender.model.FilterSelectionItem;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class IsFilterSelectedTests extends TestSetup {

	private void verifySelectedState(boolean isSelected) {
		assertThat(collection.isFilterSelected("NOFILTER"), is(false));
		assertThat(collection.isFilterSelected(0), is(false));

		assertThat(collection.isFilterSelected("FILTER 1"), is(isSelected));
		assertThat(collection.isFilterSelected(1), is(isSelected));

		assertThat(collection.isFilterSelected("FILTER 2"), is(false));
		assertThat(collection.isFilterSelected(2), is(false));

		assertThat(collection.isFilterSelected("FILTER 3"), is(false));
		assertThat(collection.isFilterSelected(3), is(false));
	}

	@Test
	public void empty_collection_is_not_selected() {
		// arrange

		// act

		// assert
		verifySelectedState(false);
	}

	@Test
	public void populated_collection_is_not_selected() {
		// arrange
		collection.setFilters(getFilters(true,true));

		// act
		testItem1.setSelected(false);

		// assert
		verifySelectedState(false);
	}

	@Test
	public void populated_collection_is_selected() {
		// arrange
		collection.setFilters(getFilters(true, true));

		// act
		testItem1.setSelected(true);

		// assert
		verifySelectedState(true);
	}


}
