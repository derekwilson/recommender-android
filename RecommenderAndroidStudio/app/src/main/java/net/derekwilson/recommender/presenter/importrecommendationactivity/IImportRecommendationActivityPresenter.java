package net.derekwilson.recommender.presenter.importrecommendationactivity;

import android.content.ContentResolver;
import android.content.Intent;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.IPresenter;

import java.util.List;

public interface IImportRecommendationActivityPresenter extends IPresenter<IImportRecommendationActivityView> {
	List<Recommendation> unpackIntent(Intent intent, ContentResolver contentResolver);

	void importRecommendations(List<Recommendation> recommendations);
}
