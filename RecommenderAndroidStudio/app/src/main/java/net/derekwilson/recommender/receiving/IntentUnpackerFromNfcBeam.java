package net.derekwilson.recommender.receiving;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Parcelable;

import net.derekwilson.recommender.json.IImporter;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;

import java.util.List;

import javax.inject.Inject;

public class IntentUnpackerFromNfcBeam implements IIntentUnpackerFromNfcBeam {

	private ILoggerFactory logger;
	private IImporter importer;

	@Inject
	public IntentUnpackerFromNfcBeam(ILoggerFactory logger, IImporter importer) {
		this.logger = logger;
		this.importer = importer;
	}

	@Override
	public List<Recommendation> getRecommendationsFromPayload(Intent intent) {
		Parcelable[] raw = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
		// we only send one message
		NdefMessage message = (NdefMessage) raw[0];
		String payload = new String(message.getRecords()[0].getPayload());
		return importer.importRecommendationsFromString(payload);
	}
}
