package net.derekwilson.recommender.html;

import net.derekwilson.recommender.crashes.ICrashReporter;
import net.derekwilson.recommender.exceptions.UnknownHtmlShareException;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.model.Recommendation;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.net.URL;

import javax.inject.Inject;

public class DocumentToRecommendationConverter implements IDocumentToRecommendationConverter {
	private ILoggerFactory logger;
	private ICrashReporter crashReporter;

	@Inject
	public DocumentToRecommendationConverter(ILoggerFactory logger, ICrashReporter crashReporter) {
		this.logger = logger;
		this.crashReporter = crashReporter;
	}

	private boolean isAmazon(URL url) {
		if (url.getHost().toLowerCase().contains("amazon.com")) {
			return true;
		}
		if (url.getHost().toLowerCase().contains("amazon.co.uk")) {
			return true;
		}
		return false;
	}

	private boolean isGoodreads(URL url) {
		if (url.getHost().toLowerCase().contains("goodreads.com")) {
			return true;
		}
		return false;
	}

	private boolean convertTitleSpan(Recommendation recommendation, Document document) {
		Element product = document.select("span#productTitle").first();
		if (product != null) {
			recommendation.setName(product.text());
			return true;
		}
		return false;
	}

	private boolean convertAmazonTitleWithSeparators(String seperator, Recommendation recommendation, String title, boolean foundAuthor) {
		String parts[] = title.split(seperator);
		Boolean retval = false;
		if (parts.length > 1 && parts[1] != null) {
			if (parts[0].toLowerCase().contains("amazon.co")) {
				recommendation.setName(parts[1].trim());
				retval = true;
			}
			if (!retval && parts[1].toLowerCase().contains("amazon.co")) {
				recommendation.setName(parts[0].trim());
				retval = true;
			}
			if (!retval && parts.length > 3 && parts[3] != null) {
				if (parts[3].toLowerCase().contains("amazon.co")) {
					recommendation.setName(parts[0].trim());
					if (!foundAuthor) {
						// dont overwrite any author we have already found
						recommendation.setBy(parts[2].trim());
						foundAuthor = true;
					}
					retval = true;
				}
			}
			if (!foundAuthor) {
				// dont overwrite any author we have already found
				if (parts.length > 2) {
					String by = parts[parts.length-2];
					if (by != null) {
						recommendation.setBy(by.trim());
					}
				} else {
					recommendation.setBy(parts[0].trim());
				}
			}
			if (!retval && parts.length > 1) {
				recommendation.setName(parts[1].trim());
				retval = true;
			}
		}
		return retval;
	}

	private boolean convertAmazonTitleWithDashes(Recommendation recommendation, String title, boolean foundAuthor) {
		return convertAmazonTitleWithSeparators(" - ", recommendation, title, foundAuthor);
	}

	private boolean convertAmazonTitleWithColons(Recommendation recommendation, String title, boolean foundAuthor) {
		return convertAmazonTitleWithSeparators(":", recommendation, title, foundAuthor);
	}

	private boolean convertAmazonTitleDefault(Recommendation recommendation, String title) {
		recommendation.setName(title);
		return true;
	}

	private boolean convertAmazonTitleTrimmed(Recommendation recommendation, String title) {
		int pos = title.indexOf(':');
		if (pos >= 0) {
			recommendation.setName(title.substring(0, pos));
			return true;
		}
		recommendation.setName(title);
		return true;
	}

	private String getFirstElementText(Element parentNode, String cssSelector) {
		Element element = parentNode.select(cssSelector).first();
		if (element != null) {
			return element.text();
		}
		return null;
	}

	private boolean convertAmazonBylineToAuthor1(Recommendation recommendation, Document document) {
		String by = getFirstElementText(document, "div#byline span.author a.contributorNameID");
		if (by != null) {
			recommendation.setBy(by);
			return true;
		}
		return false;
	}

	private boolean convertAmazonBylineToAuthor2(Recommendation recommendation, Document document) {
		String by = getFirstElementText(document, "div#byline span.author a.a-link-normal");
		if (by != null) {
			recommendation.setBy(by);
			return true;
		}
		return false;
	}

	private boolean convertAmazonBylineToAuthor3(Recommendation recommendation, Document document) {
		String by = getFirstElementText(document, "div#byline");
		if (by != null) {
			recommendation.setBy(by);
			return true;
		}
		return false;
	}

	private boolean convertReviewText1(Recommendation recommendation, String title, Document document) {
		Element recDiv = document.select("div#revMHRL").first();
		if (recDiv != null) {
			String reviewText = getFirstElementText(recDiv, "div.a-section div.a-section div.a-section");
			if (reviewText != null){
				recommendation.setNotes(title + ". " + reviewText);
				return true;
			}
			else {
				logger.getCurrentApplicationLogger().debug("cannot find recommendations text");
			}
		}
		else {
			logger.getCurrentApplicationLogger().debug("cannot find recommendations div");
		}
		return false;
	}

	private boolean convertReviewText2(Recommendation recommendation, String title, Document document) {
		String reviewText = getFirstElementText(document, "span.reviewText");
		if (reviewText != null) {
			recommendation.setNotes(title + ". " + reviewText);
			return true;
		}
		else {
			logger.getCurrentApplicationLogger().debug("cannot find review text");
		}
		return false;
	}

	private Recommendation convertAmazonDocument(URL url, Document document) {
		Recommendation recommendation = new Recommendation();
		String title = document.title();

		boolean foundAuthor = convertAmazonBylineToAuthor1(recommendation, document);
		if (!foundAuthor) {
			foundAuthor = convertAmazonBylineToAuthor2(recommendation, document);
		}
		if (!foundAuthor) {
			foundAuthor = convertAmazonBylineToAuthor3(recommendation, document);
		}
		if (!convertTitleSpan(recommendation, document)) {
			if (!convertAmazonTitleWithDashes(recommendation, title, foundAuthor)) {
				if (!convertAmazonTitleWithColons(recommendation, title, foundAuthor)) {
					// give up
					convertAmazonTitleDefault(recommendation, title);
				}
			}
		}
		int pos = title.lastIndexOf(' ');
		if (pos >= 0) {
			recommendation.setCategory(title.substring(pos + 1));
		}
		if (recommendation.getBy() != null) {
			// if we found an author - lets assume its a book
			recommendation.setCategory("Book");
		} else {
			crashReporter.logNonFatalException(new UnknownHtmlShareException("no author", url.toString()));
		}

		if (!convertReviewText1(recommendation, title, document)) {
			convertReviewText2(recommendation, title, document);
		}

		if (recommendation.getNotes() == null) {
			recommendation.setNotes(title);
		}

		recommendation.setUri(url.toString());
		return recommendation;
	}

	private Recommendation convertGoodreadsDocument(URL url, Document document) {
		Recommendation recommendation = new Recommendation();
		String title = document.title();

		if (!convertGoodreadsTitle(recommendation, title)) {
			crashReporter.logNonFatalException(new UnknownHtmlShareException("no title", url.toString()));
		}
		if (!convertGoodreadsDescription(recommendation, document)) {
			crashReporter.logNonFatalException(new UnknownHtmlShareException("no description", url.toString()));
		}

		recommendation.setCategory("Book");
		recommendation.setUri(url.toString());
		return recommendation;
	}

	private boolean convertGoodreadsDescription(Recommendation recommendation, Document document) {
		String txt = getFirstElementText(document, "div[data-testid=description]");
		if (txt != null) {
			recommendation.setNotes(txt);
			return true;
		}
		return false;
	}

	private boolean convertGoodreadsTitle(Recommendation recommendation, String title) {
		int position = title.indexOf(" by");
		if (position >= 0) {
			recommendation.setName(title.substring(0, position).trim());
			String byCondidate = title.substring(position+3);
			int endPosition = byCondidate.indexOf("| Goodreads");
			if (endPosition == -1) {
				endPosition = byCondidate.indexOf("Goodreads");
			}
			if (endPosition == -1) {
				recommendation.setBy(byCondidate.trim());
			} else {
				recommendation.setBy(byCondidate.substring(0, endPosition).trim());
			}
			return true;
		}
		return false;
	}

	@Override
	public Recommendation convert(URL url, Document document) {
		if (isAmazon(url)) {
			return convertAmazonDocument(url, document);
		}
		if (isGoodreads(url)) {
			return convertGoodreadsDocument(url, document);
		}

		// catch all
		Recommendation recommendation = new Recommendation();
		recommendation.setName(document.title());
		recommendation.setUri(url.toString());
		crashReporter.logNonFatalException(new UnknownHtmlShareException("default handler", url.toString()));
		return recommendation;
	}
}
