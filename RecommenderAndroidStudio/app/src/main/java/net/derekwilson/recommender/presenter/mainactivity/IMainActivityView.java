package net.derekwilson.recommender.presenter.mainactivity;

import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.IView;

public interface IMainActivityView extends IView {
	/**
	 * create any menu items needed for the filters
	 * @param currentFilter current collection of filters
	 */
	void showCategoryFilter(IFilterCollection currentFilter);

	/**
	 * this method is only needed at the moment as the UI for the drawer
	 * wants to handle the selection toggle itself and it want to only allow one
	 * item to be selected
	 * we want multiple select so we overwrite the state from the current collection
	 * @param currentFilter
	 */
	void setCategoryFilterUiState(IFilterCollection currentFilter);

	/**
	 * apply the filters to the lists
	 * @param currentFilter
	 */
	void applyFilterToAllTabs(IFilterCollection currentFilter);

	int getActiveTab();

	void setActiveTab(int tabIndex);

	void updateTabTitles(int tabIndex);
}
