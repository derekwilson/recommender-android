package net.derekwilson.recommender.wrapper;

import android.text.TextUtils;

import javax.inject.Inject;

public class TextUtilsWrapper implements ITextUtils {

	@Inject
	public TextUtilsWrapper() {
	}

	@Override
	public boolean isEmpty(CharSequence str) {
		return TextUtils.isEmpty(str);
	}

}
