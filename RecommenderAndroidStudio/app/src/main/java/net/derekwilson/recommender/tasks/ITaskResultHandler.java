package net.derekwilson.recommender.tasks;

public interface ITaskResultHandler<T> {
	void onSuccess(T result);
	void onError(ITaskErrorResult error);
}
