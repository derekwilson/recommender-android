package net.derekwilson.recommender.rest.wikipedia.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
		"from",
		"to"
})
public class Normalized {

	@JsonProperty("from")
	private String from;
	@JsonProperty("to")
	private String to;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 *
	 * @return
	 * The from
	 */
	@JsonProperty("from")
	public String getFrom() {
		return from;
	}

	/**
	 *
	 * @param from
	 * The from
	 */
	@JsonProperty("from")
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 *
	 * @return
	 * The to
	 */
	@JsonProperty("to")
	public String getTo() {
		return to;
	}

	/**
	 *
	 * @param to
	 * The to
	 */
	@JsonProperty("to")
	public void setTo(String to) {
		this.to = to;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
