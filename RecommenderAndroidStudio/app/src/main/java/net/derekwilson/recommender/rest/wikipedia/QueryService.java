package net.derekwilson.recommender.rest.wikipedia;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.rest.wikipedia.model.QueryResult;
import net.derekwilson.recommender.tasks.ITaskResultHandler;
import net.derekwilson.recommender.tasks.TaskErrorResult;

import javax.inject.Inject;

import retrofit.RestAdapter;
import retrofit.converter.Converter;
import retrofit.converter.JacksonConverter;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class QueryService implements IQueryService {
	private static String ENDPOINT = "https://en.wikipedia.org/";

	private ILoggerFactory logger;
	private ObjectMapper objectMapper;
	private Converter jacksonConverter;

	@Inject
	public QueryService(ILoggerFactory logger, ObjectMapper objectMapper, Converter jacksonConverter) {
		this.logger = logger;
		this.objectMapper = objectMapper;
		this.jacksonConverter = jacksonConverter;
	}

	private Observable<QueryResult> query(String title) {
		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(ENDPOINT)
				.setConverter(jacksonConverter)
				.build();
		IWikipediaService service = restAdapter.create(IWikipediaService.class);
		Observable<QueryResult> wikipediaAction = service.action("query", "extracts", "json", 500, 1, title);

		return wikipediaAction;
	}

	public Subscription getQueryResult(final String title, final ITaskResultHandler<QueryResult> handler) {
		Observable<QueryResult> query = query(title)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread());

		return query.subscribe(new Subscriber<QueryResult>() {
			@Override
			public void onCompleted() {
			}

			@Override
			public void onError(Throwable e) {
				logger.getCurrentApplicationLogger().warn("wikipedia error from {}", title, e);
				handler.onError(new TaskErrorResult(TaskErrorResult.INTENT_UNPACK_BAD_WIKIPEDIA_READ));
			}

			@Override
			public void onNext(QueryResult queryResult) {
				logger.getCurrentApplicationLogger().debug("wikipedia query onNext from {}", title);
				handler.onSuccess(queryResult);
			}
		});
	}
}
