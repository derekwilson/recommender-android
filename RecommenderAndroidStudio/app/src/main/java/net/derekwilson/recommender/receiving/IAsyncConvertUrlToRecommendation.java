package net.derekwilson.recommender.receiving;

import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.tasks.ITaskResultHandler;

import java.net.URL;

import rx.Subscription;

public interface IAsyncConvertUrlToRecommendation {
	Subscription getFromAsyncService(URL url, ITaskResultHandler<Recommendation> handler);
}

