package net.derekwilson.recommender.tests.presenter.editrecommendation;

import android.content.Intent;

import net.derekwilson.recommender.R;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class BrowseToUriTests extends PresenterTestsSetup {
	@Before
	public void setUp() {
		super.setUp();
		setupPresenter(true, false);
	}

	@Test
	public void does_not_browse_to_invalid_uri() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("","","","","uri","");

		// act
		presenter.browseToUri();

		// assert
		verify(mockActivity, never()).startActivity((Intent) any());
		verify(mockView, times(1)).showMessage(R.string.error_browse_url);
	}

	@Test
	public void does_browse_to_uri() {
		// arrange
		recommendationPassedToActivity = null;
		recommendationFromUi = setupRecommendation("","","","","http://test.com","");

		// act
		presenter.browseToUri();

		// assert
		verify(mockActivity, times(1)).startActivity((Intent) any());
	}
}
