package net.derekwilson.recommender.presenter;

public interface IPresenter<V> {
	void onCreate();
	void onDestroy();
	void bindView(V view);
	void unbindView();
}