package net.derekwilson.recommender.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.google.android.material.tabs.TabLayout;

public class LockableTabLayout extends TabLayout {
	private boolean isTabSwitchEnabled;

	public LockableTabLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.isTabSwitchEnabled = true;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if (this.isTabSwitchEnabled) {
			return super.onInterceptTouchEvent(event);
		} else {
			return true;
		}
	}

	public void setTabSwitchEnabled(boolean isSwipeEnabled) {
		this.isTabSwitchEnabled = isSwipeEnabled;
	}
}

