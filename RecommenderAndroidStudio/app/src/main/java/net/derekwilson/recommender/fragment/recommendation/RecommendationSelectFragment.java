package net.derekwilson.recommender.fragment.recommendation;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.Nullable;

import net.derekwilson.recommender.R;
import net.derekwilson.recommender.fragment.BaseFragment;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.ioc.scopes.DaggerISelectRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.ISelectRecommendationComponent;
import net.derekwilson.recommender.ioc.scopes.SelectRecommendationModule;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.selectrecommendation.ISelectRecommendationPresenter;
import net.derekwilson.recommender.presenter.selectrecommendation.ISelectRecommendationView;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

public class RecommendationSelectFragment extends BaseFragment implements ISelectRecommendationView {
	private static final String RECOMMENDATION_LIST = "recommendation_list";

	private ISelectRecommendationComponent component;

	@Inject
	protected ISelectRecommendationPresenter presenter;

	private List<Recommendation> recommendations;
	private RecommendationAdapter adapter;

	public void setArguments(List<Recommendation> recommendations) {
		if (recommendations == null) {
			return;
		}
		Bundle args = new Bundle();
		Recommendation[] array = new Recommendation[recommendations.size()];
		recommendations.toArray(array);
		args.putParcelableArray(RECOMMENDATION_LIST, array);
		setArguments(args);
	}

	private ISelectRecommendationComponent getComponent() {
		if (component == null) {
			component = DaggerISelectRecommendationComponent.builder()
							.iApplicationComponent(getApplicationComponent(getActivity()))
							.baseActivityModule(new BaseActivityModule(getActivity()))
							.selectRecommendationModule(new SelectRecommendationModule())
							.build();
		}
		return component;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getComponent().inject(this);

		final Bundle args = getArguments();
		if (args != null) {
			if (args.containsKey(RECOMMENDATION_LIST)) {
				Recommendation[] recommendationsArray = (Recommendation[]) args.getParcelableArray(RECOMMENDATION_LIST);
				if (recommendationsArray != null) {
					recommendations = Arrays.asList(recommendationsArray);
				}
			}
		}
		presenter.bindView(this);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		adapter = new RecommendationAdapter(context);
	}

	protected ListView lvRecommendations;
	protected LinearLayout layoutNoData;
	private void bindControls(View view) {
		lvRecommendations = view.findViewById(R.id.lvRecommendations);
		layoutNoData = view.findViewById(R.id.layNoData);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_recommendation_select_list, container, false);
		bindControls(view);
		presenter.onCreate();
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		lvRecommendations.setEmptyView(layoutNoData);
		lvRecommendations.setAdapter(adapter);
		adapter.call(recommendations);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isInjected()) {
			getComponent().inject(this);
		}
	}

	@Override
	public void onDestroy() {
		presenter.onDestroy();
		super.onDestroy();
	}

	@Override
	public void end() {
		getActivity().finish();
	}

	@Override
	public void showError(Throwable throwable) {
		displayMessage(throwable.getMessage());
	}

	@Override
	public void showMessage(int message) {
		displayMessage(getString(message));
	}

	@Override
	public List<Recommendation> getSelected() {
		return adapter.getItems();
	}
}
