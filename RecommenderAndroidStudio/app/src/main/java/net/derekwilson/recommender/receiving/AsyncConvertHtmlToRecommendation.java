package net.derekwilson.recommender.receiving;

import net.derekwilson.recommender.BuildConfig;
import net.derekwilson.recommender.file.ITextFileWriter;
import net.derekwilson.recommender.html.IDocumentDownloader;
import net.derekwilson.recommender.html.IDocumentToRecommendationConverter;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.tasks.ITaskErrorResult;
import net.derekwilson.recommender.tasks.ITaskResultHandler;

import org.jsoup.nodes.Document;

import java.net.URL;

import javax.inject.Inject;

import rx.Subscription;

public class AsyncConvertHtmlToRecommendation implements IAsyncConvertUrlToRecommendation {

	private ITextFileWriter textFileWriter;
	private IDocumentDownloader documentDownloader;
	private IDocumentToRecommendationConverter htmlConverter;

	@Inject
	public AsyncConvertHtmlToRecommendation(ITextFileWriter textFileWriter, IDocumentDownloader documentDownloader, IDocumentToRecommendationConverter htmlConverter) {
		this.textFileWriter = textFileWriter;
		this.documentDownloader = documentDownloader;
		this.htmlConverter = htmlConverter;
	}

	@Override
	public Subscription getFromAsyncService(final URL url, final ITaskResultHandler<Recommendation> handler) {
		Subscription subscription = documentDownloader.getDocument(url, new ITaskResultHandler<Document>() {
			@Override
			public void onSuccess(Document result) {
				if (BuildConfig.DEBUG) {
					textFileWriter.writeStringToFile("LastHtmlDownload.txt", result.outerHtml());
				}
				// maybe the conversion should be done in the rxandroid pipeline
				Recommendation recommendation = htmlConverter.convert(url,result);
				handler.onSuccess(recommendation);
			}

			@Override
			public void onError(ITaskErrorResult error) {
				handler.onError(error);
			}
		});
		return subscription;
	}
}
