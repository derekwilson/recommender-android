package net.derekwilson.recommender.tasks;

import android.content.Context;

import net.derekwilson.recommender.R;

public class TaskErrorResult implements ITaskErrorResult {
	public static final int UNKNOWN  = 818;
	public static final int INTENT_UNPACK_BAD_URL  = 819;
	public static final int INTENT_UNPACK_BAD_HTTP_READ  = 820;
	public static final int INTENT_UNPACK_BAD_WIKIPEDIA_READ = 821;

	private int code;

	public TaskErrorResult(int code) {
		this.code = code;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getMessage(Context context) {
		switch (code) {
			case INTENT_UNPACK_BAD_WIKIPEDIA_READ:
				return context.getString(R.string.error_bad_wikipedia_read);
			case INTENT_UNPACK_BAD_HTTP_READ:
				return context.getString(R.string.error_read_http);
			case INTENT_UNPACK_BAD_URL:
				return context.getString(R.string.error_bad_url);
		}
		return context.getString(R.string.error_unknown);
	}
}
