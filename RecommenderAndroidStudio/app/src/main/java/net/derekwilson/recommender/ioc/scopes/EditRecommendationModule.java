package net.derekwilson.recommender.ioc.scopes;

import net.derekwilson.recommender.data.repository.IRecommendationRepository;
import net.derekwilson.recommender.data.repository.RecommendationRepository;
import net.derekwilson.recommender.ioc.ActivityScope;
import net.derekwilson.recommender.presenter.editrecommendation.EditRecommendationPresenter;
import net.derekwilson.recommender.presenter.editrecommendation.IEditRecommendationPresenter;
import net.derekwilson.recommender.presenter.editrecommendationactivity.EditRecommendationActivityPresenter;
import net.derekwilson.recommender.presenter.editrecommendationactivity.IEditRecommendationActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class EditRecommendationModule {

	/**
	 * this is the presenter used by the edit activity
	 */
	@Provides
	@ActivityScope
	IEditRecommendationActivityPresenter provideActivityPresenter(EditRecommendationActivityPresenter presenter) {
		return presenter;
	}

	/**
	 * this is the presenter used by the edit fragment
	 */
	@Provides
	@ActivityScope
	IEditRecommendationPresenter providePresenter(EditRecommendationPresenter presenter) {
		return presenter;
	}

}

