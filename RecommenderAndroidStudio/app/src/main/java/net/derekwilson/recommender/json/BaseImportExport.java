package net.derekwilson.recommender.json;

import android.content.Context;
import android.os.Environment;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.wrapper.ITextUtils;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

public class BaseImportExport {
	protected static final String FILE_PREFIX = "recommendations";
	protected static final String FILE_SUFFIX = ".json";

	protected ObjectMapper mapper;
	protected ILoggerFactory logger;
	protected ITextUtils textUtils;
	protected Context context;

	public BaseImportExport(ObjectMapper mapper, ILoggerFactory logger, ITextUtils textUtils, Context context) {
		this.mapper = mapper;
		this.logger = logger;
		this.textUtils = textUtils;
		this.context = context;
	}

	protected String getExportBasePath() {
		// we are limited where we can write unless we ask for extra permissions in android 11+
		File directory = context.getExternalFilesDir(null);
		// throw if we cannot get the path
		return directory.getAbsolutePath() + "/export/";
	}

	public String getExportPath() {
		return String.format(getExportBasePath() + FILE_PREFIX +  FILE_SUFFIX);
	}

}
