package net.derekwilson.recommender.tests.presenter.mainactivity;

import net.derekwilson.recommender.event.DbSubscriptionRestartRequired;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import rx.functions.Action1;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class EventBusTests extends PresenterTestsSetup {

	@Before
	public void setUp() {
		setupPresenter(true, false);
	}

	@Test
	public void the_subscriptions_are_restarted() {
		// arrange
		presenter.onCreate();

		// act
		eventBus.send(new DbSubscriptionRestartRequired("TEST SUB"));

		// assert
		// once for the onCreate and once for the event
		verify(mockRepository, times(2)).getCategories(any());
	}
}
