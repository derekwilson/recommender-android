package net.derekwilson.recommender.presenter.listrecommendations;

import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.presenter.IPresenter;

import java.util.List;

public interface IListRecommendationsPresenter extends IPresenter<IListRecommendationsView> {
	void moveRecommendations();

	void deleteRecommendations();

	void setFilter(IFilterCollection currentFilter);

	void shareRecommendations();

	void beamRecommendations();

    String getCurrentSearchString();

	String getRecommendationTitle(Recommendation recommendation);

	String getRecommendationSubtitle(Recommendation recommendation);
}
