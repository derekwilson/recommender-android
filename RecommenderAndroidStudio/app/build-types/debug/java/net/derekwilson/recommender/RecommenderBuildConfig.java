package net.derekwilson.recommender;

public class RecommenderBuildConfig {
	/**
	 * Whether or not to include logging statements in the application.
	 */
	public final static boolean PRODUCTION = false;
}