package net.derekwilson.recommender.fragment;

import android.app.Activity;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.squareup.sqlbrite.SqlBrite;

import net.derekwilson.recommender.AndroidApplication;
import net.derekwilson.recommender.ioc.DaggerIBaseActivityComponent;
import net.derekwilson.recommender.ioc.IApplicationComponent;
import net.derekwilson.recommender.ioc.IBaseActivityComponent;
import net.derekwilson.recommender.ioc.BaseActivityModule;
import net.derekwilson.recommender.logging.ILoggerFactory;
import net.derekwilson.recommender.view.SnackbarHelper;

import javax.inject.Inject;

public class BaseFragment extends Fragment {

	@Inject
	protected ILoggerFactory logger;

	@Inject
	protected SqlBrite db;

	protected IBaseActivityComponent activityComponent;

	protected IApplicationComponent getApplicationComponent(Activity activity) {
		return ((AndroidApplication) activity.getApplication()).getApplicationComponent();
	}

	protected IBaseActivityComponent getBaseActivityComponent(Activity activity) {
		if (activityComponent == null) {
			activityComponent = DaggerIBaseActivityComponent.builder()
					.iApplicationComponent(getApplicationComponent(activity))
					.baseActivityModule(new BaseActivityModule(activity))
					.build();
		}
		return activityComponent;
	}

	protected boolean isInjected() {
		return logger != null;
	}

	protected void displayMessage(String message) {
		if (!SnackbarHelper.displayMessageAsSnackbar(getActivity(), message)) {
			displayMessageAsToast(message);
		}
	}

	protected void displayMessageAsToast(String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}
}
