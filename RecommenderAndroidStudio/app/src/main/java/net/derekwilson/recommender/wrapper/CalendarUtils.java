package net.derekwilson.recommender.wrapper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

public class CalendarUtils implements ICalendarUtils {
    @Inject
    public CalendarUtils() {
    }

    @Override
    public String convertCalendarToDateTimeString(Calendar date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE d, MMM, yyyy HH:mm:ss", Locale.getDefault());
        return dateFormat.format(date.getTime());
    }

    @Override
    public String convertCalendarToDateString(Calendar date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE d, MMM, yyyy", Locale.getDefault());
        return dateFormat.format(date.getTime());
    }

    @Override
    public Calendar getNow() {
        return Calendar.getInstance();
    }
}