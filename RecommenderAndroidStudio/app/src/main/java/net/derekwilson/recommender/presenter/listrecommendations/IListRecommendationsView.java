package net.derekwilson.recommender.presenter.listrecommendations;

import android.content.Intent;
import android.nfc.NdefMessage;

import net.derekwilson.recommender.model.IFilterCollection;
import net.derekwilson.recommender.model.Recommendation;
import net.derekwilson.recommender.model.RecommendationType;
import net.derekwilson.recommender.presenter.IView;

import java.util.List;

public interface IListRecommendationsView extends IView {
	void setRecommendations(List<Recommendation> recommendations);

	RecommendationType getRecommendationType();

	void setFilter(IFilterCollection currentItems);

	void setNonFilteredEmptyText();

	void setFilteredEmptyText();

	int getItemCount();

	List<Recommendation> copySelectedRecommendations();

	String getNicknameKey();

	String getSortByKey();

	void startIntent(Intent intent, int promptId);

	void sendNfcMessage(NdefMessage message);
}
