package net.derekwilson.recommender.tests.receiving.intentcontenttypefinder;

import android.content.Intent;

import net.derekwilson.recommender.receiving.IntentContentType;

import org.junit.Test;

import java.net.MalformedURLException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GetTypeOfContentTests extends BaseFinderTests {

	@Test
	public void identifies_uri_content_intent() {
		// arrange
		Intent intent = setupIntent(null, null, "content", "path");

		// act
		IntentContentType type = finder.getTypeOfContent(intent);

		// assert
		assertThat(type, is(IntentContentType.ContentUri));
	}

	@Test
	public void identifies_uri_file_intent() {
		// arrange
		Intent intent = setupIntent(null, null, "file", "/storage/emulated/0/Download/recommendations_share.recommendation");

		// act
		IntentContentType type = finder.getTypeOfContent(intent);

		// assert
		assertThat(type, is(IntentContentType.FileUri));
	}

	@Test
	public void identifies_url_intent() throws MalformedURLException {
		// arrange
		Intent intent = setupIntent(null, "http://test.com", null, null);

		// act
		IntentContentType type = finder.getTypeOfContent(intent);

		// assert
		assertThat(type, is(IntentContentType.Url));
	}

	@Test
	public void identifies_text_intent() {
		// arrange
		Intent intent = setupIntent(null, "hello tests", null, null);

		// act
		IntentContentType type = finder.getTypeOfContent(intent);

		// assert
		assertThat(type, is(IntentContentType.Text));
	}
}
